///////////////////////////////////////////////
//	Author: 		Huynh Vinh Phu
//	Student ID: 	15520622
//	Module:			addbit
////////////////////////////////////////////
module addbit(
			a,
			b,
			ci,
			s,
			co
);
	output s,co;
	input ci,a,b;
	xor(x,a,b); 
	xor(s,x,ci);
	and(y,a,b);
	and(z,ci,x);
	or (co,y,z);
endmodule
