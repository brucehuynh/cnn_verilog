module sub_tb ();
    parameter DATA_WIDTH = 8;
//	( a ,b ,c ,diff ,borrow );
    reg [DATA_WIDTH-1:0] a,b;
	reg ci;
	wire [DATA_WIDTH-1:0] s,sE;
	wire co,coE;
	
	subtractor #(8) ins (a,b,ci,s,co);
	//expected_result ins_1(sE,coE,ci,a,b);
	initial begin
		stimulus();
		#1000 $finish;
	end
	
	task stimulus;
		begin
			a <= 0;
			b <= 0;
			ci <=0;
			repeat(100) begin
			#5 a<= $random;
            b <= $random;
            ci <= $random;
			
			if( {co,s} == a-b-ci) 
				$display("Expected: %b. Simulatation: %b --> PASS",a-b-ci,{co,s});
			else
				$display("Expected: %b. Simulatation: %b --> FAILED",a-b-ci,{co,s});
	    end
    end
		
	endtask

endmodule