//`include "subtract_bit.v"
module subtractor #(
		parameter DATA_WIDTH = 24
		)
	( a ,b ,c ,diff ,borrow );

	input [DATA_WIDTH-1:0] a,b;
	input c;
	output [DATA_WIDTH-1:0] diff;
	output borrow;

	
	
	wire [DATA_WIDTH-1:0] br;
	assign borrow = br[DATA_WIDTH-1];
	
	genvar i;
	generate
		for(i = 0; i < DATA_WIDTH; i = i+1)
			begin :gen_sub
				//addbit ins(s[i],cout[i],cin[i],a[i],b[i]);
				//if(i>0) assign cin[i] = cout[i-1];
				if(i == 0) subtract_bit ins(a[i],b[i],c,diff[i],br[i]);
				else if (i != DATA_WIDTH -1) subtract_bit ins(a[i],b[i],br[i-1],diff[i],br[i]);
				else subtract_bit ins(a[i],b[i],br[i-1],diff[i],br[i]);
			end
	endgenerate

endmodule
