///////////////////////////////////////////////
//	Author: 		Huynh Vinh Phu
//	Student ID: 	15520622
//	Module:			adder_32bit
////////////////////////////////////////////

module adder #(
		parameter DATA_WIDTH = 32
		)
		(s,co,ci,a,b);
	output [DATA_WIDTH-1:0] s;
	output co;
	input ci;
	input [DATA_WIDTH-1:0] a,b;

	//wire [DATA_WIDTH-1:0] cin;
	wire [DATA_WIDTH-1:0] cout;
	//assign ci = cin[0];
	//assign co = cout[DATA_WIDTH-1];	
	assign co = cout[DATA_WIDTH-1];
	genvar i;
	generate
		for(i = 0; i < DATA_WIDTH; i = i+1)
			begin :gen_loop
				//addbit ins(s[i],cout[i],cin[i],a[i],b[i]);
				//if(i>0) assign cin[i] = cout[i-1];
				//module addbit(a,b,ci,s,co);
				if(i == 0) addbit ins(a[i],b[i],ci,s[i],cout[i]);
				else addbit ins(a[i],b[i],cout[i-1],s[i],cout[i]);
				//addbit ins(s[i],cout[i],cout[i-1],a[i],b[i]);
			end
	endgenerate

endmodule
