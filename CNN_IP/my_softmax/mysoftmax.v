module mysoftmax #(
    parameter DATA_WIDTH = 32
)
    (
    clk,
    resetn,
    enable,
    valid_in,
	a0,
	a1,
	a2,
	a3,
	a4,
	a5,
	a6,
	a7,
	a8,
	a9,
    is0,
    is1,
    is2,
    is3,
    is4,
    is5,
    is6,
    is7,
    is8,
    is9,
    valid_out
    );
    input clk;
    input resetn;
    input enable;
    input valid_in;
	input [31:0] a0,a1,a2,a3,a4,a5,a6,a7,a8,a9;
    output [31:0] is0,is1,is2,is3,is4,is5,is6,is7,is8,is9;
    output valid_out;

    wire [31:0] a [0:9];
    wire [31:0] a_delay [0:9];
    wire [9:0] valid_out_get_weight;
    wire [9:0] valid_out_div;
	
    assign a[0] = a0;
	assign a[1] = a1;
	assign a[2] = a2;
	assign a[3] = a3;
	assign a[4] = a4;
	assign a[5] = a5;
	assign a[6] = a6;
	assign a[7] = a7;
	assign a[8] = a8;
	assign a[9] = a9;
	

    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a0(
                            clk,
                            resetn,
                            valid_in,
                            a[0],
                            a_delay[0]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a1(
                            clk,
                            resetn,
                            valid_in,
                            a[1],
                            a_delay[1]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a2(
                            clk,
                            resetn,
                            valid_in,
                            a[2],
                            a_delay[2]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a3(
                            clk,
                            resetn,
                            valid_out_get_weight[3],
                            a[3],
                            a_delay[3]
                            );

    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a4(
                            clk,
                            resetn,
                            valid_in,
                            a[4],
                            a_delay[4]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a5(
                            clk,
                            resetn,
                            valid_in,
                            a[5],
                            a_delay[5]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a6(
                            clk,
                            resetn,
                            valid_in,
                            a[6],
                            a_delay[6]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a7(
                            clk,
                            resetn,
                            valid_in,
                            a[7],
                            a_delay[7]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a8(
                            clk,
                            resetn,
                            valid_in,
                            a[8],
                            a_delay[8]
                            );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(24)) 
                    delay_a9(
                            clk,
                            resetn,
                            valid_in,
                            a[9],
                            a_delay[9]
                            );
        

	wire [31:0] stage1 [0:4];
	wire [4:0] valid_add_1;
	genvar i;
	generate
	for (i = 0; i < 9; i=i+2) begin
			//j=j+1;
			fpadd fpadd_s1_ins(clk,resetn,enable,valid_in,a[i],a[i+1],stage1[i>>1],valid_add_1[i>>1]);
	end
	endgenerate          

    wire [31:0] stage2 [0:2];
	wire [1:0] valid_add_2;
	fpadd fpadd_s2_ins(clk,resetn,enable,valid_add_1[0],stage1[0],stage1[1],stage2[0],valid_add_2[0]);
	fpadd fpadd_s2_ins_1(clk,resetn,enable,valid_add_1[2],stage1[2],stage1[3],stage2[1],valid_add_2[1]);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage2_4(clk,resetn,valid_add_1[0],stage1[4],stage2[2]);

    wire [31:0] stage3 [0:1];
	wire valid_add_3;
	fpadd fpadd_s3_ins(clk,resetn,enable,valid_add_2[0],stage2[0],stage2[1],stage3[0],valid_add_3);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage3_2(clk,resetn,valid_add_2[0],stage2[2],stage3[1]);

	wire [31:0] sum_of_exp; // sum of Exp
	wire valid_add_4;
	fpadd fpadd_s8_ins(clk,resetn,enable,valid_add_3,stage3[0],stage3[1],sum_of_exp,valid_add_4);

    fpdiv fpdiv_ins0(clk,resetn,enable,valid_add_4,a_delay[0],sum_of_exp,is0,valid_out_div[0]);
    fpdiv fpdiv_ins1(clk,resetn,enable,valid_add_4,a_delay[1],sum_of_exp,is1,valid_out_div[1]);
    fpdiv fpdiv_ins2(clk,resetn,enable,valid_add_4,a_delay[2],sum_of_exp,is2,valid_out_div[2]);
    fpdiv fpdiv_ins3(clk,resetn,enable,valid_add_4,a_delay[3],sum_of_exp,is3,valid_out_div[3]);
    fpdiv fpdiv_ins4(clk,resetn,enable,valid_add_4,a_delay[4],sum_of_exp,is4,valid_out_div[4]);
    fpdiv fpdiv_ins5(clk,resetn,enable,valid_add_4,a_delay[5],sum_of_exp,is5,valid_out_div[5]);
    fpdiv fpdiv_ins6(clk,resetn,enable,valid_add_4,a_delay[6],sum_of_exp,is6,valid_out_div[6]);
    fpdiv fpdiv_ins7(clk,resetn,enable,valid_add_4,a_delay[7],sum_of_exp,is7,valid_out_div[7]);
    fpdiv fpdiv_ins8(clk,resetn,enable,valid_add_4,a_delay[8],sum_of_exp,is8,valid_out_div[8]);
    fpdiv fpdiv_ins9(clk,resetn,enable,valid_add_4,a_delay[9],sum_of_exp,is9,valid_out_div[9]);
    assign valid_out = valid_out_div[0];



endmodule