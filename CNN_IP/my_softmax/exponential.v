module exponential(
    clk,
    resetn,
    enable,
    valid_in,
    x,
    result,
    valid_out
    );

    input clk;
    input resetn;
    input enable;
    input valid_in;
    input [31:0] x;
    output [31:0] result;
    output valid_out;

    wire [31:0] res_add_1,res_add_2,res_add_3;
    wire [31:0] p1,p2,p3,p4;
    wire valid_out_add1,valid_out_add2,valid_out_add3;
    wire valid_out_mult1,valid_out_mult2,valid_out_mult3,valid_out_mult4;

    wire [31:0] res_add_1_delay,res_add_2_delay;
    
    //res_add_3,
    //valid_out_add3
    assign result = res_add_3;
    assign valid_out = valid_out_add3;

    //module fpadd(clk,resetn,enable,valid_in,a,b,out,valid_out);
    fpadd add1(
        clk,
        resetn,
        enable,
        valid_in,
        32'h3f800000, // one
        x,
        res_add_1,
        valid_out_add1
    );

    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(94))
                delay_add1 (
                    clk,
                    resetn,
                    valid_out_add1,
                    res_add_1,
                    res_add_1_delay
                    );
   
    FP_multiplier mult1(
        	clk,
			resetn,
			enable,
			valid_in,
			x,
			x,
			p1,
			valid_out_mult1
    );

    FP_multiplier mult2(
        	clk,
			resetn,
			enable,
			valid_out_mult1,
			p1,
			32'h3f000000, // half
			p2,
			valid_out_mult2
    );
    FP_multiplier mult3(
        	clk,
			resetn,
			enable,
			valid_out_mult1,
			p1,
			p1,
			p3,
			valid_out_mult3
    );

    fpadd add2(
        clk,
        resetn,
        enable,
        valid_out_mult2,
        res_add_1_delay,
        p2,
        res_add_2,
        valid_out_add2
    );
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(44))
                delay_add2 (
                    clk,
                    resetn,
                    valid_out_add2,
                    res_add_2,
                    res_add_2_delay
                    );

    FP_multiplier mult4(
        	clk,
			resetn,
			enable,
			valid_out_mult3,
			p3,
			32'h3e2aaaab, // oversix 
			p4,
			valid_out_mult4
    );
    fpadd add3(
        clk,
        resetn,
        enable,
        valid_out_mult4,
        res_add_2_delay,
        p4,
        res_add_3,
        valid_out_add3
    );

endmodule