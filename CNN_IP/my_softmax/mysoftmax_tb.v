module mysoftmax_tb();
    reg clk;
    reg resetn;
    reg enable;
    reg valid_in;
    reg [31:0] data_in;
    wire [31:0] is0,is1,is2,is3,is4,is5,is6,is7,is8,is9;
    wire valid_out;

    mysoftmax #(.DATA_WIDTH(32),.WIDTH(28),.HEIGHT(28)) softmax_ins
                        (
                        clk,
                        resetn,
                        enable,
                        valid_in,
                        data_in,
                        is0,
                        is1,
                        is2,
                        is3,
                        is4,
                        is5,
                        is6,
                        is7,
                        is8,
                        is9,
                        valid_out
                        );

    always #1 clk = ~clk;
    initial begin
      clk = 0;
      resetn = 0;
      enable = 0;
      valid_in = 0;
      #10 resetn = 1;
      enable = 1;
      #10 valid_in <= 1;
      data_in <= 32'h3f800000;
      #10000 $finish();
    end


endmodule