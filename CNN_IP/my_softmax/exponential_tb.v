module exponential_tb();
/*
module exponential(
    clk,
    resetn,
    enable,
    valid_in,
    x,
    result,
    valid_out
    );
*/
    reg clk;
    reg resetn;
    reg enable;
    reg valid_in;
    reg [31:0] x;
    wire [31:0] result;
    wire valid_out;

    exponential exp(clk,resetn,enable,valid_in,x,result,valid_out);

    always #1 clk = ~clk;
    initial begin
    $monitor("x = %b, result = %b, valid out = %b",x,result,valid_out);
		clk = 0;
		resetn = 0;
	 	enable = 0;		
	#10	resetn = 1;
		enable = 1;
        valid_in =1;
        x = 32'b0;
        #2 x  = 32'b00111111100000000000000000000000;
        #2 x = 32'b10111111100000000000000000000000;
        #2 x = 32'b01000011010001000000000000000000;
        #500 $finish();
    end

endmodule