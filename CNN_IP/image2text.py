#import matplotlib.image as mpimg
#import matplotlib.pyplot as plt
import cv2 as cv
import sys
from scipy import ndimage
import numpy as np
import struct

def bin_to_float(b):
    """ Convert binary string to a float. """
    f = int(b, 2)  
    return struct.unpack('f', struct.pack('I', f))[0]

def createImage(W,H):
    k = 1
    im = np.empty([H,W,1],dtype=np.uint8)
    for i in range(H):
        for j in range(W): 
            im[i][j]=np.uint8(k)
            if k < 128:
                k = k + 1
            else:
                k = 1
        
    return im

def i2t(image,filename):
    #filename = 'image_to_text.txt'
    f = open(filename,'w')
    #image = mpimg.imread(fileimage)
    try:
        height, width, channels = image.shape
        X = image.reshape(height*width,channels)
        frames = 1 # it's image, duh
        f.write(str(width))
        f.write("\n")
        f.write(str(height))
        f.write("\n")
        f.write(str(frames))
        f.write("\n")
        for j in range(width*height):
        #for i in range(channels): 
            data=X[j][0]
            #print(data)
            f.write(str(data))
            f.write("\n")
    except:
        height, width = image.shape
        X = image.reshape(height*width)
        frames = 1 # it's image, duh
        f.write(str(width))
        f.write("\n")
        f.write(str(height))
        f.write("\n")
        f.write(str(frames))
        f.write("\n")
        for j in range(width*height):
        #for i in range(channels): 
            data=X[j]
            #print(data)
            f.write(str(data))
            f.write("\n")
    
    
    #print(width,height,sep='\n')
    
    f.close

def t2i_rgb():
    filename = 'image_to_text.txt'
    f = open(filename,'r')
    line1 = f.readline()
    width=int(line1.split('\n')[0])
    #print (width)
    line2 = f.readline()
    height=int(line2.split('\n')[0])
    #print (height)
    line3 = f.readline()
    frame=int(line3.split('\n')[0])
    #print (frame)
    im = np.empty([height,width,3],dtype=np.uint8)
    for i in range (width):
        for j in range (height):
            R = f.readline()
            im[i][j][0] = np.uint8(R.split('\n')[0])
            G = f.readline()
            im[i][j][1] = np.uint8(G.split('\n')[0])
            B = f.readline()
            im[i][j][2] = np.uint8(B.split('\n')[0])
    #print (im)
    f.close()
    cv.imshow('Image_rgb Read from text',im)
    cv.waitKey(0)
    return im

def t2i_gray():
    filename = '../scripts/text2img_sink.txt'
    f = open(filename,'r')
    line1 = f.readline()
    width=int(line1.split('\n')[0])
    print (width)
    line2 = f.readline()
    height=int(line2.split('\n')[0])
    print (height)
    line3 = f.readline()
    frame=int(line3.split('\n')[0])
    print (frame)
    im = np.empty([height,width,1],dtype=np.uint8)
    k=0
    for i in range (width):
        for j in range (height):
            #R = f.readline()
            k=k+1
            print("Pixels #",k)
            G = f.readline()
            G=G.strip()
            G=np.uint8(bin_to_float(G))
            print(G)
            if not G: break
            #B = f.readline()
            #data = G.split(' ')[0]
            im[i][j][0] = (G)
    #print (im)
    f.close()
    cv.imwrite('result.jpg',im)
    cv.imshow('result',im)
    cv.waitKey(0)
    return im

def edge_detect(image):
    #k = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
    sobelX = np.array((
	[1/9, 1/9, 1/9],
	[1/9, 1/9, 1/9],
	[1/9, 1/9, 1/9]), dtype="float")
    result = cv.filter2D(image,-1,sobelX)
    return result

def grayConversion(image):
    grayValue = 0.07 * image[:,:,2] + 0.72 * image[:,:,1] + 0.21 * image[:,:,0]
    gray_img = grayValue.astype(np.uint8)
    return gray_img

image = cv.imread(sys.argv[1])
#print(image)
x = cv.resize(image,(28,28))
gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
res = cv.resize(gray,(28,28))

#image = createImage(6,6)
#print(image)
a = edge_detect(res)
i2t(x,'lena.txt')
i2t(a,'lena_result.txt')
#res = grayConversion(res)
#res = edge_detect(res)
#a = cv.cvtColor(res, cv.COLOR_BGR2GRAY)
cv.imwrite('result_egde.jpg',a)
cv.imshow('result_egde',a)
cv.waitKey(0)

#
#i2t(image)
#t2i_gray()

#cv.waitKey(0)

