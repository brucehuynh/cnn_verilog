`timescale 1ns/1ps
module div24_test();

    reg clk,resetn,enable;
    reg [47:0] z;
    wire [23:0] d;
    wire [24:0] q;
    wire [47:0] s;
    wire valid_out;
    integer count = 0;

    wire [47:0] z_bk;
    wire [23:0] d_bk;
    reg [22:0] d_r;
    assign d = {d_r,1'b1};

    divider dut(clk,resetn,enable,z,d,q,s,valid_out);
    delay_clock #(.DATA_WIDTH(48),.N_CLOCKs(51)) store_z(clk,resetn,enable,z,z_bk);
    delay_clock #(.DATA_WIDTH(24),.N_CLOCKs(51)) store_d(clk,resetn,enable,d,d_bk);
    always #2 clk = ~clk;

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) count = 0;
        else if (enable == 1'b1) begin 
            count = count + 1;
            $display("Clock count = %d ",count);
            if ( count >= 50 && q*d_bk + s == z_bk ) $display("Z %d , D %d ,Q %d, REM %d PASS",z_bk,d_bk,q,s);
            else if ( count >= 50 && q*d_bk + s != z_bk ) $display("Z %d , D %d ,Q %d, REM %d FAIL",z_bk,d_bk,q,s);
        end
        else count = count;
    end
 
    initial begin 
        $monitor("time = %d, clk=%b,z= %d, d = %d, q= %d, rem = %d valid = %b",$time,clk,z_bk,d_bk,q,s,valid_out);
        clk = 0;
        enable = 1;
        resetn = 1;
        //count = 0;
        //#2
        z <= 48'd0;
        d_r <= 23'd234;
        
        stimulus;
        #204
        
        
        #1000 $finish();
    end

    task stimulus;
		begin
			repeat(200) begin
			#4 z<= $random;
            
            d_r <= $random;
            //d <= 
    
            //ci <= $random;
	    end
    end
    endtask


//module div4bit(clk,resetn,enable,z,d,q,s);



endmodule