//51 chu ky
module divider(clk,resetn,enable,z,d,q,s,valid_out);
	input clk;
	input resetn;
	input enable;
	input [47:0] z;
	input [23:0] d;
	output [24:0] q;
	output [47:0] s;
	output reg valid_out;

	reg [24:0] quotient;
	reg [47:0] remainder;
	reg [47:0] divisor;

	wire [47:0] kq_sub1,kq_sub2,kq_sub3,kq_sub4,kq_sub5,kq_sub6,kq_sub7,kq_sub8,kq_sub9,kq_sub10,kq_sub11,kq_sub12,kq_sub13,kq_sub14,kq_sub15,kq_sub16,kq_sub17,kq_sub18,kq_sub19,kq_sub20,kq_sub21,kq_sub22,kq_sub23,kq_sub24,kq_sub25;

	wire co_sub1,co_sub2,co_sub3,co_sub4,co_sub5,co_sub6,co_sub7,co_sub8,co_sub9,co_sub10,co_sub11,co_sub12,co_sub13,co_sub14,co_sub15,co_sub16,co_sub17,co_sub18,co_sub19,co_sub20,co_sub21,co_sub22,co_sub23,co_sub24,co_sub25;

	wire [47:0] r1,d1,r3,d3,r5,d5,r7,d7,r9,d9,r11,d11,r13,d13,r15,d15,r17,d17,r19,d19,r21,d21,r23,d23,r25,d25,r27,d27,r29,d29,r31,d31,r33,d33,r35,d35,r37,d37,r39,d39,r41,d41,r43,d43,r45,d45,r47,d47,r49,d49;

	wire [24:0] q1,q3,q5,q7,q9,q11,q13,q15,q17,q19,q21,q23,q25,q27,q29,q31,q33,q35,q37,q39,q41,q43,q45,q47,q49;

	reg [24:0] q2,q4,q6,q8,q10,q12,q14,q16,q18,q20,q22,q24,q26,q28,q30,q32,q34,q36,q38,q40,q42,q44,q46,q48,q50;

	reg [47:0] d2,r2,d4,r4,d6,r6,d8,r8,d10,r10,d12,r12,d14,r14,d16,r16,d18,r18,d20,r20,d22,r22,d24,r24,d26,r26,d28,r28,d30,r30,d32,r32,d34,r34,d36,r36,d38,r38,d40,r40,d42,r42,d44,r44,d46,r46,d48,r48,d50,r50;

	assign q = q50;
	assign s = r50;
//delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(51)) delay_en(clk,resetn,enable,enable,)
	reg [5:0] counter=0;

	always @ (posedge clk or negedge resetn) begin
	  if (resetn == 1'b0) counter <= 0;
	  else if (enable == 1'b1) begin
		 counter <= counter + 6'b1;
	  end
	  else counter <= counter;
	end
	always @ (posedge clk or negedge resetn) begin
	  if (resetn == 1'b0) valid_out <= 0;
	  else if (counter == 6'd50) begin
		 valid_out <= 1'b1;
	  end
	  else valid_out <= valid_out;
	end

// init
	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			quotient <= 0; 
			remainder <= 0; 
			divisor <= 0;  
	end
	else if (enable == 1'b1) begin
			quotient <= 0; 
			remainder <= z; 
			divisor <= {d,24'b0}; 
	end
	else begin
			quotient <= quotient; 
			remainder <= remainder; 
			divisor <= divisor; 
		end
	end
	//buffer 1
	nbit_dff #(25) quotient_ff1(clk,resetn,enable,quotient,q1);
	nbit_dff #(48) divisor_ff1(clk,resetn,enable,divisor,d1);
	nbit_dff #(48) remainder_ff1(clk,resetn,enable,remainder,r1);
	//sub 1
	subtractor #(48) sub_1(r1,d1,1'b0,kq_sub1,co_sub1);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q2 <= 0; 
			r2 <= 0; 
			d2 <= 0;  
	end
	else if (co_sub1 == 1'b1) begin
			q2 <= {q1[23:0],1'b0}; 
			r2 <= r1; 
			d2 <= {1'b0, d1[47:1]}; 
	end
	else if (co_sub1 == 1'b0) begin
			q2 <= {q1[23:0],1'b1}; 
			r2 <= kq_sub1; 
			d2 <= {1'b0, d1[47:1]}; 
		end
	end
	//buffer 2
	nbit_dff #(25) quotient_ff3(clk,resetn,enable,q2,q3);
	nbit_dff #(48) divisor_ff3(clk,resetn,enable,d2,d3);
	nbit_dff #(48) remainder_ff3(clk,resetn,enable,r2,r3);
	//sub 2
	subtractor #(48) sub_2(r3,d3,1'b0,kq_sub2,co_sub2);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q4 <= 0; 
			r4 <= 0; 
			d4 <= 0;  
	end
	else if (co_sub2 == 1'b1) begin
			q4 <= {q3[23:0],1'b0}; 
			r4 <= r3; 
			d4 <= {1'b0, d3[47:1]}; 
	end
	else if (co_sub2 == 1'b0) begin
			q4 <= {q3[23:0],1'b1}; 
			r4 <= kq_sub2; 
			d4 <= {1'b0, d3[47:1]}; 
		end
	end
	//buffer 3
	nbit_dff #(25) quotient_ff5(clk,resetn,enable,q4,q5);
	nbit_dff #(48) divisor_ff5(clk,resetn,enable,d4,d5);
	nbit_dff #(48) remainder_ff5(clk,resetn,enable,r4,r5);
	//sub 3
	subtractor #(48) sub_3(r5,d5,1'b0,kq_sub3,co_sub3);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q6 <= 0; 
			r6 <= 0; 
			d6 <= 0;  
	end
	else if (co_sub3 == 1'b1) begin
			q6 <= {q5[23:0],1'b0}; 
			r6 <= r5; 
			d6 <= {1'b0, d5[47:1]}; 
	end
	else if (co_sub3 == 1'b0) begin
			q6 <= {q5[23:0],1'b1}; 
			r6 <= kq_sub3; 
			d6 <= {1'b0, d5[47:1]}; 
		end
	end
	//buffer 4
	nbit_dff #(25) quotient_ff7(clk,resetn,enable,q6,q7);
	nbit_dff #(48) divisor_ff7(clk,resetn,enable,d6,d7);
	nbit_dff #(48) remainder_ff7(clk,resetn,enable,r6,r7);
	//sub 4
	subtractor #(48) sub_4(r7,d7,1'b0,kq_sub4,co_sub4);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q8 <= 0; 
			r8 <= 0; 
			d8 <= 0;  
	end
	else if (co_sub4 == 1'b1) begin
			q8 <= {q7[23:0],1'b0}; 
			r8 <= r7; 
			d8 <= {1'b0, d7[47:1]}; 
	end
	else if (co_sub4 == 1'b0) begin
			q8 <= {q7[23:0],1'b1}; 
			r8 <= kq_sub4; 
			d8 <= {1'b0, d7[47:1]}; 
		end
	end
	//buffer 5
	nbit_dff #(25) quotient_ff9(clk,resetn,enable,q8,q9);
	nbit_dff #(48) divisor_ff9(clk,resetn,enable,d8,d9);
	nbit_dff #(48) remainder_ff9(clk,resetn,enable,r8,r9);
	//sub 5
	subtractor #(48) sub_5(r9,d9,1'b0,kq_sub5,co_sub5);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q10 <= 0; 
			r10 <= 0; 
			d10 <= 0;  
	end
	else if (co_sub5 == 1'b1) begin
			q10 <= {q9[23:0],1'b0}; 
			r10 <= r9; 
			d10 <= {1'b0, d9[47:1]}; 
	end
	else if (co_sub5 == 1'b0) begin
			q10 <= {q9[23:0],1'b1}; 
			r10 <= kq_sub5; 
			d10 <= {1'b0, d9[47:1]}; 
		end
	end
	//buffer 6
	nbit_dff #(25) quotient_ff11(clk,resetn,enable,q10,q11);
	nbit_dff #(48) divisor_ff11(clk,resetn,enable,d10,d11);
	nbit_dff #(48) remainder_ff11(clk,resetn,enable,r10,r11);
	//sub 6
	subtractor #(48) sub_6(r11,d11,1'b0,kq_sub6,co_sub6);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q12 <= 0; 
			r12 <= 0; 
			d12 <= 0;  
	end
	else if (co_sub6 == 1'b1) begin
			q12 <= {q11[23:0],1'b0}; 
			r12 <= r11; 
			d12 <= {1'b0, d11[47:1]}; 
	end
	else if (co_sub6 == 1'b0) begin
			q12 <= {q11[23:0],1'b1}; 
			r12 <= kq_sub6; 
			d12 <= {1'b0, d11[47:1]}; 
		end
	end
	//buffer 7
	nbit_dff #(25) quotient_ff13(clk,resetn,enable,q12,q13);
	nbit_dff #(48) divisor_ff13(clk,resetn,enable,d12,d13);
	nbit_dff #(48) remainder_ff13(clk,resetn,enable,r12,r13);
	//sub 7
	subtractor #(48) sub_7(r13,d13,1'b0,kq_sub7,co_sub7);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q14 <= 0; 
			r14 <= 0; 
			d14 <= 0;  
	end
	else if (co_sub7 == 1'b1) begin
			q14 <= {q13[23:0],1'b0}; 
			r14 <= r13; 
			d14 <= {1'b0, d13[47:1]}; 
	end
	else if (co_sub7 == 1'b0) begin
			q14 <= {q13[23:0],1'b1}; 
			r14 <= kq_sub7; 
			d14 <= {1'b0, d13[47:1]}; 
		end
	end
	//buffer 8
	nbit_dff #(25) quotient_ff15(clk,resetn,enable,q14,q15);
	nbit_dff #(48) divisor_ff15(clk,resetn,enable,d14,d15);
	nbit_dff #(48) remainder_ff15(clk,resetn,enable,r14,r15);
	//sub 8
	subtractor #(48) sub_8(r15,d15,1'b0,kq_sub8,co_sub8);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q16 <= 0; 
			r16 <= 0; 
			d16 <= 0;  
	end
	else if (co_sub8 == 1'b1) begin
			q16 <= {q15[23:0],1'b0}; 
			r16 <= r15; 
			d16 <= {1'b0, d15[47:1]}; 
	end
	else if (co_sub8 == 1'b0) begin
			q16 <= {q15[23:0],1'b1}; 
			r16 <= kq_sub8; 
			d16 <= {1'b0, d15[47:1]}; 
		end
	end
	//buffer 9
	nbit_dff #(25) quotient_ff17(clk,resetn,enable,q16,q17);
	nbit_dff #(48) divisor_ff17(clk,resetn,enable,d16,d17);
	nbit_dff #(48) remainder_ff17(clk,resetn,enable,r16,r17);
	//sub 9
	subtractor #(48) sub_9(r17,d17,1'b0,kq_sub9,co_sub9);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q18 <= 0; 
			r18 <= 0; 
			d18 <= 0;  
	end
	else if (co_sub9 == 1'b1) begin
			q18 <= {q17[23:0],1'b0}; 
			r18 <= r17; 
			d18 <= {1'b0, d17[47:1]}; 
	end
	else if (co_sub9 == 1'b0) begin
			q18 <= {q17[23:0],1'b1}; 
			r18 <= kq_sub9; 
			d18 <= {1'b0, d17[47:1]}; 
		end
	end
	//buffer 10
	nbit_dff #(25) quotient_ff19(clk,resetn,enable,q18,q19);
	nbit_dff #(48) divisor_ff19(clk,resetn,enable,d18,d19);
	nbit_dff #(48) remainder_ff19(clk,resetn,enable,r18,r19);
	//sub 10
	subtractor #(48) sub_10(r19,d19,1'b0,kq_sub10,co_sub10);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q20 <= 0; 
			r20 <= 0; 
			d20 <= 0;  
	end
	else if (co_sub10 == 1'b1) begin
			q20 <= {q19[23:0],1'b0}; 
			r20 <= r19; 
			d20 <= {1'b0, d19[47:1]}; 
	end
	else if (co_sub10 == 1'b0) begin
			q20 <= {q19[23:0],1'b1}; 
			r20 <= kq_sub10; 
			d20 <= {1'b0, d19[47:1]}; 
		end
	end
	//buffer 11
	nbit_dff #(25) quotient_ff21(clk,resetn,enable,q20,q21);
	nbit_dff #(48) divisor_ff21(clk,resetn,enable,d20,d21);
	nbit_dff #(48) remainder_ff21(clk,resetn,enable,r20,r21);
	//sub 11
	subtractor #(48) sub_11(r21,d21,1'b0,kq_sub11,co_sub11);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q22 <= 0; 
			r22 <= 0; 
			d22 <= 0;  
	end
	else if (co_sub11 == 1'b1) begin
			q22 <= {q21[23:0],1'b0}; 
			r22 <= r21; 
			d22 <= {1'b0, d21[47:1]}; 
	end
	else if (co_sub11 == 1'b0) begin
			q22 <= {q21[23:0],1'b1}; 
			r22 <= kq_sub11; 
			d22 <= {1'b0, d21[47:1]}; 
		end
	end
	//buffer 12
	nbit_dff #(25) quotient_ff23(clk,resetn,enable,q22,q23);
	nbit_dff #(48) divisor_ff23(clk,resetn,enable,d22,d23);
	nbit_dff #(48) remainder_ff23(clk,resetn,enable,r22,r23);
	//sub 12
	subtractor #(48) sub_12(r23,d23,1'b0,kq_sub12,co_sub12);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q24 <= 0; 
			r24 <= 0; 
			d24 <= 0;  
	end
	else if (co_sub12 == 1'b1) begin
			q24 <= {q23[23:0],1'b0}; 
			r24 <= r23; 
			d24 <= {1'b0, d23[47:1]}; 
	end
	else if (co_sub12 == 1'b0) begin
			q24 <= {q23[23:0],1'b1}; 
			r24 <= kq_sub12; 
			d24 <= {1'b0, d23[47:1]}; 
		end
	end
	//buffer 13
	nbit_dff #(25) quotient_ff25(clk,resetn,enable,q24,q25);
	nbit_dff #(48) divisor_ff25(clk,resetn,enable,d24,d25);
	nbit_dff #(48) remainder_ff25(clk,resetn,enable,r24,r25);
	//sub 13
	subtractor #(48) sub_13(r25,d25,1'b0,kq_sub13,co_sub13);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q26 <= 0; 
			r26 <= 0; 
			d26 <= 0;  
	end
	else if (co_sub13 == 1'b1) begin
			q26 <= {q25[23:0],1'b0}; 
			r26 <= r25; 
			d26 <= {1'b0, d25[47:1]}; 
	end
	else if (co_sub13 == 1'b0) begin
			q26 <= {q25[23:0],1'b1}; 
			r26 <= kq_sub13; 
			d26 <= {1'b0, d25[47:1]}; 
		end
	end
	//buffer 14
	nbit_dff #(25) quotient_ff27(clk,resetn,enable,q26,q27);
	nbit_dff #(48) divisor_ff27(clk,resetn,enable,d26,d27);
	nbit_dff #(48) remainder_ff27(clk,resetn,enable,r26,r27);
	//sub 14
	subtractor #(48) sub_14(r27,d27,1'b0,kq_sub14,co_sub14);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q28 <= 0; 
			r28 <= 0; 
			d28 <= 0;  
	end
	else if (co_sub14 == 1'b1) begin
			q28 <= {q27[23:0],1'b0}; 
			r28 <= r27; 
			d28 <= {1'b0, d27[47:1]}; 
	end
	else if (co_sub14 == 1'b0) begin
			q28 <= {q27[23:0],1'b1}; 
			r28 <= kq_sub14; 
			d28 <= {1'b0, d27[47:1]}; 
		end
	end
	//buffer 15
	nbit_dff #(25) quotient_ff29(clk,resetn,enable,q28,q29);
	nbit_dff #(48) divisor_ff29(clk,resetn,enable,d28,d29);
	nbit_dff #(48) remainder_ff29(clk,resetn,enable,r28,r29);
	//sub 15
	subtractor #(48) sub_15(r29,d29,1'b0,kq_sub15,co_sub15);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q30 <= 0; 
			r30 <= 0; 
			d30 <= 0;  
	end
	else if (co_sub15 == 1'b1) begin
			q30 <= {q29[23:0],1'b0}; 
			r30 <= r29; 
			d30 <= {1'b0, d29[47:1]}; 
	end
	else if (co_sub15 == 1'b0) begin
			q30 <= {q29[23:0],1'b1}; 
			r30 <= kq_sub15; 
			d30 <= {1'b0, d29[47:1]}; 
		end
	end
	//buffer 16
	nbit_dff #(25) quotient_ff31(clk,resetn,enable,q30,q31);
	nbit_dff #(48) divisor_ff31(clk,resetn,enable,d30,d31);
	nbit_dff #(48) remainder_ff31(clk,resetn,enable,r30,r31);
	//sub 16
	subtractor #(48) sub_16(r31,d31,1'b0,kq_sub16,co_sub16);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q32 <= 0; 
			r32 <= 0; 
			d32 <= 0;  
	end
	else if (co_sub16 == 1'b1) begin
			q32 <= {q31[23:0],1'b0}; 
			r32 <= r31; 
			d32 <= {1'b0, d31[47:1]}; 
	end
	else if (co_sub16 == 1'b0) begin
			q32 <= {q31[23:0],1'b1}; 
			r32 <= kq_sub16; 
			d32 <= {1'b0, d31[47:1]}; 
		end
	end
	//buffer 17
	nbit_dff #(25) quotient_ff33(clk,resetn,enable,q32,q33);
	nbit_dff #(48) divisor_ff33(clk,resetn,enable,d32,d33);
	nbit_dff #(48) remainder_ff33(clk,resetn,enable,r32,r33);
	//sub 17
	subtractor #(48) sub_17(r33,d33,1'b0,kq_sub17,co_sub17);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q34 <= 0; 
			r34 <= 0; 
			d34 <= 0;  
	end
	else if (co_sub17 == 1'b1) begin
			q34 <= {q33[23:0],1'b0}; 
			r34 <= r33; 
			d34 <= {1'b0, d33[47:1]}; 
	end
	else if (co_sub17 == 1'b0) begin
			q34 <= {q33[23:0],1'b1}; 
			r34 <= kq_sub17; 
			d34 <= {1'b0, d33[47:1]}; 
		end
	end
	//buffer 18
	nbit_dff #(25) quotient_ff35(clk,resetn,enable,q34,q35);
	nbit_dff #(48) divisor_ff35(clk,resetn,enable,d34,d35);
	nbit_dff #(48) remainder_ff35(clk,resetn,enable,r34,r35);
	//sub 18
	subtractor #(48) sub_18(r35,d35,1'b0,kq_sub18,co_sub18);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q36 <= 0; 
			r36 <= 0; 
			d36 <= 0;  
	end
	else if (co_sub18 == 1'b1) begin
			q36 <= {q35[23:0],1'b0}; 
			r36 <= r35; 
			d36 <= {1'b0, d35[47:1]}; 
	end
	else if (co_sub18 == 1'b0) begin
			q36 <= {q35[23:0],1'b1}; 
			r36 <= kq_sub18; 
			d36 <= {1'b0, d35[47:1]}; 
		end
	end
	//buffer 19
	nbit_dff #(25) quotient_ff37(clk,resetn,enable,q36,q37);
	nbit_dff #(48) divisor_ff37(clk,resetn,enable,d36,d37);
	nbit_dff #(48) remainder_ff37(clk,resetn,enable,r36,r37);
	//sub 19
	subtractor #(48) sub_19(r37,d37,1'b0,kq_sub19,co_sub19);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q38 <= 0; 
			r38 <= 0; 
			d38 <= 0;  
	end
	else if (co_sub19 == 1'b1) begin
			q38 <= {q37[23:0],1'b0}; 
			r38 <= r37; 
			d38 <= {1'b0, d37[47:1]}; 
	end
	else if (co_sub19 == 1'b0) begin
			q38 <= {q37[23:0],1'b1}; 
			r38 <= kq_sub19; 
			d38 <= {1'b0, d37[47:1]}; 
		end
	end
	//buffer 20
	nbit_dff #(25) quotient_ff39(clk,resetn,enable,q38,q39);
	nbit_dff #(48) divisor_ff39(clk,resetn,enable,d38,d39);
	nbit_dff #(48) remainder_ff39(clk,resetn,enable,r38,r39);
	//sub 20
	subtractor #(48) sub_20(r39,d39,1'b0,kq_sub20,co_sub20);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q40 <= 0; 
			r40 <= 0; 
			d40 <= 0;  
	end
	else if (co_sub20 == 1'b1) begin
			q40 <= {q39[23:0],1'b0}; 
			r40 <= r39; 
			d40 <= {1'b0, d39[47:1]}; 
	end
	else if (co_sub20 == 1'b0) begin
			q40 <= {q39[23:0],1'b1}; 
			r40 <= kq_sub20; 
			d40 <= {1'b0, d39[47:1]}; 
		end
	end
	//buffer 21
	nbit_dff #(25) quotient_ff41(clk,resetn,enable,q40,q41);
	nbit_dff #(48) divisor_ff41(clk,resetn,enable,d40,d41);
	nbit_dff #(48) remainder_ff41(clk,resetn,enable,r40,r41);
	//sub 21
	subtractor #(48) sub_21(r41,d41,1'b0,kq_sub21,co_sub21);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q42 <= 0; 
			r42 <= 0; 
			d42 <= 0;  
	end
	else if (co_sub21 == 1'b1) begin
			q42 <= {q41[23:0],1'b0}; 
			r42 <= r41; 
			d42 <= {1'b0, d41[47:1]}; 
	end
	else if (co_sub21 == 1'b0) begin
			q42 <= {q41[23:0],1'b1}; 
			r42 <= kq_sub21; 
			d42 <= {1'b0, d41[47:1]}; 
		end
	end
	//buffer 22
	nbit_dff #(25) quotient_ff43(clk,resetn,enable,q42,q43);
	nbit_dff #(48) divisor_ff43(clk,resetn,enable,d42,d43);
	nbit_dff #(48) remainder_ff43(clk,resetn,enable,r42,r43);
	//sub 22
	subtractor #(48) sub_22(r43,d43,1'b0,kq_sub22,co_sub22);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q44 <= 0; 
			r44 <= 0; 
			d44 <= 0;  
	end
	else if (co_sub22 == 1'b1) begin
			q44 <= {q43[23:0],1'b0}; 
			r44 <= r43; 
			d44 <= {1'b0, d43[47:1]}; 
	end
	else if (co_sub22 == 1'b0) begin
			q44 <= {q43[23:0],1'b1}; 
			r44 <= kq_sub22; 
			d44 <= {1'b0, d43[47:1]}; 
		end
	end
	//buffer 23
	nbit_dff #(25) quotient_ff45(clk,resetn,enable,q44,q45);
	nbit_dff #(48) divisor_ff45(clk,resetn,enable,d44,d45);
	nbit_dff #(48) remainder_ff45(clk,resetn,enable,r44,r45);
	//sub 23
	subtractor #(48) sub_23(r45,d45,1'b0,kq_sub23,co_sub23);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q46 <= 0; 
			r46 <= 0; 
			d46 <= 0;  
	end
	else if (co_sub23 == 1'b1) begin
			q46 <= {q45[23:0],1'b0}; 
			r46 <= r45; 
			d46 <= {1'b0, d45[47:1]}; 
	end
	else if (co_sub23 == 1'b0) begin
			q46 <= {q45[23:0],1'b1}; 
			r46 <= kq_sub23; 
			d46 <= {1'b0, d45[47:1]}; 
		end
	end
	//buffer 24
	nbit_dff #(25) quotient_ff47(clk,resetn,enable,q46,q47);
	nbit_dff #(48) divisor_ff47(clk,resetn,enable,d46,d47);
	nbit_dff #(48) remainder_ff47(clk,resetn,enable,r46,r47);
	//sub 24
	subtractor #(48) sub_24(r47,d47,1'b0,kq_sub24,co_sub24);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q48 <= 0; 
			r48 <= 0; 
			d48 <= 0;  
	end
	else if (co_sub24 == 1'b1) begin
			q48 <= {q47[23:0],1'b0}; 
			r48 <= r47; 
			d48 <= {1'b0, d47[47:1]}; 
	end
	else if (co_sub24 == 1'b0) begin
			q48 <= {q47[23:0],1'b1}; 
			r48 <= kq_sub24; 
			d48 <= {1'b0, d47[47:1]}; 
		end
	end
	//buffer 25
	nbit_dff #(25) quotient_ff49(clk,resetn,enable,q48,q49);
	nbit_dff #(48) divisor_ff49(clk,resetn,enable,d48,d49);
	nbit_dff #(48) remainder_ff49(clk,resetn,enable,r48,r49);
	//sub 25
	subtractor #(48) sub_25(r49,d49,1'b0,kq_sub25,co_sub25);

	always @ (posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) begin 
			q50 <= 0; 
			r50 <= 0; 
			d50 <= 0;
			//valid_out <=0;  
	end
	else if (co_sub25 == 1'b1) begin
			q50 <= {q49[23:0],1'b0}; 
			r50 <= r49; 
			d50 <= {1'b0, d49[47:1]};
			//valid_out <= 1; 
	end
	else if (co_sub25 == 1'b0) begin
			q50 <= {q49[23:0],1'b1}; 
			r50 <= kq_sub25; 
			d50 <= {1'b0, d49[47:1]}; 
			//valid_out <=1;
		end
	end
endmodule
