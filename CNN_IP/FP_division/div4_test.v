`timescale 1ns/1ps
module div4_test();

    reg clk,resetn,enable;
    reg [7:0] z;
    reg [3:0] d;
    wire [4:0] q;
    wire [7:0] s;
    integer count = 0;

    div_4 dut(clk,resetn,enable,z,d,q,s);
    always #2 clk = ~clk;

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) count = 0;
        else if (enable == 1'b1) begin 
            count = count + 1;
            $display("Clock count = %d ",count);
        end
        else count = count;
    end

    initial begin 
        $monitor("time = %d, clk=%b,z= %d, d = %d, q= %d, rem = %d",$time,clk,z,d,q,s);
        clk = 0;
        enable = 1;
        resetn = 1;
        //count = 0;
        //#2
        z <= 8'd16;
        d <= 4'd4;
        //#4 
        //z <= 8'd20;
        //d <= 4'd5;
        stimulus;
        #44 $finish();
    end

    task stimulus;
		begin
			repeat(20) begin
			#4 z<= $random;
            d <= $random;
            //ci <= $random;
	    end
    end
    endtask


//module div4bit(clk,resetn,enable,z,d,q,s);



endmodule