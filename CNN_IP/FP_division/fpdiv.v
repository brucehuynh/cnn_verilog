module fpdiv(clk,resetn,enable,valid_in,dividend,divisor,quotient,valid_out);
    input clk;
    input resetn;
    input enable;
    input valid_in;
    input [31:0] dividend;
    input [31:0] divisor;
    output [31:0] quotient;
    output valid_out;

    reg [31:0] dividendR,divisorR;
    reg s;
    reg [7:0] e;
    wire s_out;
    wire [7:0] e_out;
    wire [7:0] e_next;
    reg [7:0] e_final;
    //reg [23:0] m;
    wire [24:0] temp_q;
    reg [22:0] q;
    wire [47:0] temp_s;
    wire done_div;


    delay_clock #(.DATA_WIDTH(1), .N_CLOCKs(51)) delay_s(clk,resetn,enable,s,s_out);
    delay_clock #(.DATA_WIDTH(8), .N_CLOCKs(50)) delay_e(clk,resetn,enable,e,e_next);
    delay_clock #(.DATA_WIDTH(1), .N_CLOCKs(53)) delay_valid(clk,resetn,enable,valid_in,valid_out);
    assign quotient = (valid_out)?{s_out,e_final,q}:32'b0; 

    // xu ly input 
    always @ (posedge clk or negedge resetn) begin
       if (resetn == 1'b0) begin
            dividendR <= 32'b0;
            divisorR <= 32'b00111111100000000000000000000000;
       end
      else if(valid_in == 1'b1) begin
            dividendR <= dividend;
            divisorR <= divisor;
        end
        else if(valid_in == 1'b0) begin
            dividendR <= dividendR;
            divisorR <= divisorR;
        end
    end

    //sign
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) s <= 0;
        else if (enable == 1'b1) begin 
            s <= dividendR[31] ^ divisorR[31];
        end
        else if (enable == 1'b0) begin
            s <= s;
        end
    end
    //exp
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) e <= 0;
        else if (enable == 1'b1) begin 
            e = dividendR[30:23] - divisorR[30:23];
            e = e + 8'd127;
        end
        else if (enable == 1'b0) begin
            e <= e;
        end
    end

    //man
    
    divider m_div(clk,resetn,valid_in,{1'b1,dividendR[22:0],24'b0},{1'b1,divisorR[22:0]},temp_q,temp_s,done_div);

    always @ (posedge clk or negedge resetn) begin
        if (resetn==1'b0) begin
            e_final <= 0;
            q <= 0;
            //valid_out <= 1'b0;
        end
        else if (done_div == 1'b1 && temp_q[24] == 1'b0) begin 
            e_final <= e_next - 8'b1;
            if (temp_s == 0) begin
              q <= temp_q[22:0];
              //valid_out <= 1'b1;
            end 
            else begin
              q <= temp_q[22:0]+23'b1;
              //valid_out <= 1'b1;
            end 
        end
        else if (done_div == 1'b1 && temp_q[24] == 1'b1) begin
            e_final <= e_next;
            if (temp_s == 0) begin
              q <= temp_q[23:1];
              //valid_out <= 1'b1;
            end 
            else begin 
                q <= temp_q[23:1]+23'b1;
                //valid_out <= 1'b1;
                end
        end
        else begin
            e_final <= e_final;
            q <= q;
            //valid_out <= valid_out;
        end
    end
    

endmodule