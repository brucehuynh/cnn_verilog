// q = quotient, d = divisor, z = dividend, s = remain
module CAS_cell(clk,resetn,enable,q_in,z,d_in,cin,q_out,s,d_out);
    input clk;
    input resetn;
    input enable;
    input q_in;
    input z;
    input d_in;
    input cin;
    output q_out;
    output s;
    output d_out;

    xor xor1(x,q_in,d_in);
    addbit addbit1(x,z,cin,s,q_out);
    assign d_out = d_in;

endmodule