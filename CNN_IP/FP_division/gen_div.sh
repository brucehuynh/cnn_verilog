#!/bin/csh
set bits_d = `echo "$1-1" | bc`
set bits_z = `echo "2*$1-1" | bc`
set bits_q = "$1"
set bits = `echo "2*$1" | bc`
set numberof_sub = `echo "$1+1" | bc`
set numberof_clk = `echo "2*$1+2" | bc`


echo "module divider(clk,resetn,enable,z,d,q,s);" > divider.v
#echo "paramater DATA_WIDTH = $1;"
echo "input clk;"  >> divider.v
echo "input resetn;"  >> divider.v
echo "input enable;"  >> divider.v
echo "input [${bits_z}:0] z;" >> divider.v
echo "input [${bits_d}:0] d;" >> divider.v
echo "output [${bits_q}:0] q;" >> divider.v
echo "output [${bits_z}:0] s;" >> divider.v
echo "" >> divider.v
echo "reg [${bits_q}:0] quotient;" >> divider.v
echo "reg [${bits_z}:0] remainder;" >> divider.v
echo "reg [${bits_z}:0] divisor;" >> divider.v
echo "" >>divider.v
echo "" >>divider.v
# kq sub
printf "\twire [${bits_z}:0] " >> divider.v
set i = 1;
while ( $i <= $numberof_sub )
    printf "kq_sub${i}" >> divider.v
    if ($i == $numberof_sub) then
        printf ";" >> divider.v
    else 
        printf "," >> divider.v
    endif
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

# co sub
printf "\twire " >> divider.v
set i = 1;
while ( $i <= $numberof_sub )
    printf "co_sub${i}" >> divider.v
    if ($i == $numberof_sub) then
        printf ";" >> divider.v
    else 
        printf "," >> divider.v
    endif
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

# wire r d le
printf "\twire [${bits_z}:0] " >> divider.v
set i = 1;
while ( $i < $numberof_clk )
    if (`expr $i % 2` != 0 ) then
        printf "r${i},d${i}" >> divider.v
    else 
        printf "," >> divider.v
    endif
    if ($i == 49) printf ";" >> divider.v
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

# wire q le
printf "\twire [${bits_q}:0] " >> divider.v
set i = 1;
while ( $i < $numberof_clk )
    if (`expr $i % 2` != 0 ) then
        printf "q${i}" >> divider.v
    else 
        printf "," >> divider.v
    endif
    if ($i == `echo "$numberof_clk-1" | bc`) printf ";" >> divider.v
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

# reg q chan
printf "\treg [${bits_q}:0] " >> divider.v
set i = 2;
while ( $i <= $numberof_clk )
    if (`expr $i % 2` == 0 ) then
        printf "q${i}" >> divider.v
    else 
        printf "," >> divider.v
    endif
    if ($i == $numberof_clk) printf ";" >> divider.v
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

# reg d r chan
printf "\treg [${bits_z}:0] " >> divider.v
set i = 2;
while ( $i <= $numberof_clk )
    if (`expr $i % 2` == 0 ) then
        printf "d${i},r${i}" >> divider.v
    else 
        printf "," >> divider.v
    endif
    if ($i == $numberof_clk) printf ";" >> divider.v
    @ i++;
end
echo "" >>divider.v
echo "" >>divider.v

echo "assign q = q${numberof_clk};" >> divider.v
echo "assign s = r${numberof_clk};" >> divider.v
echo "" >>divider.v

echo "// init" >>divider.v
printf "\talways @ (posedge clk or negedge resetn) begin \n" >> divider.v
printf "\t\tif(resetn == 1'b0) begin \n" >> divider.v
printf "\t\t\tquotient <= 0; \n" >> divider.v
printf "\t\t\tremainder <= 0; \n" >> divider.v
printf "\t\t\tdivisor <= 0;  \n" >> divider.v
printf "\tend\n" >> divider.v
printf "\telse if (enable == 1'b1) begin\n" >> divider.v
printf "\t\t\tquotient <= 0; \n" >> divider.v
printf "\t\t\tremainder <= z; \n" >> divider.v
printf "\t\t\tdivisor <= {d,$1'b0}; \n" >> divider.v
printf "\tend\n" >> divider.v
printf "\telse begin\n" >> divider.v
printf "\t\t\tquotient <= quotient; \n" >> divider.v
printf "\t\t\tremainder <= remainder; \n" >> divider.v
printf "\t\t\tdivisor <= divisor; \n" >> divider.v
printf "\t\tend\n" >> divider.v
printf "\tend\n" >> divider.v

set i = 1;
set k = 1;
while ( $i <= $numberof_clk )
    set j = `echo "$i-1" |bc`;
    if ( $i == 1 ) then
        printf "\t//buffer $i\n" >> divider.v
        printf "\tnbit_dff #(${numberof_sub}) quotient_ff${i}(clk,resetn,enable,quotient,q${i});\n" >> divider.v
        printf "\tnbit_dff #(${bits}) divisor_ff${i}(clk,resetn,enable,divisor,d${i});\n" >> divider.v
        printf "\tnbit_dff #(${bits}) remainder_ff${i}(clk,resetn,enable,remainder,r${i});\n" >> divider.v
        printf "\t//sub $i\n" >> divider.v
        printf "\tsubtractor #(${bits}) sub_${i}(r${i},d${i},1'b0,kq_sub${i},co_sub${i});\n\n" >> divider.v
    else if (`expr $i % 2` == 0 ) then
        printf "\talways @ (posedge clk or negedge resetn) begin \n" >> divider.v
        printf "\t\tif(resetn == 1'b0) begin \n" >> divider.v
        printf "\t\t\tq${i} <= 0; \n" >> divider.v
        printf "\t\t\tr${i} <= 0; \n" >> divider.v
        printf "\t\t\td${i} <= 0;  \n" >> divider.v
        printf "\tend\n" >> divider.v
        printf "\telse if (co_sub${k} == 1'b1) begin\n" >> divider.v
        printf "\t\t\tq${i} <= {q${j}[${bits_d}:0],1'b0}; \n" >> divider.v
        printf "\t\t\tr${i} <= r${j}; \n" >> divider.v
        printf "\t\t\td${i} <= {1'b0, d${j}[${bits_z}:1]}; \n" >> divider.v
        printf "\tend\n" >> divider.v
        printf "\telse if (co_sub${k} == 1'b0) begin\n" >> divider.v
        printf "\t\t\tq${i} <= {q${j}[${bits_d}:0],1'b1}; \n" >> divider.v
        printf "\t\t\tr${i} <= kq_sub${k}; \n" >> divider.v
        printf "\t\t\td${i} <= {1'b0, d${j}[${bits_z}:1]}; \n" >> divider.v
        printf "\t\tend\n" >> divider.v
        printf "\tend\n" >> divider.v
    else 
        @ k++
        printf "\t//buffer $k\n" >> divider.v
        printf "\tnbit_dff #(${numberof_sub}) quotient_ff${i}(clk,resetn,enable,q${j},q${i});\n" >> divider.v
        printf "\tnbit_dff #(${bits}) divisor_ff${i}(clk,resetn,enable,d${j},d${i});\n" >> divider.v
        printf "\tnbit_dff #(${bits}) remainder_ff${i}(clk,resetn,enable,r${j},r${i});\n" >> divider.v
        printf "\t//sub $k\n" >> divider.v
        printf "\tsubtractor #(${bits}) sub_${k}(r${i},d${i},1'b0,kq_sub${k},co_sub${k});\n\n" >> divider.v
    endif
    @ i++;
end



echo "endmodule" >>divider.v
cat divider.v