module div_4(clk,resetn,enable,z,d,q,s);
    input clk;
    input resetn;
    input enable;
    input [7:0] z;
    input [3:0] d;
    output [4:0] q;
    output [7:0] s;

    reg [4:0] quotient;
    reg [7:0] remainder;
    reg [7:0] divisor;
    wire [7:0] kq_sub1,kq_sub2,kq_sub3,kq_sub4,kq_sub5;
    wire co_sub1,co_sub2,co_sub3,co_sub4,co_sub5;
    wire [7:0] d1,r1,d3,r3,d5,r5,d7,r7,d9,r9;
    //wire [7:0] z1,z3,z5,z7,z9;
    wire [4:0] q1,q3,q5,q7,q9;
    reg [4:0] q2,q4,q6,q8,q10;
    reg [7:0] r2,r4,r6,r8,r10,d2,d4,d6,d8,d10;

    assign q = q10;
    assign s = r10;

    //init
    always @ (posedge clk or negedge resetn) begin
      if(resetn == 1'b0) begin
        quotient <= 0;
        remainder <= 0;
        divisor <= 0;
      end
      else if(enable == 1'b1) begin
        quotient <= 0;
        divisor <= {d,4'b0};
        remainder <= z;
        //z0 <= z;
      end
      else begin
        quotient <= quotient;
        divisor <= divisor;
        remainder <= remainder;
      end
    end

    // buffer 1
    nbit_dff #(5) quotient_ff0(clk,resetn,enable,quotient,q1);
    nbit_dff #(8) divisor_ff0(clk,resetn,enable,divisor,d1);
    nbit_dff #(8) remainder_ff0(clk,resetn,enable,remainder,r1);
    //nbit_dff #(8) dividend_ff0(clk,resetn,enable,remainder,z1);

    // sub 1
    subtractor #(8) sub_1(r1,d1,1'b0,kq_sub1,co_sub1);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r2 <= 0;
            q2 <= 0;
            d2 <= 0;
            //z2 <= 0;
        end
        else if (co_sub1 == 1'b1) begin
            r2 <= r1;
            q2 <= {q1[3:0],1'b0};
            d2 <= {1'b0, d1[7:1]};
            //z2 <= z1;
        end
        else if (co_sub1 == 1'b0) begin
            r2 <= kq_sub1;
            q2 <= {q1[3:0],1'b1};
            d2 <= {1'b0, d1[7:1]};
            //z2 <= z1;
        end
        
    end

    // buffer 2
    nbit_dff #(5) quotient_ff1(clk,resetn,enable,q2,q3);
    nbit_dff #(8) divisor_ff1(clk,resetn,enable,d2,d3);
    nbit_dff #(8) remainder_ff1(clk,resetn,enable,r2,r3);
    //nbit_dff #(8) dividend_ff1(clk,resetn,enable,z2,z3);

    // sub 2
    subtractor #(8) sub_2(r3,d3,1'b0,kq_sub2,co_sub2);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r4 <= 0;
            q4 <= 0;
            d4 <= 0;
            //z4 <= 0;
        end
        else if (co_sub2 == 1'b1) begin
            r4 <= r3;
            q4 <= {q3[3:0],1'b0};
            d4 <= {1'b0,d3[7:1]};
            //z4 <= z3;
        end
        else if (co_sub2 == 1'b0) begin
            r4 <= kq_sub2;
            q4 <= {q3[3:0],1'b1};
            d4 <= {1'b0,d3[7:1]};
            //z4 <= z3;
        end
        
    end

    // buffer 3
    nbit_dff #(5) quotient_ff2(clk,resetn,enable,q4,q5);
    nbit_dff #(8) divisor_ff2(clk,resetn,enable,d4,d5);
    nbit_dff #(8) remainder_ff2(clk,resetn,enable,r4,r5);
    //nbit_dff #(8) dividend_ff2(clk,resetn,enable,z4,z5);

    // sub 3
    subtractor #(8) sub_3(r5,d5,1'b0,kq_sub3,co_sub3);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r6 <= 0;
            q6 <= 0;
            d6 <= 0;
            //z6 <= 0;
        end
        else if (co_sub3 == 1'b1) begin
            r6 <= r5;
            q6 <= {q5[3:0],1'b0};
            d6 <= {1'b0,d5[7:1]};
            //z6 <= z5;
        end
        else if (co_sub3 == 1'b0) begin
            r6 <= kq_sub3;
            q6 <= {q5[3:0],1'b1};
            d6 <= {1'b0,d5[7:1]};
            //z6 <= z5;
        end
        
    end

    // buffer 4
    nbit_dff #(5) quotient_ff3(clk,resetn,enable,q6,q7);
    nbit_dff #(8) divisor_ff3(clk,resetn,enable,d6,d7);
    nbit_dff #(8) remainder_ff3(clk,resetn,enable,r6,r7);
    //nbit_dff #(8) dividend_ff3(clk,resetn,enable,z6,z7);

    // sub 4
    subtractor #(8) sub_4(r7,d7,1'b0,kq_sub4,co_sub4);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r8 <= 0;
            q8 <= 0;
            d8 <= 0;
            //z8 <= 0;
        end
        else if (co_sub4 == 1'b1) begin
            r8 <= r7;
            q8 <= {q7[3:0],1'b0};
            d8 <= {1'b0,d7[7:1]};
            //z8 <= z7;
        end
        else if (co_sub4 == 1'b0) begin
            r8 <= kq_sub4;
            q8 <= {q7[3:0],1'b1};
            d8 <= {1'b0,d7[7:1]};
            //z8 <= z7;
        end
    end

    
    // buffer 5
    nbit_dff #(5) quotient_ff4(clk,resetn,enable,q8,q9);
    nbit_dff #(8) divisor_ff4(clk,resetn,enable,d8,d9);
    nbit_dff #(8) remainder_ff4(clk,resetn,enable,r8,r9);
    //nbit_dff #(8) dividend_ff4(clk,resetn,enable,z8,z9);

    // sub 5
    subtractor #(8) sub_5(r9,d9,1'b0,kq_sub5,co_sub5);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r10 <= 0;
            q10 <= 0;
            d10 <= 0;
            //z10 <= 0;
        end
        else if (co_sub5 == 1'b1) begin
            r10 <= r9;
            q10 <= {q9[3:0],1'b0};
            d10 <= {1'b0,d9[7:1]};
            //z10 <= z9;
        end
        else if (co_sub5 == 1'b0) begin
            r10 <= kq_sub5;
            q10 <= {q9[3:0],1'b1};
            d10 <= {1'b0,d9[7:1]};
            //z10 <= z9;
        end
    end

    /*
        // buffer 6
    nbit_dff #(5) quotient_ff5(clk,resetn,enable,q10,q11);
    nbit_dff #(8) divisor_ff5(clk,resetn,enable,d10,d11);
    nbit_dff #(8) remainder_ff5(clk,resetn,enable,r10,r11);
    //nbit_dff #(8) dividend_ff4(clk,resetn,enable,z8,z9);

    // sub 6
    subtractor #(8) sub_6(r11,d11,1'b0,kq_sub6,co_sub6);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r12 <= 0;
            q12 <= 0;
            d12 <= 0;
            //z10 <= 0;
        end
        else if (co_sub6 == 1'b1) begin
            r12 <= r11;
            q12 <= {q11[3:0],1'b0};
            d12 <= {1'b0,d11[7:1]};
            //z10 <= z9;
        end
        else if (co_sub6 == 1'b0) begin
            r12 <= kq_sub6;
            q12 <= {q11[3:0],1'b1};
            d12 <= {1'b0,d11[7:1]};
            //z10 <= z9;
        end
    end

            // buffer 7
    nbit_dff #(5) quotient_ff6(clk,resetn,enable,q12,q13);
    nbit_dff #(8) divisor_ff6(clk,resetn,enable,d12,d13);
    nbit_dff #(8) remainder_ff6(clk,resetn,enable,r12,r13);
    //nbit_dff #(8) dividend_ff4(clk,resetn,enable,z8,z9);

    // sub 7
    subtractor #(8) sub_7(r13,d13,1'b0,kq_sub7,co_sub7);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r14 <= 0;
            q14 <= 0;
            d14 <= 0;
            //z10 <= 0;
        end
        else if (co_sub7 == 1'b1) begin
            r14 <= r13;
            q14 <= {q13[3:0],1'b0};
            d14 <= {1'b0,d13[7:1]};
            //z10 <= z9;
        end
        else if (co_sub7 == 1'b0) begin
            r14 <= kq_sub7;
            q14 <= {q13[3:0],1'b1};
            d14 <= {1'b0,d13[7:1]};
            //z10 <= z9;
        end
    end

            // buffer 8
    nbit_dff #(5) quotient_ff7(clk,resetn,enable,q14,q15);
    nbit_dff #(8) divisor_ff7(clk,resetn,enable,d14,d15);
    nbit_dff #(8) remainder_ff7(clk,resetn,enable,r14,r15);
    //nbit_dff #(8) dividend_ff4(clk,resetn,enable,z8,z9);

    // sub 8
    subtractor #(8) sub_8(r15,d15,1'b0,kq_sub8,co_sub8);
    
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            r16 <= 0;
            q16 <= 0;
            d16 <= 0;
            //z10 <= 0;
        end
        else if (co_sub8 == 1'b1) begin
            r16 <= r15;
            q16 <= {q15[3:0],1'b0};
            d16 <= {1'b0,d15[7:1]};
            //z10 <= z9;
        end
        else if (co_sub8 == 1'b0) begin
            r16 <= kq_sub8;
            q16 <= {q15[3:0],1'b1};
            d16 <= {1'b0,d15[7:1]};
            //z10 <= z9;
        end
    end
    */
endmodule