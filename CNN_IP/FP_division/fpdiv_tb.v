module fpdiv_tb();
    reg clk;
    reg resetn;
    reg enable;
    reg valid_in;
    reg [31:0] dividend;
    reg [31:0] divisor;
    wire [31:0] quotient;
    wire valid_out;

    fpdiv dut(clk,resetn,enable,valid_in,dividend,divisor,quotient,valid_out);
    always #20 clk = ~clk;

    integer count=0;
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) count = 0;
        else if (enable == 1'b1) begin 
            count = count + 1;
            $display("Clock count = %d ",count);
        end
        else count = count;
    end
    //wire [31:0] a;
    //reg [31:0] x = 32'b00111101110011001100110011001101;
    //reg [31:0] y = 32'b00111101111001100110011001100110;
    //assign a = x^y;
    //xor x2(a,32'b00111101110011001100110011001101,32'b00111110011001100110011001100110);
    initial begin
        clk =0;
		resetn = 1;
		//enable = 0;
		enable =1;
        $monitor("clk = %b, dividend = %b, divisor= %b, out = %b, valid_out = %b ",clk,dividend,divisor,quotient,valid_out);
        divisor = 32'b01000001001000000000000000000000; // 10
        dividend = 32'b00111111100000000000000000000000; // 1
        valid_in = 1;
        
        #40
        dividend = 32'b00000100000000000000000100000000;
        #40
        divisor = 32'b10100000000000010000000000000000;
        #40
        dividend = 32'b01000100000000000000001000000000;
        
        #3000
        //$display("aaaa =%b",a);
        $finish();
    end

endmodule // fp_div_tb
