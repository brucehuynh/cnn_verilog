module div4bit(clk,resetn,enable,z,d,q,s);
    input clk;
    input resetn;
    input enable;
    input [6:0] z;
    input [3:0] d;
    output [3:0] q;
    output [3:0] s;

    wire [3:0] s1,s2,s3,s4;
    wire [3:0] q1,q2,q3,q4;
    wire [3:0] d1_out,d2_out,d3_out,d4_out;

    assign q = {q4[0],q3[0],q2[0],q1[0]};
    assign s = s4;
    //(clk,resetn,enable,q_in,z,d_in,q_out,s,d_out);
    genvar i;
    for (i = 3; i >= 0; i=i-1) begin: stage1
        if (i == 3) CAS_cell st1(clk,resetn,enable,1'b1,z[i],d[i],1'b1,q1[i],s1[i],d1_out[i]);
        else CAS_cell st1(clk,resetn,enable,1'b1,z[i],d[i],q1[i+1],q1[i],s1[i],d1_out[i]);
    end

    //genvar i;
    for (i = 3; i >= 0; i=i-1) begin: stage2
        if (i == 3) CAS_cell st2(clk,resetn,enable,q1[0],z[i+1],d1_out[i],q1[0],q2[i],s2[i],d2_out[i]);
        else CAS_cell st2(clk,resetn,enable,q1[0],s1[i],d1_out[i],q2[i+1],q2[i],s2[i],d2_out[i]);
    end

    //genvar i;
    for (i = 3; i >= 0; i=i-1) begin: stage3
        if (i == 3) CAS_cell st3(clk,resetn,enable,q2[0],z[i+2],d2_out[i],q2[0],q3[i],s3[i],d3_out[i]);
        else CAS_cell st3(clk,resetn,enable,q2[0],s2[i],d2_out[i],q3[i+1],q3[i],s3[i],d3_out[i]);
    end

    //genvar i;
    for (i = 3; i >= 0; i=i-1) begin: stage4
        if (i == 3) CAS_cell st4(clk,resetn,enable,q3[0],z[i+3],d3_out[i],q3[0],q4[i],s4[i],d4_out[i]);
        else CAS_cell st4(clk,resetn,enable,q3[0],s3[i],d3_out[i],q4[i+1],q4[i],s4[i],d4_out[i]);
    end
    


endmodule