/*
`include "cmpshift.v"
`include "buffer1.v"
`include "fpaddsub.v"
`include "buffer2.v"
`include "normalized.v"
`include "../Lib_Cells/delay_clock.v"
*/
module fpadd(clk,resetn,enable,valid_in,a,b,out,valid_out);
    input[31:0]a,b;
    input clk,resetn,enable,valid_in;
    output [31:0]out;
    output valid_out;

    wire [7:0] ex,ey,exy,ex1,ey1,ex2,ex3;
    wire s,s3,sr,sn,s4,sx1,sy1,sn1,sn2,sn3,sn4,sr1,sr2,sn5,sn6;
    wire [23:0] mx,my,mxy,mx1,my1;
    wire [24:0] mxy1,mxy2;

    reg s1,s2;
    reg [7:0] e1,e2;
    reg [23:0] m1,m2;
    reg iszero;
    wire iszero_1;
    //reg [2:0] counter;

    wire s2_n;
    not (s2_n,b[31]);
    //assign iszero = (in1 == 32'b0 || in2 == 32'b0)?1'b1:1'b0;

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            iszero <= 1'b0;
            s1 <= 0;
            s2 <= 0;
            e1 <= 0;
            e2 <= 0;
            m1 <= 0;
            m2 <= 0;
        end
        else if (valid_in == 1'b1) begin
            if({a[31],a[30:23],a[22:0]} == {s2_n,b[30:23],b[22:0]}) begin
                iszero <= 1'b1;
                s1 <= 0;
                s2 <= 0;
                e1 <= 0;
                e2 <= 0;
                m1 <= {1'b1,23'b0};
                m2 <= {1'b1,23'b0};
            end
            else if({a[31],a[30:23],a[22:0]} != {s2_n,b[30:23],b[22:0]})begin
                if (a == 32'b0 && b == 32'b0 == 1'b1) begin
                    iszero <= 1'b1;
                    s1 <= a[31];
                    s2 <= b[31];
                    e1 <= a[30:23];
                    e2 <= b[30:23];
                    m1 <= {1'b1,a[22:0]};
                    m2 <= {1'b1,b[22:0]};
                end
                else begin
                    iszero <= 1'b0;
                    s1 <= a[31];
                    s2 <= b[31];
                    e1 <= a[30:23];
                    e2 <= b[30:23];
                    m1 <= {1'b1,a[22:0]};
                    m2 <= {1'b1,b[22:0]};
                end
            end
        end
        else if (valid_in == 1'b0) begin
            iszero <= iszero;
            s1 <= s1;
            s2 <= s2;
            e1 <= e1;
            e2 <= e2;
            m1 <= m1;
            m2 <= m1;
        end
    end
    /*
    assign s1=(valid_in)?a[31]:1'bx;
    assign s2=(valid_in)?b[31]:1'bx;
    assign e1=(valid_in)?a[30:23]:8'bx;
    assign e2=(valid_in)?b[30:23]:8'bx;
    assign m1[23]=1'b1;
    assign m2[23]=1'b1;
    assign m1[22:0]=(valid_in)?a[22:0]:23'bx;
    assign m2[22:0]=(valid_in)?b[22:0]:23'bx;
    */
    delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(4)) zero_inst(clk,resetn,enable,iszero,iszero_1);
    delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(6)) valid_inst(clk,resetn,enable,valid_in,valid_out);
    /*
    and enable_counter_inst(enable_counter,enable,valid_in);

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            counter <= 3'b0;
        end
        else if (enable == 1'b1 ) begin
            if(valid_in == 1'b1) counter <= counter + 3'b1;
            else if (valid_in == 1'b0) counter <= counter;
        end
        else if (enable == 1'b0 ) begin
            counter <= counter;
        end
    end

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            valid_out <= 1'b0;
        end
        else if ()
        
    end
    */
    //submodule for compare and shfit
    cmpshift as(e1[7:0],e2[7:0],s1,s2,m1[23:0],m2[23:0],clk,resetn,enable,ex,ey,mx,my,s,sx1,sy1);
    buffer1 buff1(ex,ey,sx1,sy1,mx,my,s,clk,resetn,enable,ex1,ey1,mx1,my1,sn,sn1,sn2);
    //sub module for mantissa addition snd subtraction
    faddsub as1(mx1,my1,sn1,sn2,sn,ex1,clk,resetn,enable,mxy1,ex2,sn3,sn4,s3,sr1);
    buffer2 buff2(mxy1,s3,sr1,ex2,sn3,sn4,clk,resetn,enable,mxy2,ex3,sn5,sn6,s4,sr2);
    //sub module for normalization
    normalized as2(iszero_1,mxy2,sr2,sn5,sn6,s4,clk,resetn,enable,ex3,sr,exy,mxy);

    assign out=(valid_out==1'b1)?{sr,exy,mxy[22:0]}:32'b0;
endmodule
