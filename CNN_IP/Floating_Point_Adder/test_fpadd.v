`timescale 10ns/10ps
//`include "fpadd.v"
module test_fpadd();
    reg clk,resetn,enable;
    reg [31:0] a;
    reg [31:0] b;
    wire [31:0] out;
    reg valid_in;
    wire valid_out;

    fpadd axx(clk,resetn,enable,valid_in,a,b,out,valid_out);
    //(clk,resetn,enable,valid_in,a,b,out,valid_out);
    always #20 clk = ~clk;

    integer counter=0;

    always @ (posedge clk) begin
            counter = counter + 1;
            $display("Clock count: %d",counter);
        end

    initial begin
        clk =0;
		resetn = 1;
		valid_in = 1;
		enable =1;
		
        $monitor("clk = %b, a = %b, b= %b, out = %b, valid_out= %b",clk,a,b,out,valid_out);
        a = 32'b00000000000000000000000000000000;
        b = 32'b00000000000000000000000000000000;

        #40
        b = 32'b00000000000000000000000000000000;
        #40
        b = 32'b10100000000000010000000000000000;
        #40
        b = 32'b01000100000000000000001000000000;
        #600
        $finish();

    end


endmodule
