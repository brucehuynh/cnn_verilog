//`include ../Lib_Cells/dff.v
//`include "../Lib_Cells/nbit_dff.v"
module buffer1(ex,ey,sx1,sy1,mx,my,s,clk,resetn,enable,ex1,ey1,mx1,my1,sn,sn1,sn2);
    input [7:0]ex,ey;
    input [23:0]mx,my;
    input s,clk,enable,resetn,sx1,sy1;
	
	output wire [7:0]ex1,ey1;
    output wire [23:0]mx1,my1;
    output wire sn,sn1,sn2;
	
	nbit_dff #(1) m1(clk,resetn,enable,sx1,sn1);
	nbit_dff #(1) m2(clk,resetn,enable,sy1,sn2);
	nbit_dff #(8) m3(clk,resetn,enable,ex,ex1);
	nbit_dff #(8) m4(clk,resetn,enable,ey,ey1);
	nbit_dff #(24) m5(clk,resetn,enable,mx,mx1);
	nbit_dff #(24) m6(clk,resetn,enable,my,my1);
	nbit_dff #(1) m7(clk,resetn,enable,s,sn);
	
endmodule
