module cmpshift(e1,e2,s1,s2,m1,m2,clk,resetn,enable,ex,ey,mx,my,s,sx1,sy1); //module for copare &shift
    input [7:0]e1,e2;
    input [23:0]m1,m2;
    input clk,resetn,enable,s1,s2; // s1 s2 dấu của fp 
    output reg[7:0]ex,ey;          // mũ
    output reg[23:0]mx,my;			// mantiss
    output reg s,sx1,sy1;			// s tín hiệu cho biết mũ nào lớn hơn

    wire [7:0]diff,diff_2;
	wire carry,carry_2;

	subtractor #(8) sub_0(e1,e2,1'b0,diff,carry); //-127
	subtractor #(8) sub_1(e2,e1,1'b0,diff_2,carry_2); //-127

    always@(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) begin
			ex <=8'b0;
			ey <= 8'b0;
			mx <= 24'b0;
			my <= 24'b0;
			s <= 1'b0;
			sx1 <= 1'b0;
			sy1 <= 1'b0;
		end
		else if (enable == 1'b1) begin
			
			/*
			if(e1==e2) begin
				ex<=e1+8'b1;
				ey<=e2+8'b1;
				mx<=m1;
				my<=m2;
				s<=1'b1;
			end
			else */
			if(carry == 1'b0) begin
				//diff<=e1-e2;
				ex<=e1+8'b1;
				ey<=e1+8'b1;
				mx<=m1;
				my<=m2>>diff;
				s<=1'b1; 
				sx1<=s1;
				sy1<=s2;
			end
			else if(carry == 1'b1) begin
				//diff<=e2-e1;
				ex<=e2+8'b1;
				ey<=e2+8'b1;
				mx<=m1>>diff_2;
				my<=m2;
				s<=1'b0;
				sx1<=s1;
				sy1<=s2;
			end
		end
		else begin
			ex <= ex;
			ey <= ey;
			mx <= mx;
			my <= my;
			s <= s;
			sx1 <= sx1;
			sy1 <= sy1;
		end
    end
endmodule