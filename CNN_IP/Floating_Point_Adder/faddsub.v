module faddsub(a,b,s1,s2,sn,ex1,clk,resetn,enable,out,ex2,sn3,sn4,s,sr1); //submodule for addition or subtraction
    input [23:0]a,b;
    input[7:0]ex1;
	//;
    input s1,s2,clk,resetn,enable,sn;
    output reg [7:0]ex2;
    output reg [24:0]out;
    output reg s,sn3,sn4,sr1; 

	wire s_w;
	xor (s_w,s1,s2);

    always@(posedge clk or negedge resetn) begin
		if (resetn == 1'b0) begin
			ex2 <= 8'b0;
			out <= 25'b0;
			s <= 1'b0;
			sn3 <= 1'b0;
			sn4 <= 1'b0;	
			sr1 <= 1'b0;
		end else if (enable == 1'b1) begin
			ex2<=ex1;
			sr1<=sn;
			sn3<=s1;
			sn4<=s2;
			s<=s_w;
			if(s_w==1'b1) begin
				out<=a-b;
			end
			else begin
				out<=a+b;
			end
		end
		else begin
			ex2 <= ex2;
			out <= out;
			s <= s;
			sn3 <= sn3;
			sn4 <= sn4;
			sr1 <= sr1;
		end
    end
endmodule