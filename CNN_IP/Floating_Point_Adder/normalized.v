module normalized (iszero_1,mxy2,sr2,sn5,sn6,s4,clk,resetn,enable,ex3,sr,exy,mxy);
				//(mxy1,s,s1,s2,s3,clk,resetn,enable,ex,sr,exy,mxy);
	input iszero_1;
    input[24:0]mxy2;
    input sr2,sn5,sn6,s4,clk,resetn,enable; // s4 for cùng dấu
    input[7:0]ex3;
    output reg sr;
    output reg[7:0]exy;
    output reg[23:0]mxy;

    reg [24:0]mxy3;

    always@(posedge clk or negedge resetn) begin
		if (resetn == 1'b0) begin
			sr <= 1'b0;
			exy <= 8'b0;
			mxy <= 24'b0;
		end 
		else if (enable == 1'b1) begin
			if (iszero_1 == 1'b1) begin
				sr <= 1'b0;
				exy <= 8'b0;
				mxy <= 24'b0;
			end
			else begin
				sr=sr2?sn5^(mxy2[24]&s4):sn6^(mxy2[24]&s4);
				mxy3=(mxy2[24]&s4)?~mxy2+25'b1:mxy2;
				mxy=mxy3[24:1];
				exy=ex3;
				/*
				if(mxy[23]&mxy[22] == 1'b1)  //here
					exy = ex + 8'b1;
				else exy=ex;
				*/
				repeat(24)
				begin
				if(mxy[23]==1'b0) begin
					mxy=mxy<<1'b1;
					exy=exy-8'b1;
					end
				end
			end
		end else begin
			sr <= sr;
			exy <= exy;
			mxy <= mxy;
		end
    end
endmodule