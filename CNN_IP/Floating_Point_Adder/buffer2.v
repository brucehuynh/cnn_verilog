//`include "../Lib_Cells/nbit_dff.v"
module buffer2(mxy1,s3,sr1,ex,sn3,sn4,clk,resetn,enable,mxy2,ex3,sn5,sn6,s4,sr2);
    input [24:0]mxy1;
    input s3,clk,resetn,enable,sr1,sn3,sn4;
    input [7:0]ex;
	/*
    output reg[24:0]mxy2;
    output reg[7:0]ex3;
    output reg s4,sn5,sn6,sr2;

    always@(posedge clk) begin
        sr2=sr1;
        sn5=sn3;
        sn6=sn4;
        ex3=ex;
        mxy2=mxy1;
        s4=s3;
    end
	*/
	output wire [24:0]mxy2;
    output wire [7:0]ex3;
    output wire s4,sn5,sn6,sr2;
	
	nbit_dff # (1) m1(clk,resetn,enable,sr1,sr2);
	nbit_dff # (1) m2(clk,resetn,enable,sn3,sn5);
	nbit_dff # (1) m3(clk,resetn,enable,sn4,sn6);
	nbit_dff # (8) m4(clk,resetn,enable,ex,ex3);
	nbit_dff #(25) m5(clk,resetn,enable,mxy1,mxy2);
	nbit_dff # (1) m6(clk,resetn,enable,s3,s4);
	
endmodule
