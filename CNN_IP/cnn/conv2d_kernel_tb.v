module conv2d_kernel_tb();
reg clk,resetn,enable;
wire [31:0] is0,is1,is2,is3,is4,is5,is6,is7,is8,is9;
wire stop;
conv2d_kernel_test inst(clk,resetn,enable,is0,is1,is2,is3,is4,is5,is6,is7,is8,is9,stop);

always @ (stop) begin
	if (stop == 1'b1) $finish();
end

always #1 clk = ~clk;
initial begin
		clk = 0;
		resetn = 0;
	 	enable = 0;		
	#10	resetn = 1;
		enable = 1;
end

endmodule