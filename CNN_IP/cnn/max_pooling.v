module max_pooling #(
    parameter DATA_WIDTH = 32,
    parameter IMG_WIDTH = 28,
    parameter IMG_HEIGHT = 28
)(
    clk,
    enable,
    resetn,
    valid_in,
    //enable_count,
    data_in,
    data_out,
    valid_out,
    done
);
    input clk;
    input enable;
    input resetn;
    input valid_in;
    
    input [DATA_WIDTH-1:0] data_in;
    output [DATA_WIDTH-1:0] data_out;
    output reg valid_out;
    output reg done;

    // declare
    //reg [15:0]           enable_buffer;
    reg enable_count;
    reg [DATA_WIDTH-1:0] data;
    reg  counter_h;
    reg [5:0] counter_v;
    reg [15:0] counter_2;
    reg [15:0] counter_1;
    reg [15:0] counter_3;
    reg valid_max;
    wire [DATA_WIDTH-1:0] w1,w2,w3,w4;
    wire valid_out_max;
    wire [31:0] data_out_max;

    assign w4 = data;

    always @ (posedge clk or negedge resetn) begin // count for done signal
        if(resetn == 1'b0) counter_2 <= 0;
        else if (valid_out == 1'b1) begin
            counter_2 <= counter_2 + 1;
        end
        else begin
            counter_2 <= counter_2;
        end
    end
    always @(posedge clk or negedge resetn) begin
        if (resetn == 1'b0) done <= 1'b0;
        else if(counter_2 == (IMG_HEIGHT>>1)*(IMG_WIDTH>>1)-1 ) begin
            done <= 1'b1;
        end
        else begin
            done <= done;
        end
    end
    
    always @ (posedge clk or negedge resetn) begin // count for output valid
        if(resetn == 1'b0) counter_3 <= 0;
        else if (valid_out_max == 1'b1) begin
            counter_3 <= counter_3 + 1;
        end
        else begin
            counter_3 <= counter_3;
        end
    end

    
    always @(posedge clk or negedge resetn) begin
        if (resetn == 1'b0) valid_out <= 1'b0;
        else if(counter_3 == (IMG_WIDTH>>1)*(IMG_HEIGHT>>1)) begin
            valid_out <= 1'b1;
        end
        else begin
            valid_out <= valid_out;
        end
    end
    

    // xet tin hieu valid
    always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) data <= 32'b0;
		else if(valid_in == 1'b1) data <= data_in;
		else data <= data;
	end

    always @(posedge clk or negedge resetn) begin // day buffer 1 input
		if(resetn == 1'b0) counter_1 <= 0;
        else if(enable == 1'b1 ) begin
            if (valid_in == 1'b1)begin
              counter_1 <= counter_1 + 1;
            end
            else counter_1 <= 0;
        end
        else counter_1 <= counter_1;
    end
    //enable counter for max pool
    always @(posedge clk or negedge resetn) begin 
		if(resetn == 1'b0) enable_count <= 1'b0;
        else if(valid_in == 1'b1) begin
            if(counter_1 == IMG_WIDTH-1) begin
                enable_count <= 1'b1;
            end
            else begin 
                enable_count <= enable_count;
            end
        end
        else if(valid_in == 1'b0)
            enable_count <= enable_count;
	end


    //counter for horizontal 
    always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) counter_h <= 1'b0;
        else if(enable_count == 1'b1) begin
            if(counter_h == 1'b1) begin
                counter_h <= 0;
            end
            else begin 
                counter_h <= counter_h + 1'b1;
            end
        end
        else if(enable_count == 1'b0) counter_h <= counter_h;
	end

   

    //counter for vertical 
    always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) counter_v <= 6'b0;
        else if(enable_count == 1'b1) begin
            if(counter_v == 2*IMG_WIDTH -1  ) begin
                counter_v <= 6'b0;
            end
            else begin
              counter_v <= counter_v + 6'b1;
            end
        end
        else if(enable_count == 1'b0) counter_v <= counter_v;
	end

    // max enable signal
    always @(posedge clk or negedge resetn) begin
        if (resetn == 1'b0) valid_max <= 1'b0;
        else if(counter_v <= IMG_WIDTH -1 && counter_h == 1'b1) begin
            valid_max <= 1'b1;
        end
        else begin
            valid_max <= 1'b0;
        end
    end

    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_1(
                    clk,
                    resetn,
                    enable,
                    w4,
                    w2
                    );

    nbit_dff #(DATA_WIDTH) 
                    dff_1(
                    clk,
                    resetn,
                    enable,
                    w4,
                    w3
                    );

    nbit_dff #(DATA_WIDTH) 
                    dff_2(
                    clk,
                    resetn,
                    enable,
                    w2,
                    w1
                    );

    max2 max_inst(
        clk,
        enable,
        resetn,
        valid_max,
        w1,
        w2,
        w3,
        w4,
        data_out_max,
        valid_out_max
        );     
    
    //wire [31:0] temp; // luu tam gia tri cua max pool
    //nbit_dff #(32) temp_inst(clk,resetn,valid_out_max,data_out_max,temp);
    /*
    or (enable_lb2,valid_out_max,done);
    */

    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH((IMG_WIDTH>>1)*(IMG_HEIGHT>>1))) 
                  lb_2(
                    clk,
                    resetn,
                    valid_out_max|valid_out,
                    data_out_max,
                    data_out
                    );  
    


endmodule