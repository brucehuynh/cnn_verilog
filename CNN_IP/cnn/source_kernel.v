////////////////////////////////////////////
//  MODULE: SOURCE
//  AUTHOR: VirosTeam
//  Created on Mar 18, 2017
////////////////////////////////////////////

module source_kernel #(
	parameter     DATA_WIDTH = 8		// 8 IS GREY_IMAGE || 24 IS RGB IMAGE
	)
	(	
	input    wire                        clk,                // Clock signal
	input    wire                        rst,                // Reset signal
	input    wire                        enable,              // Enable signal
	input	 wire						 load_bias_done,
	input	 wire						 load_kernel_done,
	input	 wire						 load_weight_done,
	output   reg     [DATA_WIDTH-1:0]    data,               // Data per one pixel of image
	output   reg                         data_valid_in,      // That signal confirm that data output are valid
	output	 reg	  [31:0]			 k1,
	output	 reg	  [31:0]			 k2,
	output	 reg	  [31:0]			 k3,
	output	 reg	  [31:0]			 k4,
	output	 reg	  [31:0]			 k5,
	output	 reg	  [31:0]			 k6,
	output	 reg	  [31:0]			 k7,
	output	 reg	  [31:0]			 k8,
	output	 reg	  [31:0]			 k9,
	output	 reg	  [31:0]			 bk,
	output	 reg	  [31:0]			 b0,
	output	 reg	  [31:0]			 b1,
	output	 reg	  [31:0]			 b2,
	output	 reg	  [31:0]			 b3,
	output	 reg	  [31:0]			 b4,
	output	 reg	  [31:0]			 b5,
	output	 reg	  [31:0]			 b6,
	output	 reg	  [31:0]			 b7,
	output	 reg	  [31:0]			 b8,
	output	 reg	  [31:0]			 b9,
	output	 reg	  [31:0]			 w0,
	output	 reg	  [31:0]			 w1,
	output	 reg	  [31:0]			 w2,
	output	 reg	  [31:0]			 w3,
	output	 reg	  [31:0]			 w4,
	output	 reg	  [31:0]			 w5,
	output	 reg	  [31:0]			 w6,
	output	 reg	  [31:0]			 w7,
	output	 reg	  [31:0]			 w8,
	output	 reg	  [31:0]			 w9,
	output	 reg						 load_kernel,
	output	 reg						 load_bias,
	output	 reg						 load_weight
	);

	reg     [15:0]              width;              // The width of image input
	reg     [15:0]              high;               // The high of image input
	reg     [31:0]              frames;             // Number of frames
	integer     data_file;               // Return value of file that open
	integer     counter=0;               // Counter
	integer getwidth;
	integer getheight;
	integer getframes;
	integer getdata;
	integer getKernel;
	integer getbias;
	integer getweight;
	integer kernel_file;
	integer bias_file;
	integer w0_file;
	integer w1_file;
	integer w2_file;
	integer w3_file;
	integer w4_file;
	integer w5_file;
	integer w6_file;
	integer w7_file;
	integer w8_file;
	integer w9_file;
	
	reg    [31:0]   eor;                 // Define the value that end of reading file
	
	wire enable_pixel;
	
	and and_1(enable_pixel,load_bias_done,load_kernel_done,load_weight_done);
	
	// Initial reading file text_image
	initial begin
		data_file = $fopen("number9.txt", "r"); // Open file with read access
	    kernel_file = $fopen("kernel.txt","r");
		bias_file = $fopen("bias.txt","r");
		w0_file = $fopen("W0.txt","r");
		w1_file = $fopen("W1.txt","r");
		w2_file = $fopen("W2.txt","r");
		w3_file = $fopen("W3.txt","r");
		w4_file = $fopen("W4.txt","r");
		w5_file = $fopen("W5.txt","r");
		w6_file = $fopen("W6.txt","r");
		w7_file = $fopen("W7.txt","r");
		w8_file = $fopen("W8.txt","r");
		w9_file = $fopen("W9.txt","r");
	    // Check file is located or not
			if (data_file == 0)
				begin
						$display("File image text isnt found");	
						$finish;
				end
			if (kernel_file == 0)
				begin
						$display("File kernel isnt found");	
						$finish;
				end
			if (bias_file == 0)
				begin
						$display("File bias isnt found");	
						$finish;
				end
			if (w0_file == 0)
				begin
						$display("File weight 0 isnt found");	
						$finish;
				end	
			if (w1_file == 0)
				begin
						$display("File weight 1 isnt found");	
						$finish;
				end	
			if (w2_file == 0)
				begin
						$display("File weight 2 isnt found");	
						$finish;
				end	
			if (w3_file == 0)
				begin
						$display("File weight 3 isnt found");	
						$finish;
				end	
			if (w4_file == 0)
				begin
						$display("File weight 4 isnt found");	
						$finish;
				end	
			if (w5_file == 0)
				begin
						$display("File weight 5 isnt found");	
						$finish;
				end	
			if (w6_file == 0)
				begin
						$display("File weight 6 isnt found");	
						$finish;
				end	
			if (w7_file == 0)
				begin
						$display("File weight 7 isnt found");	
						$finish;
				end	
			if (w8_file == 0)
				begin
						$display("File weight 8 isnt found");	
						$finish;
				end	
			if (w9_file == 0)
				begin
						$display("File weight 9 isnt found");	
						$finish;
				end	
		if (load_kernel == 1'b1)  $fclose(kernel_file);
		if (load_bias == 1'b1)  $fclose(bias_file);
		if (load_weight_done == 1'b1)  begin 
			$fclose(w0_file);
			$fclose(w1_file);
			$fclose(w2_file);
			$fclose(w3_file);
			$fclose(w4_file);
			$fclose(w5_file);
			$fclose(w6_file);
			$fclose(w7_file);
			$fclose(w8_file);
			$fclose(w9_file);
		end
	 end
	 
	// Always procedural
	always @(posedge clk or negedge rst) begin
  // Reset signal
		if (rst == 1'b0) begin
			data <= 0;
			data_valid_in <= 0;
			width <= 0;
			high <= 0;
			frames <= 0;
			eor <=0;
		end
  // Do if enable are high
		else if(enable_pixel == 1'b1) begin
			counter <= counter + 1'b1;              // Inscrese Counter
			if (counter==1) begin                  // Reading width of image value
				getwidth = $fscanf(data_file, "%d", width);     // Store width value into var "width"
				$display("WIDTH = %d", width);       // Display width to transcript
				eor <= 32'b0;                          // Set eor = 0
				end
			else if (counter==2) begin             // Reading high of image value
				getheight = $fscanf(data_file, "%d", high);      // Store high value into var "high"
				$display("HIGH = %d", high);         // Display high to transcript
				eor <= 32'b0;                          // Set eor = 0
				end
			else if (counter==3) begin             // Reading the number of frame of input
				getframes = $fscanf(data_file, "%d", frames);    // Store value into var "frames"
				$display("FRAMES = %d", frames);				 // Display frames to transcipt
				eor <= width*high*frames+3;           // input are GREY_IMAGE 
				end
			else if (counter > 3 && counter <= eor)begin
			//`ifdef GREY_IMAGE = 1
				getdata = $fscanf(data_file, "%d", data);      // 8 bits per once reading (GREY_IMAGE)
				if(getdata > 0) /*$display("DATA= %d", data)*/;
				else begin 
						$display("-----------------------Cant read data");
						$fclose	(data_file);
				end
			end
			// Set the signal notify that data is valid for output
			data_valid_in <= (counter > eor || (counter < 4)) ? 0 : 1 ;
		end
		
	end

	always @(posedge clk or negedge rst) begin
  // Reset signal
		if (rst == 1'b0) begin
			b0 <= 32'b0;
			b1 <= 32'b0;
            b2 <= 32'b0;
            b3 <= 32'b0;
            b4 <= 32'b0;
            b5 <= 32'b0;
            b6 <= 32'b0;
    	    b7 <= 32'b0;
          	b8 <= 32'b0;
        	b9 <= 32'b0;
			getbias <= 0;
			load_bias <= 1'b0;
		end
		else if(rst == 1'b1) begin
			if (enable == 1'b1)begin
				getbias <= getbias+1;
				if (getbias == 1)begin
					$fscanf(bias_file, "%b", b0);
					$fscanf(bias_file, "%b", b1);
					$fscanf(bias_file, "%b", b2);
					$fscanf(bias_file, "%b", b3);
					$fscanf(bias_file, "%b", b4);
					$fscanf(bias_file, "%b", b5);
					$fscanf(bias_file, "%b", b6);
					$fscanf(bias_file, "%b", b7);
					$fscanf(bias_file, "%b", b8);
					$fscanf(bias_file, "%b", b9);
					load_bias <= 1'b1;
				end
				else begin
					b0 <= b0;
					b1 <= b1;
					b2 <= b2;
					b3 <= b3;
					b4 <= b4;
					b5 <= b5;
					b6 <= b6;
					b7 <= b7;
					b8 <= b8;
					b9 <= b9;
					load_bias <= 1'b0;
				end
				//load_kernel <= (getKernel == 1) ? 1 : 0 ;
			end
			else begin
				b0 <= b0;
				b1 <= b1;
				b2 <= b2;
				b3 <= b3;
				b4 <= b4;
				b5 <= b5;
				b6 <= b6;
				b7 <= b7;
				b8 <= b8;
				b9 <= b9;
				load_bias <= load_bias;
			end
		end
	end
	
	always @(posedge clk or negedge rst) begin
  // Reset signal
		if (rst == 1'b0) begin
			k1 <= 32'b0;
            k2 <= 32'b0;
            k3 <= 32'b0;
            k4 <= 32'b0;
            k5 <= 32'b0;
            k6 <= 32'b0;
    	    k7 <= 32'b0;
          	k8 <= 32'b0;
        	k9 <= 32'b0;
			bk <= 32'b0;
			getKernel <= 0;
			load_kernel <= 1'b0;
		end
		else if(rst == 1'b1) begin
			if (enable == 1'b1)begin
				getKernel <= getKernel+1;
				if (getKernel == 1)begin
					$fscanf(kernel_file, "%b", k1);
					$fscanf(kernel_file, "%b", k2);
					$fscanf(kernel_file, "%b", k3);
					$fscanf(kernel_file, "%b", k4);
					$fscanf(kernel_file, "%b", k5);
					$fscanf(kernel_file, "%b", k6);
					$fscanf(kernel_file, "%b", k7);
					$fscanf(kernel_file, "%b", k8);
					$fscanf(kernel_file, "%b", k9);
					$fscanf(kernel_file, "%b", bk);
					load_kernel <= 1'b1;
				end
				else begin
					k1 <= k1;
					k2 <= k2;
					k3 <= k3;
					k4 <= k4;
					k5 <= k5;
					k6 <= k6;
					k7 <= k7;
					k8 <= k8;
					k9 <= k9;
					bk <= bk;
					load_kernel <= 1'b0;
				end
				//load_kernel <= (getKernel == 1) ? 1 : 0 ;
			end
			else begin
				k1 <= k1;
				k2 <= k2;
				k3 <= k3;
				k4 <= k4;
				k5 <= k5;
				k6 <= k6;
				k7 <= k7;
				k8 <= k8;
				k9 <= k9;
				bk <= bk;
				load_kernel <= load_kernel;
			end
		end
	end
	always @(posedge clk or negedge rst) begin
  // Reset signal
		if (rst == 1'b0) begin
			w0 <= 32'b0;
			w1 <= 32'b0;
            w2 <= 32'b0;
            w3 <= 32'b0;
            w4 <= 32'b0;
            w5 <= 32'b0;
            w6 <= 32'b0;
    	    w7 <= 32'b0;
          	w8 <= 32'b0;
        	w9 <= 32'b0;
			getweight <= 0;
			load_weight <= 1'b0;
		end
		else if(rst == 1'b1) begin
			if (enable == 1'b1)begin
				if (getweight < 169)begin
					getweight <= getweight+1;
					$fscanf(w0_file, "%b", w0);
					$fscanf(w1_file, "%b", w1);
					$fscanf(w2_file, "%b", w2);
					$fscanf(w3_file, "%b", w3);
					$fscanf(w4_file, "%b", w4);
					$fscanf(w5_file, "%b", w5);
					$fscanf(w6_file, "%b", w6);
					$fscanf(w7_file, "%b", w7);
					$fscanf(w8_file, "%b", w8);
					$fscanf(w9_file, "%b", w9);
					load_weight <= 1'b1;
				end
				else begin
					getweight <= getweight;
					w0 <= w0;
					w1 <= w1;
					w2 <= w2;
					w3 <= w3;
					w4 <= w4;
					w5 <= w5;
					w6 <= w6;
					w7 <= w7;
					w8 <= w8;
					w9 <= w9;
					load_weight <= 1'b0;
				end
				//load_kernel <= (getKernel == 1) ? 1 : 0 ;
			end
			else begin
				w0 <= w0;
				w1 <= w1;
				w2 <= w2;
				w3 <= w3;
				w4 <= w4;
				w5 <= w5;
				w6 <= w6;
				w7 <= w7;
				w8 <= w8;
				w9 <= w9;
				load_weight <= load_weight;
			end
		end
	end
endmodule
