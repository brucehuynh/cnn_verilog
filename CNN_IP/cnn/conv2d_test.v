module conv2d_test(clk,resetn,enable);
    parameter WIDTH = 6;
    parameter HEIGHT = 6;
    input clk;
    input resetn;
    input enable;

    wire [7:0] data_img;
    wire valid_source;
    wire valid_out;
    wire [31:0] data_out;
	wire [31:0] data_conv;
	wire done;
    //wire valid_out_pixel1;
    wire valid_out_pixel;

    /*
    controller #(.WIDTH(WIDTH),.HEIGHT(HEIGHT)) 
            controller_ins (
                clk,
                resetn,
                enable,
                enable_cvt,
                valid_out
                //valid_out_pixel1
                );
    */
    source #(8) 
        source_inst(
            clk,
            resetn,
            enable,
            data_img,
            valid_source
            );

    conv2d #(.DATA_WIDTH(8),.IMG_WIDTH(WIDTH),.IMG_HEIGHT(HEIGHT)) 
            conv2d_inst(
                    clk,
                    resetn,
                    enable,
                    valid_source,
                    data_img,
                    //enable_cvt,
                    data_conv,
                    valid_out_pixel,
                    valid_out_a
                    );
	truncate #(.WIDTH(WIDTH),.HEIGHT(HEIGHT))
			truncate_ins(
				clk,
				resetn,
				valid_out_pixel,
				data_conv,
				data_out,
				valid_out,
				done
			);

    sink #(.DATA_WIDTH(32),.IMG_WIDTH(WIDTH),.IMG_HEIGHT(HEIGHT))
            sink_inst(
                clk,
                resetn,
                enable,
                data_out,
                valid_out,
                done
            );

endmodule
