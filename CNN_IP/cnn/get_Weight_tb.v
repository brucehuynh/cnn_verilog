`timescale 1ns/1ps
module get_Weight_tb (input clk,
	input resetn,
	input enable,
	input valid_in,
	input [31:0] data_in,
	output [31:0] data_out,
	output valid_out);
    

    get_Weight # (
		.Filename("W2.txt"),
		.WIDTH(14), // after maxpool
		.HEIGHT(14) // after maxpool
		) dut	(clk,resetn,enable,valid_in,data_in,data_out,valid_out);

    
    //reg [9:0] counter;
	
    
	/*
    always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) begin
            counter <= 0;
		
		end
		else if (enable == 1'b1) begin
			counter <= counter + 10'b1;
			
			
			//valid_in <= (counter == 14*14)? 1'b0 :1'b1;
		end
		else begin
			counter <= counter;
			//Weight <= Weight;
			//valid_in <= valid_in;
		end
	end
	*/
endmodule