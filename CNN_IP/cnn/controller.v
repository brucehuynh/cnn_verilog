module controller(
    input clk,
    input resetn,
    input enable,
    output reg enable_cvt,
    output reg valid_out
    //output reg valid_pixel
);
    parameter WIDTH = 30;
    parameter HEIGHT = 30;

    integer counter;

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            counter = 64'b0;
            //enable_cvt = 1'b0;
            //valid_out = 1'b0;
        end
        else if (enable == 1'b1) begin
            counter = counter + 64'b1;
        end
        else if (enable == 1'b0) begin
            counter = counter;
        end
    end 

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            enable_cvt = 1'b0; 
        end
        else if(/*counter >= 2*WIDTH + 10*/counter >= WIDTH +8) begin //8 // + 5
            enable_cvt = 1'b1;
        end
        else begin
            enable_cvt = enable_cvt;
        end
    end 
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            valid_out = 1'b0; 
        end
        else if(counter == (WIDTH)*(HEIGHT) + 2*WIDTH + 85) begin // 8 + 1 +47 +20 +1
            valid_out = 1'b1;
        end
        else begin
            valid_out = valid_out;
        end
    end 
    /*
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            valid_pixel = 1'b0; 
        end
        else if(counter >=  2*WIDTH + 68) begin
            valid_pixel = 1'b1;
        end
        else begin
            valid_pixel = valid_pixel;
        end
    end 
    */
    

endmodule
