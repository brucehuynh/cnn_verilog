module truncate #(
    parameter WIDTH = 28,
    parameter HEIGHT = 28
	)
	(
	clk, 
	resetn,
	enable, 
	data_conv,
	data_truncate,
	valid_out,
	done
	);
	input clk;
	input resetn;
	input enable;
	input [31:0] data_conv;
	output [31:0] data_truncate;
	output reg valid_out;
	output reg done;
	
	reg [4:0] count_v,count_h;
	reg [9:0] counter_2;
	reg [9:0] counter_3;
	//reg [31:0] data_out;
	wire enable_buffer;
	
	//counter vertical
	always @ (posedge clk or negedge resetn) begin
		if(resetn == 1'b0) count_v <= 5'b0;
		else if (count_h == WIDTH-1) begin
				if( count_v == WIDTH-1) count_v <= 5'b0;
				else if (count_v != WIDTH-1) count_v <= count_h + 5'b1;
			end
		else if (count_h != WIDTH-1) count_v <= count_v;
	end
	//counter horizontal
	always @ (posedge clk or negedge resetn) begin
		if(resetn == 1'b0) count_h <= 5'b0;
		else if (enable == 1'b1) begin
				if( count_h == WIDTH-1) count_h <= 5'b0;
				else if (count_h != WIDTH-1) count_h <= count_h + 5'b1;
			end
		else if (enable == 1'b0) begin
				count_h <= count_h;
			end
	end
	//enable buffer out
	/*
	always @ (posedge clk or negedge resetn) begin
		if(resetn == 1'b0) enable_buffer <= 1'b0;
		else if(count_h == 5'b0 | count_h == WIDTH-1 | count_v == 5'b0 | count_v == HEIGHT-1)
			enable_buffer <= 1'b0;
		else enable_buffer <= 1'b1;
	end
	*/
	assign enable_buffer = (count_h == 5'b0 | count_h == WIDTH-1 | count_v == 5'b0 | count_v == HEIGHT-1)?1'b0:1'b1;
	// count for output valid
	always @ (posedge clk or negedge resetn) begin 
        if(resetn == 1'b0) counter_3 <= 0;
        else if (enable_buffer == 1'b1) begin
            counter_3 <= counter_3 + 10'b1;
        end
        else begin
            counter_3 <= counter_3;
        end
    end
	
	always @(posedge clk or negedge resetn) begin
        if (resetn == 1'b0) valid_out <= 1'b0;
        else if(counter_3 == ((WIDTH-2))*((HEIGHT-2)) -1 ) begin
            valid_out <= 1'b1;
        end
        else begin
            valid_out <= valid_out;
        end
    end
	
	// count for output done
	always @ (posedge clk or negedge resetn) begin 
        if(resetn == 1'b0) counter_2 <= 0;
        else if (valid_out == 1'b1) begin
            counter_2 <= counter_2 + 10'b1;
        end
        else begin
            counter_2 <= counter_2;
        end
    end
	
	always @(posedge clk or negedge resetn) begin
        if (resetn == 1'b0) done <= 1'b0;
        else if(counter_2 == ((WIDTH-2))*((HEIGHT-2))-1) begin
            done <= 1'b1;
        end
        else begin
            done <= done;
        end
    end
	
	line_buffer #(.DATA_WIDTH(32),.IMG_WIDTH((WIDTH-2)*(HEIGHT-2))) 
                  lb_2(
                    clk,
                    resetn,
                    enable_buffer|valid_out,
                    data_conv,
                    data_truncate
                    ); 
	
endmodule