`timescale 1ns/1ps
module gwtb();

reg clk;
	reg resetn;
	reg enable;
	reg valid_in;
	reg [31:0] data_in;
	wire [31:0] data_out;
	wire valid_out;

get_Weight_tb dut11 (clk,
	resetn,
	enable,
	valid_in,
	data_in,
	data_out,
	valid_out);
    

initial begin
            clk = 0;
            resetn = 0;
			data_in <= 32'b00111111100000000000000000000000;
            #10 resetn =1;
            enable = 1;
			valid_in =1;
            $monitor("data_out = %b,valid out = %b",data_out,valid_out);
            # 10000 $finish();
	end
    always #1 clk = ~clk;
endmodule 