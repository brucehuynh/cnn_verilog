module lines #(parameter DATA_WIDTH = 8,
                parameter IMG_WIDTH = 299)
        (clk,resetn,enable,data_valid_in,data_in,w1,w2,w3,w4,w5,w6,w7,w8,w9,valid_out);
    input clk;
    input resetn;
    input enable;
    input data_valid_in;
    
    //input valid_in;
    input [DATA_WIDTH-1:0] data_in;

    output [31:0] w1;
    output [31:0] w2;
    output [31:0] w3;
    output [31:0] w4;
    output [31:0] w5;
    output [31:0] w6;
    output [31:0] w7; 
    output [31:0] w8;
    output [31:0] w9;
    output valid_out;

    

    wire [DATA_WIDTH-1:0] a1,a2,a3,a4,a5,a6,a7,a8,a9;
    wire [8:0] valid_out_cvt2float;

    reg [7:0] counter;
    reg [DATA_WIDTH-1:0] data;
    

    always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) data <= 8'b0;
		else if(data_valid_in== 1'b1) data <= data_in;
		else if(data_valid_in== 1'b0) data <= data;
	end
    
    reg enable_cvt;
    //assign a9 = data;
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            counter = 0;
            //enable_cvt = 1'b0;
            //valid_out = 1'b0;
        end
        else if (enable == 1'b1 && data_valid_in ==1'b1) begin
            counter = counter + 1;
        end
        else if (enable == 1'b0) begin
            counter = counter;
        end
    end 

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            enable_cvt = 1'b0; 
        end
        else if(counter == IMG_WIDTH + 2/*counter == 3*IMG_WIDTH +3*/) begin //8 // + 5
            enable_cvt = 1'b1;
        end
        else begin
            enable_cvt = enable_cvt;
        end
    end 
    //2 line bf
    assign a9 = data;
    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_1(
                    clk,
                    resetn,
                    enable,
                    a9,
                    a6
                    );
                    
    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_2(
                    clk,
                    resetn,
                    enable,
                    a6,
                    a3
                    );


    /* dung 3 line bf
    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_1(
                    clk,
                    resetn,
                    enable,
                    data,
                    a9
                    );
                    
    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_2(
                    clk,
                    resetn,
                    enable,
                    a9,
                    a6
                    );
    
    line_buffer #(.DATA_WIDTH(DATA_WIDTH),.IMG_WIDTH(IMG_WIDTH)) 
                  lb_3(
                    clk,
                    resetn,
                    enable,
                    a6,
                    a3
                    );
    */
    //windows scan
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a8(
                clk,
                resetn,
                enable,
                a9,
                a8
                );
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a7(
                clk,
                resetn,
                enable,
                a8,
                a7
                );
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a5(
                clk,
                resetn,
                enable,
                a6,
                a5
                );
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a4(
                clk,
                resetn,
                enable,
                a5,
                a4
                );
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a2(
                clk,
                resetn,
                enable,
                a3,
                a2
                );
    nbit_dff #(.DATA_WIDTH(DATA_WIDTH)) 
              dff_a1(
                clk,
                resetn,
                enable,
                a2,
                a1
                );

    int2float w1_inst(
                      clk,
                      resetn,
                      enable,
                      a1,
                      enable_cvt,
                      w1,
                      valid_out_cvt2float[0]
                      );
    int2float w2_inst(
                      clk,
                      resetn,
                      enable,
                      a2,
                      enable_cvt,
                      w2,
                      valid_out_cvt2float[1]
                      );
    int2float w3_inst(
                      clk,
                      resetn,
                      enable,
                      a3,
                      enable_cvt,
                      w3,
                      valid_out_cvt2float[2]
                      );
    int2float w4_inst(
		                  clk,
                      resetn,
                      enable,
                      a4,
                      enable_cvt,
                      w4,
                      valid_out_cvt2float[3]
                      );
    int2float w5_inst(
                      clk,
                      resetn,
                      enable,
                      a5,
                      enable_cvt,
                      w5,
                      valid_out_cvt2float[4]
                      );
    int2float w6_inst(
                      clk,
                      resetn,
                      enable,
                      a6,
                      enable_cvt,
                      w6,
                      valid_out_cvt2float[5]
                      );
    int2float w7_inst(
                      clk,
                      resetn,
                      enable,
                      a7,
                      enable_cvt,
                      w7,
                      valid_out_cvt2float[6]
                      );
    int2float w8_inst(
                      clk,
                      resetn,
                      enable,
                      a8,
                      enable_cvt,
                      w8,
                      valid_out_cvt2float[7]
                      );
    int2float w9_inst(
                      clk,
                      resetn,
                      enable,
                      a9,
                      enable_cvt,
                      w9,
                      valid_out_cvt2float[8]
                      );

    and and_9(valid_out,
            valid_out_cvt2float[0],
            valid_out_cvt2float[1],
            valid_out_cvt2float[2],
            valid_out_cvt2float[3],
            valid_out_cvt2float[4],
            valid_out_cvt2float[5],
            valid_out_cvt2float[6],
            valid_out_cvt2float[7],
            valid_out_cvt2float[8]);

endmodule