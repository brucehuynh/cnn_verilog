////////////////////////////////////////////
//  MODULE: SOURCE
//  AUTHOR: VirosTeam
//  Created on Mar 18, 2017
////////////////////////////////////////////

module source #(
	parameter     DATA_WIDTH = 8		// 8 IS GREY_IMAGE || 24 IS RGB IMAGE
	)
	(	
	input    wire                        clk,                // Clock signal
	input    wire                        rst,                // Reset signal
	input    wire                        enable,              // Enable signal
	output   reg     [DATA_WIDTH-1:0]  data,               // Data per one pixel of image
	output   reg                         data_valid_in      // That signal confirm that data output are valid
	);

	reg     [15:0]              width;              // The width of image input
	reg     [15:0]              high;               // The high of image input
	reg     [31:0]              frames;             // Number of frames
	integer     data_file;               // Return value of file that open
	integer     counter=0;               // Counter
	integer getwidth;
	integer getheight;
	integer getframes;
	integer getdata;
	reg    [31:0]   eor;                 // Define the value that end of reading file
	
	// Initial reading file text_image
	initial begin
		data_file = $fopen("6x6.txt", "r"); // Open file with read access
	    
	    // Check file is located or not
			if (data_file == 0)
				begin
						$display("File isnt found");	
						$finish;
				end
	 end
	 
	// Always procedural
	always @(posedge clk or negedge rst) begin
  // Reset signal
		if (rst == 1'b0) begin
			data <= 0;
			data_valid_in <= 0;
			width <= 0;
			high <= 0;
			frames <= 0;
			eor <=0;
		end
  // Do if enable are high
		else if(enable == 1'b1) begin
			counter <= counter + 1'b1;              // Inscrese Counter
			if (counter==1) begin                  // Reading width of image value
				getwidth = $fscanf(data_file, "%d", width);     // Store width value into var "width"
				$display("WIDTH = %d", width);       // Display width to transcript
				eor <= 32'b0;                          // Set eor = 0
				end
			else if (counter==2) begin             // Reading high of image value
				getheight = $fscanf(data_file, "%d", high);      // Store high value into var "high"
				$display("HIGH = %d", high);         // Display high to transcript
				eor <= 32'b0;                          // Set eor = 0
				end
			else if (counter==3) begin             // Reading the number of frame of input
				getframes = $fscanf(data_file, "%d", frames);    // Store value into var "frames"
				$display("FRAMES = %d", frames);				 // Display frames to transcipt
				eor <= width*high*frames+3;           // input are GREY_IMAGE 
				end
			else if (counter > 3 && counter <= eor)begin
			//`ifdef GREY_IMAGE = 1
				getdata = $fscanf(data_file, "%d", data);      // 8 bits per once reading (GREY_IMAGE)
				if(getdata > 0) /*$display("DATA= %d", data)*/;
				else begin 
						$display("-----------------------Cant read data");
						$fclose	(data_file);
				end
			end
			// Set the signal notify that data is valid for output
			data_valid_in <= (counter > eor || (counter < 4)) ? 0 : 1 ;
		end
		
	end
	
endmodule
