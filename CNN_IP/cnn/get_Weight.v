module get_Weight # (
		parameter WIDTH = 13, // after maxpool
		parameter HEIGHT = 13 // after maxpool
		)
	(
	clk,
	resetn,
	enable,
	load_weight,
	load_bias,
	valid_in,
	data_in,
	weight,
	bias,
	feature,
	valid_out,
	load_weight_done,
	load_bias_done
	);
	input clk;
	input resetn;
	input enable;
	input load_weight;
	input load_bias;
	input valid_in;
	input [31:0] data_in;
	input [31:0] weight;
	input [31:0] bias;
	output [31:0] feature;
	output valid_out;
	output load_weight_done;
	output load_bias_done;

	
	reg [9:0] counter;
	
	reg load_bias_done;
	reg load_weight_done;
	reg [31:0] biasR;
	wire [31:0] tmp [0:168];
	wire [31:0] product [0:168];
	wire [168:0] valid_out_mult;
	
	always @ (posedge clk or negedge resetn)begin
        if (resetn == 1'b0) begin 
			  biasR <= 0;
              load_bias_done <= 0;
        end
		    else if (resetn == 1'b1) begin
          if (load_bias == 1'b1) begin  
              biasR <= bias;
              load_bias_done <= 1'b1;
            end
          else if (load_bias == 1'b0) begin
              biasR <= biasR;
              load_bias_done <= load_bias_done;
          end
		    end
    end
	
	//generate function
	genvar i;
	generate
	for (i = 0; i < 169; i=i+1) begin
		if(i == 0) nbit_dff #(32) nbit_dff_ins(clk,resetn,load_weight,weight,tmp[i]);
		else nbit_dff#(32) nbit_dff_ins(clk,resetn,load_weight,tmp[i-1],tmp[i]);
	end
	endgenerate
	
	always @ (posedge clk or negedge resetn) begin
		if(resetn == 1'b0) begin
			counter <= 10'b0;
		end
		else if (load_weight == 1'b1) begin
			counter <= counter + 10'b1;
		end
		else if (load_weight == 1'b0) begin
			counter <= counter;
		end
	end
	always @ (posedge clk or negedge resetn) begin
		if(resetn == 1'b0) begin
			load_weight_done <= 1'b0;
		end
		else if (counter == WIDTH*HEIGHT -1) begin
			load_weight_done <= 1'b1;
		end
		else begin
			load_weight_done <= load_weight_done;
		end
	end

	generate
	for (i = 0; i < 169; i=i+1) begin
		FP_multiplier mult_weight(clk,resetn,enable,valid_in,data_in,tmp[i],product[i],valid_out_mult[i]);
	end
	endgenerate
	

//////////////////////
	//integer j=0;
	wire [31:0] stage1 [0:84];
	wire [83:0] valid_add_1;
	//wire 
	generate
	for (i = 0; i < 168; i=i+2) begin //166 167
		//j=j+1;
		fpadd fpadd_s1_ins(clk,resetn,enable,valid_out_mult[0],product[i],product[i+1],stage1[i>>1],valid_add_1[i>>1]);
	end
	endgenerate
	delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage1_84(clk,resetn,valid_out_mult[168],product[168],stage1[84]);
/////////////////////////
	//integer k=0;
	wire [31:0] stage2 [0:42];
	wire [41:0] valid_add_2;
	//wire 
	generate
	for (i = 0; i < 84; i=i+2) begin //82 83
		//k=k+1;
		fpadd fpadd_s2_ins(clk,resetn,enable,valid_add_1[i],stage1[i],stage1[i+1],stage2[i>>1],valid_add_2[i>>1]);
	end
	endgenerate
	delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage2_42(clk,resetn,valid_add_1[0],stage1[84],stage2[42]);
//////////////////////////
	//integer l=0;
	wire [31:0] stage3 [0:21];
	wire [20:0] valid_add_3;
	//wire 
	generate
	for (i = 0; i < 41; i=i+2) begin //40 41
		//l=l+1;
		fpadd fpadd_s3_ins(clk,resetn,enable,valid_add_2[i],stage2[i],stage2[i+1],stage3[i>>1],valid_add_3[i>>1]);
	end
	endgenerate
	delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage3_21(clk,resetn,valid_add_2[0],stage2[42],stage3[21]);
//////////////////////////
	//integer m=0;
	wire [31:0] stage4 [0:10];
	wire [10:0] valid_add_4;
	//wire 
	generate
	for (i = 0; i < 22; i=i+2) begin //20 21
		//m=m+1;
		fpadd fpadd_s4_ins(clk,resetn,enable,valid_add_3[i],stage3[i],stage3[i+1],stage4[i>>1],valid_add_4[i>>1]);
	end
	endgenerate
	//delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage4_20(clk,resetn,valid_add_3[0],stage3[20],stage4[10]);
//////////////////////////
	//integer n=0;
	wire [31:0] stage5 [0:5];
	wire [4:0] valid_add_5;
	//wire 
	generate
	for (i = 0; i < 10; i=i+2) begin //8 9
		//n=n+1;
		fpadd fpadd_s5_ins(clk,resetn,enable,valid_add_4[i],stage4[i],stage4[i+1],stage5[i>>1],valid_add_5[i>>1]);
	end
	endgenerate
	wire valid_add_bias;
	fpadd fpadd_bias(clk,resetn,enable,valid_add_4[0],stage4[10],bias,stage5[5],valid_add_bias);
	//delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage5_10(clk,resetn,valid_add_4[0],stage4[10],stage5[5]);
//////////////////////////
	//integer o=0;
	wire [31:0] stage6 [0:2];
	wire [2:0] valid_add_6;
	
	//wire 
	generate
	for (i = 0; i < 5; i=i+2) begin //4 5
		//o=o+1;
		fpadd fpadd_s6_ins(clk,resetn,enable,valid_add_5[i],stage5[i],stage5[i+1],stage6[i>>1],valid_add_6[i>>1]);
	end
	endgenerate
	//
	
//////////////////////////
	
	wire [31:0] stage7 [0:1];
	wire valid_add_7;
	fpadd fpadd_s7_ins(clk,resetn,enable,valid_add_6[0],stage6[0],stage6[1],stage7[0],valid_add_7);
	//fpadd fpadd_s7_ins_1(clk,resetn,enable,valid_add_6[2],stage6[2],stage6[3],stage7[1],valid_add_7[1]);
	delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(6)) stage7_1(clk,resetn,valid_add_6[0],stage6[2],stage7[1]);

	wire [31:0] stage8;
	wire valid_add_8;
	fpadd fpadd_s8_ins(clk,resetn,enable,valid_add_7,stage7[0],stage7[1],stage8,valid_add_8);

	exponential expo_ins(
						clk,
						resetn,
						enable,
						valid_add_8,
						stage8,
						feature,
						valid_out
						);
	
	
endmodule
