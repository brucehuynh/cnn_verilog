//give output after 12 clocks
module max (clk,enable,resetn,valid_in,in1,in2,in3,in4,out,valid_out);
    input clk;
    input enable;
    input resetn;
    input valid_in;
    input [31:0] in1;
    input [31:0] in2;
    input [31:0] in3;
    input [31:0] in4;
    output [31:0] out;
    output reg valid_out;

   
    reg [31:0] R1,R2,R3;
    //reg [31:0] out;
    wire [31:0] out1,out2,out3;
    wire [31:0] di1,di2,di3,di4,dR1,dR2;
    wire [31:0] wR1,wR2;
    wire valid_out1,valid_out2;
    reg max1,max2;

    assign wR1 = R1;
    assign wR2 = R2;
    assign out =R3;

    //instance
    fpadd m1(clk,enable,resetn,valid_in,in1,{1'b1,in2[30:0]},out1,valid_out1);
    fpadd m2(clk,enable,resetn,valid_in,in3,{1'b1,in4[30:0]},out2,valid_out2);
    fpadd m3(clk,enable,resetn,max1&max2,R1,{1'b1,R2[30:0]},out3,valid_out3);

    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m4(clk,enable,resetn,in1,di1);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m5(clk,enable,resetn,in2,di2);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m6(clk,enable,resetn,in3,di3);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m7(clk,enable,resetn,in4,di4);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m8(clk,enable,resetn,wR1,dR1);
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(5)) m9(clk,enable,resetn,wR2,dR2);


    // tim duoc max cua in1 va in2
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            R1 <=0;
            max1<=1'b0;
        end
        else if (valid_out1 == 1'b1) begin
            if(out1[31] == 1'b0) begin
                 R1 <=di1;
                 max1<=1'b1;
            end
            else begin
                 R1 <= di2;
                 max1<=1'b1;
            end
        end
        else begin 
            R1 <= R1;
            max1<=max1;
        end
    end

    // tim duoc max cua in3 va in4
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            R2 <=0;
            max2<=1'b0;
        end
        else if (valid_out2 == 1'b1) begin
            if(out2[31] == 1'b0) begin
                 R2 <= di3;
                 max2<=1'b1;
            end
            else begin
                 R2 <= di4;
                 max2<=1'b1;
            end
        end
        else begin 
            R2 <= R2;
            max2<=max2;
        end
    end

    // tim duoc max cua 4 input
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            R3 <=0;
            valid_out<=1'b0;
        end
        else if (valid_out3 == 1'b1) begin
            if(out3[31] == 1'b0) begin
                 R3 <= dR1;
                 valid_out<=1'b1;
            end
            else begin
                 R3 <= dR2;
                 valid_out<=1'b1;
            end
        end
        else begin 
            R3 <= R3;
            valid_out<=valid_out;
        end
    end


endmodule