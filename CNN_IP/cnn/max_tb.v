`timescale 1ns/1ps
module max_tb();
    reg clk;
    reg enable;
    reg resetn;
    reg valid_in;
    reg [31:0] in1;
    reg [31:0] in2;
    reg [31:0] in3;
    reg [31:0] in4;
    wire [31:0] out;
    wire valid_out;


    max2 m1(clk,enable,resetn,valid_in,in1,in2,in3,in4,out,valid_out);

    always #1 clk = ~clk;
    always #2 begin 
        in1[30:0] = $random;
        in2[30:0] = $random;
        in3[30:0] = $random;
        in4[30:0] = $random;
    end

    initial begin
        $monitor("time = %d, in1 = %b, in2 = %b, in3 = %b, in4 = %b, out = %b, valid out = %b",$time,in1,in2,in3,in4,out,valid_out);
        in1[31] = 0;
        in2[31] = 0;
        in3[31] = 0;
        in4[31] = 0;
        clk = 0;
        resetn = 0;
        enable =1;
        #10 resetn = 1;
        valid_in =1;
        #30 $finish();
        
    end

endmodule