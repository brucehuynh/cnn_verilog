module max2 (clk,enable,resetn,valid_in,in1,in2,in3,in4,out,valid_out);
    input clk;
    input enable;
    input resetn;
    input valid_in;
    input [31:0] in1;
    input [31:0] in2;
    input [31:0] in3;
    input [31:0] in4;
    output [31:0] out;
    output valid_out;

    reg [31:0] A,B,C,D;
    reg [31:0] maxab,maxcd,max;
    reg [1:0] counter;
    assign out = max;


    delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(3)) valid_ins(clk,resetn,enable,valid_in,valid_out);
    /*
    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            counter <= 0;
        end
        else if (enable == 1'b1) begin
            if( valid_in == 1'b1) counter <= counter + 1;
            else if (valid_in == 1'b0) counter <= 0;
        end
        else begin
            counter <= counter;
        end
    end 

    always @ (posedge clk or negedge resetn) begin
        if(resetn == 1'b0) begin
            valid_out <= 0;
        end
        else if ( counter == 2'b10) begin
            valid_out <= 1;
        end
        else begin
            valid_out <= valid_out;
        end
    end 
    */
    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            A <= 0;
            B <= 0;
            C <= 0;
            D <= 0;
        end
        else if (valid_in == 1'b1) begin
            A <= in1;
            B <= in2;
            C <= in3;
            D <= in4;
        end 
        else if (valid_in == 1'b0) begin
            A <= A;
            B <= B;
            C <= C;
            D <= D;
        end 
    end

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            maxab <= 0;
        end
        else if (enable == 1'b1) begin
            if ( $signed(A) > $signed(B) ) begin
              maxab <= A;
            end
            else maxab <= B;
        end 
        else if (enable == 1'b0) begin
            maxab <= maxab;
        end 
    end

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            maxcd <= 0;
        end
        else if (enable == 1'b1) begin
            if ( $signed(C) > $signed(D) ) begin
              maxcd <= C;
            end
            else maxcd <= D;
        end 
        else if (enable == 1'b0) begin
            maxcd <= maxcd;
        end 
    end

    always @ (posedge clk or negedge resetn) begin
        if (resetn == 1'b0) begin
            max <= 0;
            //valid_out <= 0;
        end
        else if (enable == 1'b1) begin
            if ( $signed(maxab) >= $signed(maxcd) ) begin
              max <= maxab;
              //valid_out <= 1;
            end
            else begin
                max <= maxcd;
                //valid_out <= 1;
            end 
        end 
        else if (enable == 1'b0) begin
            max <= max;
            //valid_out <= valid_out;
        end 
    end
endmodule