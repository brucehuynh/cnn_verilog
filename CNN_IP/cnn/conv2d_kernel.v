/*
`include "../Lib_Cells/dff.v"
`include "line_buffer.v"
`include "../Floating_Point_Multiplier/FP_multiplier.v"
`include "../Floating_Point_Adder/fpadd.v"
`include "../Integer2Float/int2float.v"
`include "../Integer2Float/float2int.v"
*/
module conv2d_kernel #(
  parameter DATA_WIDTH = 8,
  parameter IMG_WIDTH = 300,
  parameter IMG_HEIGHT = 30
  )
  (
    input     wire            clk,
    input     wire            resetn,
    input     wire            enable,
    input     wire            data_valid_in,
    input     wire   [DATA_WIDTH-1:0]    data_in,
    input     wire            load_kernel,
    input     wire   [31:0]   k1,
    input     wire   [31:0]   k2,
    input     wire   [31:0]   k3,
    input     wire   [31:0]   k4,
    input     wire   [31:0]   k5,
    input     wire   [31:0]   k6,
    input     wire   [31:0]   k7,
    input     wire   [31:0]   k8,
    input     wire   [31:0]   k9,
	input     wire   [31:0]   bk,
    //input     wire            enable_cvt,
    
    output    wire   [31:0]    data_out,
    output     valid_out_pixel, // bi tat sau W*H - (W-2)*(H-2)
    output reg done_img
  );
    wire [31:0] w1,w2,w3,w4,w5,w6,w7,w8,w9;
    reg [31:0] k1R,k2R,k3R,k4R,k5R,k6R,k7R,k8R,k9R,bkR;
    wire [31:0] x1,x2,x3,x4,x5,x6,x7,x8,x9;
    wire [8:0] valid_out_mult;
    wire [31:0] res1,res2,res3,res4,res5,res6,res7,res8,res_x9;
    
    wire valid_add1,valid_add2,valid_add3,valid_add4,valid_add5,valid_add6,valid_add7,valid_add8;
    wire [31:0] x9_bk;
    //wire [31:0] res_flt;
    // control signals : enable_cvt, enable_mult 

    //reg [DATA_WIDTH-1:0] data;
    //reg enable_counter,enable_cvt;
    wire enable_mult;
    reg [31:0] counter;
    reg load_kernel_done;
	wire [31:0] data_conv;

    //reg [1:0] counter;
    /*  Assign kernel -- kernel unit */
    /*
    assign k1 = 32'b0;
    assign k2 = 32'b0;
    assign k3 = 32'b0;
    assign k4 = 32'b0;
    assign k5 = 32'b00111111100000000000000000000000;
    assign k6 = 32'b0;
    assign k7 = 32'b0;
    assign k8 = 32'b0;
    assign k9 = 32'b0;
    */
    /* Egde x 
    
    assign k1 = 32'b00111111100000000000000000000000; //1
    assign k2 = 32'b0;
    assign k3 = 32'b10111111100000000000000000000000; //- 1
    assign k4 = 32'b01000000000000000000000000000000; //2
    assign k5 = 32'b0;
    assign k6 = 32'b11000000000000000000000000000000; //-2
    assign k7 = 32'b00111111100000000000000000000000; //1 
    assign k8 = 32'b0;
    assign k9 = 32'b10111111100000000000000000000000; //- 1

    /* Average */
    /*
    assign k1 = 32'b00111111100000000000000000000000; //1
    assign k2 = 32'b00111111100000000000000000000000;
    assign k3 = 32'b00111111100000000000000000000000; //- 1
    assign k4 = 32'b00111111100000000000000000000000; //1
    assign k5 = 32'b00111111100000000000000000000000;
    assign k6 = 32'b00111111100000000000000000000000; //-1
    assign k7 = 32'b00111111100000000000000000000000; //1 
    assign k8 = 32'b00111111100000000000000000000000;
    assign k9 = 32'b00111111100000000000000000000000; //- 1
    */
    // load kernel
    always @ (posedge clk or negedge resetn)begin
        if (resetn == 1'b0) begin 
              k1R <= 32'b0;
              k2R <= 32'b0;
              k3R <= 32'b0;
              k4R <= 32'b0;
              k5R <= 32'b0;
              k6R <= 32'b0;
              k7R <= 32'b0;
              k8R <= 32'b0;
              k9R <= 32'b0;
			  bkR <= 32'b0;
              load_kernel_done <= 0;
        end
		    else if (resetn == 1'b1) begin
          if (load_kernel == 1'b1) begin  
              k1R <= k1;
              k2R <= k2;
              k3R <= k3;
              k4R <= k4;
              k5R <= k5;
              k6R <= k6;
              k7R <= k7;
              k8R <= k8;
              k9R <= k9;
			  bkR <= bk;
              load_kernel_done <= 1'b1;
            end
          else if (load_kernel == 1'b0) begin
              k1R <= k1R;
              k2R <= k2R;
              k3R <= k3R;
              k4R <= k4R;
              k5R <= k5R;
              k6R <= k6R;
              k7R <= k7R;
              k8R <= k8R;
              k9R <= k9R;
			  bkR <= bk;
              load_kernel_done <= load_kernel_done;
          end
		    end
    end
    
    lines #(.DATA_WIDTH(8),.IMG_WIDTH(IMG_WIDTH)) 
              lbs(
                clk,
                resetn,
                enable,
                data_valid_in&load_kernel_done,
                data_in,
                w1,
                w2,
                w3,
                w4,
                w5,
                w6,
                w7,
                w8,
                w9,
                enable_mult
              );

    FP_multiplier kernel_1(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k1R,
                        w1,
                        x1,
                        valid_out_mult[0]
                        );
    FP_multiplier kernel_2(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k2R,
                        w2,
                        x2,
                        valid_out_mult[1]
                        );
    FP_multiplier kernel_3(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k3R,
                        w3,
                        x3,
                        valid_out_mult[2]
                        );
    FP_multiplier kernel_4(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k4R,
                        w4,
                        x4,
                        valid_out_mult[3]
                        );
    FP_multiplier kernel_5(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k5R,
                        w5,
                        x5,
                        valid_out_mult[4]
                        );
    FP_multiplier kernel_6(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k6R,
                        w6,
                        x6,
                        valid_out_mult[5]
                        );
    FP_multiplier kernel_7(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k7R,
                        w7,
                        x7,
                        valid_out_mult[6]
                        );
    FP_multiplier kernel_8(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k8R,
                        w8,
                        x8,
                        valid_out_mult[7]
                        );
    FP_multiplier kernel_9(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k9R,
                        w9,
                        x9,
                        valid_out_mult[8]
                        );

    //fpadd(clk,resetn,enable,valid_in,a,b,out,valid_out);
    fpadd add_1(
                clk,
                resetn,
                enable,
                valid_out_mult[0]&valid_out_mult[1],
                x1,
                x2,
                res1,
                valid_add1
                );
    fpadd add_2(
                clk,
                resetn,
                enable,
                valid_out_mult[2]&valid_out_mult[3],
                x3,
                x4,
                res2,
                valid_add2
                );
    fpadd add_3(
                clk,
                resetn,
                enable,
                valid_out_mult[4]&valid_out_mult[5],
                x5,
                x6,
                res3,
                valid_add3
                );
    fpadd add_4(
                clk,
                resetn,
                enable,
                valid_out_mult[6]&valid_out_mult[7],
                x7,
                x8,
                res4,
                valid_add4
                );
	fpadd add_bk(
                clk,
                resetn,
                enable,
                valid_out_mult[8],
                x9,
                bkR,
                res_x9,
                valid_add_bias
                );
    fpadd add_5(
                clk,
                resetn,
                enable,
                valid_add1&valid_add2,
                res1,
                res2,
                res5,
                valid_add5
                );
    fpadd add_6(
                clk,
                resetn,
                enable,
                valid_add3&valid_add4,
                res3,
                res4,
                res6,
                valid_add6
                );
    fpadd add_7(
                clk,
                resetn,
                enable,
                valid_add5&valid_add6,
                res5,
                res6,
                res7,
                valid_add7
                );
    
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(12)) delayx9(clk,enable,resetn,res_x9,x9_bk);
    fpadd add_8(
                clk,
                resetn,
                enable,
                valid_add7&valid_out_mult[8],
                res7,
                x9_bk,
                res8,
                valid_add8
                );
    //
    fpdiv division(
                clk,
                resetn,
                enable,
                valid_add8,
                res8,
                32'b01000001000100000000000000000000, // nine
                data_conv,
                valid_out_pixel
                );
	activate relu(data_conv,data_out);
	
    always @ (posedge clk or negedge resetn) begin
      if(resetn == 1'b0) counter <= 0;
      else if(valid_out_pixel == 1'b1) counter <= counter + 1;
      else counter <= counter;
    end
    always @ (posedge clk or negedge resetn) begin
      if(resetn == 1'b0) done_img <= 0;
      else if(counter == (IMG_WIDTH)*(IMG_HEIGHT) -1 ) done_img <= 1'b1;
      else done_img <= done_img;
    end


    //float2int result(clk,resetn,enable,res8,valid_add8,data_out,valid_out_pixel);
    
  

  


  endmodule
