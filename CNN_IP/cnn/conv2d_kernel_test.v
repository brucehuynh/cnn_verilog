module conv2d_kernel_test(clk,resetn,enable,is0,is1,is2,is3,is4,is5,is6,is7,is8,is9,stop);
        parameter WIDTH = 28;
        parameter HEIGHT = 28;
        input clk;
        input resetn;
        input enable;
        output stop;
		output [31:0] is0,is1,is2,is3,is4,is5,is6,is7,is8,is9;

        wire [7:0] data_img;
        wire valid_source;
        wire enable_cvt,valid_out;
        wire done_conv;
        wire [31:0] pixel_conv;
        wire [31:0] data_out;
        wire [31:0] data_maxpool;
        //wire valid_out_pixel1;
        wire valid_out_pixel;
        wire done_maxpool;
        wire [31:0] k1,k2,k3,k4,k5,k6,k7,k8,k9;
		wire [31:0] b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
		wire [31:0] a0,a1,a2,a3,a4,a5,a6,a7,a8,a9;
		wire [31:0] w0,w1,w2,w3,w4,w5,w6,w7,w8,w9;
        wire [31:0] bk;
		wire [9:0] load_bias_done;
		wire [9:0] load_weight_done;
        wire [9:0] valid_out_weight;

		wire [31:0] data_truncate;
		wire valid_out_truncate;
		wire done_truncate;
        wire load_kernel;

        /*
        controller #(.WIDTH(WIDTH),.HEIGHT(HEIGHT)) 
                controller_ins (
                        clk,
                        resetn,
                        enable,
                        enable_cvt,
                        valid_out
                        //valid_out_pixel1
                        );
        */
        source_kernel #(8) 
                source_inst(
                clk,
                resetn,
                enable,
				load_bias_done[0],
				load_kernel_done,
				load_weight_done[0],
                data_img,
                valid_source,
                k1,
                k2,
                k3,
                k4,
                k5,
                k6,
                k7,
                k8,
                k9,
				bk,
				b0,
				b1,
				b2,
				b3,
				b4,
				b5,
				b6,
				b7,
				b8,
				b9,
				w0,
				w1,
				w2,
				w3,
				w4,
				w5,
				w6,
				w7,
				w8,
				w9,
                load_kernel,
				load_bias,
				load_weight
                );

        conv2d_kernel #(.DATA_WIDTH(8),.IMG_WIDTH(WIDTH),.IMG_HEIGHT(HEIGHT)) 
                conv2d_inst(
                        clk,
                        resetn,
                        enable,
                        valid_source,
                        data_img,
                        load_kernel,
                        k1,
                        k2,
                        k3,
                        k4,
                        k5,
                        k6,
                        k7,
                        k8,
                        k9,
						bk,
                        pixel_conv,
                        valid_out_pixel,
                        done_conv
                        );
		truncate #(.WIDTH(WIDTH),.HEIGHT(HEIGHT)) 
				truncate_inst(
					clk,
					resetn,
					valid_out_pixel,
					pixel_conv,
					data_truncate,
					valid_out_truncate,
					done_truncate
					);
                        
        max_pooling #(.DATA_WIDTH(32),.IMG_WIDTH(WIDTH-2),.IMG_HEIGHT(HEIGHT-2)) 
                max_pooling_inst(
                        clk,
                        enable,
                        resetn,
                        valid_out_truncate,
                        data_truncate,
                        data_maxpool,
                        valid_out_maxpool,
                        done_maxpool
                        );
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_0(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w0,
						b0, //bias 0
						a0,
						valid_out_weight[0],
						load_weight_done[0],
						load_bias_done[0]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_1(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w1,
						b1, //bias 0
						a1,
						valid_out_weight[1],
						load_weight_done[1],
						load_bias_done[1]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_2(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w2,
						b2, //bias 0
						a2,
						valid_out_weight[2],
						load_weight_done[2],
						load_bias_done[2]
						);				
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_3(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w3,
						b3, //bias 0
						a3,
						valid_out_weight[3],
						load_weight_done[3],
						load_bias_done[3]
						);	
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_4(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w4,
						b4, //bias 0
						a4,
						valid_out_weight[4],
						load_weight_done[4],
						load_bias_done[4]
						);	
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_5(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w5,
						b5, //bias 0
						a5,
						valid_out_weight[5],
						load_weight_done[5],
						load_bias_done[5]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_6(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w6,
						b6, //bias 0
						a6,
						valid_out_weight[6],
						load_weight_done[6],
						load_bias_done[6]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_7(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w7,
						b7, //bias 0
						a7,
						valid_out_weight[7],
						load_weight_done[7],
						load_bias_done[7]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_8(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w8,
						b8, //bias 0
						a8,
						valid_out_weight[8],
						load_weight_done[8],
						load_bias_done[8]
						);
		get_Weight # (.WIDTH((WIDTH-2)>>1),.HEIGHT((HEIGHT-2)>>1))
				get_Weight_9(
						clk,
						resetn,
						enable,
						load_weight,
						load_bias,
						valid_out_maxpool,
						data_maxpool,
						w9,
						b9, //bias 0
						a9,
						valid_out_weight[9],
						load_weight_done[9],
						load_bias_done[9]
						);
						/*
		mysoftmax #(.DATA_WIDTH(32))
					softmax(
						clk,
						resetn,
						enable,
						valid_out_weight[0],
						a0,
						a1,
						a2,
						a3,
						a4,
						a5,
						a6,
						a7,
						a8,
						a9,
						is0,
						is1,
						is2,
						is3,
						is4,
						is5,
						is6,
						is7,
						is8,
						is9,
						stop
					);
                
        
        
                /*
        sink #(.DATA_WIDTH(32),.IMG_WIDTH(WIDTH/2),.IMG_HEIGHT(HEIGHT/2))
                sink_inst(
                        clk,
                        resetn,
                        enable,
                        data_out,
                        valid_out_pixel,
                        done_maxpool
                );
                */
endmodule
