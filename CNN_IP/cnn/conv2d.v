/*
`include "../Lib_Cells/dff.v"
`include "line_buffer.v"
`include "../Floating_Point_Multiplier/FP_multiplier.v"
`include "../Floating_Point_Adder/fpadd.v"
`include "../Integer2Float/int2float.v"
`include "../Integer2Float/float2int.v"
*/
module conv2d #(
  parameter DATA_WIDTH = 8,
  parameter IMG_WIDTH = 300,
  parameter IMG_HEIGHT = 30
  )
  (
    input     wire            clk,
    input     wire            resetn,
    input     wire            enable,
    input     wire            data_valid_in,
    input     wire   [DATA_WIDTH-1:0]    data_in,
    //input     wire            enable_cvt,
    output    wire   [31:0]    data_out,
    output     valid_out_pixel, // bi tat sau W*H - (W-2)*(H-2)
    output reg done_img
  );
    wire [31:0] w1,w2,w3,w4,w5,w6,w7,w8,w9;
    wire [31:0] k1,k2,k3,k4,k5,k6,k7,k8,k9;
    wire [31:0] x1,x2,x3,x4,x5,x6,x7,x8,x9;
    wire [8:0] valid_out_mult;
    wire [31:0] res1,res2,res3,res4,res5,res6,res7,res8;
    
    wire valid_add1,valid_add2,valid_add3,valid_add4,valid_add5,valid_add6,valid_add7,valid_add8;
    wire [31:0] x9_bk;
    //wire [31:0] res_flt;
    // control signals : enable_cvt, enable_mult 

    //reg [DATA_WIDTH-1:0] data;
    //reg enable_counter,enable_cvt;
    wire enable_mult;
    reg [31:0] counter;

    //reg [1:0] counter;
    /*  Assign kernel -- kernel unit */
    /*
    assign k1 = 32'b0;
    assign k2 = 32'b0;
    assign k3 = 32'b0;
    assign k4 = 32'b0;
    assign k5 = 32'b00111111100000000000000000000000;
    assign k6 = 32'b0;
    assign k7 = 32'b0;
    assign k8 = 32'b0;
    assign k9 = 32'b0;
    */
    /* Egde x */
    
    assign k1 = 32'b00111111100000000000000000000000; //1
    assign k2 = 32'b0;
    assign k3 = 32'b10111111100000000000000000000000; //- 1
    assign k4 = 32'b01000000000000000000000000000000; //2
    assign k5 = 32'b0;
    assign k6 = 32'b11000000000000000000000000000000; //-2
    assign k7 = 32'b00111111100000000000000000000000; //1 
    assign k8 = 32'b0;
    assign k9 = 32'b10111111100000000000000000000000; //- 1

    /* Average */
    /*
    assign k1 = 32'b00111111100000000000000000000000; //1
    assign k2 = 32'b00111111100000000000000000000000;
    assign k3 = 32'b00111111100000000000000000000000; //- 1
    assign k4 = 32'b00111111100000000000000000000000; //1
    assign k5 = 32'b00111111100000000000000000000000;
    assign k6 = 32'b00111111100000000000000000000000; //-1
    assign k7 = 32'b00111111100000000000000000000000; //1 
    assign k8 = 32'b00111111100000000000000000000000;
    assign k9 = 32'b00111111100000000000000000000000; //- 1
    */
    

    lines #(.DATA_WIDTH(8),.IMG_WIDTH(IMG_WIDTH)) 
              lbs(
                clk,
                resetn,
                enable,
                data_valid_in,
                data_in,
                w1,
                w2,
                w3,
                w4,
                w5,
                w6,
                w7,
                w8,
                w9,
                enable_mult
              );

    FP_multiplier kernel_1(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k1,
                        w1,
                        x1,
                        valid_out_mult[0]
                        );
    FP_multiplier kernel_2(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k2,
                        w2,
                        x2,
                        valid_out_mult[1]
                        );
    FP_multiplier kernel_3(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k3,
                        w3,
                        x3,
                        valid_out_mult[2]
                        );
    FP_multiplier kernel_4(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k4,
                        w4,
                        x4,
                        valid_out_mult[3]
                        );
    FP_multiplier kernel_5(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k5,
                        w5,
                        x5,
                        valid_out_mult[4]
                        );
    FP_multiplier kernel_6(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k6,
                        w6,
                        x6,
                        valid_out_mult[5]
                        );
    FP_multiplier kernel_7(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k7,
                        w7,
                        x7,
                        valid_out_mult[6]
                        );
    FP_multiplier kernel_8(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k8,
                        w8,
                        x8,
                        valid_out_mult[7]
                        );
    FP_multiplier kernel_9(
                        clk,
                        resetn,
                        enable,
                        enable_mult,
                        k9,
                        w9,
                        x9,
                        valid_out_mult[8]
                        );

    //fpadd(clk,resetn,enable,valid_in,a,b,out,valid_out);
    fpadd add_1(
                clk,
                resetn,
                enable,
                valid_out_mult[0]&valid_out_mult[1],
                x1,
                x2,
                res1,
                valid_add1
                );
    fpadd add_2(
                clk,
                resetn,
                enable,
                valid_out_mult[2]&valid_out_mult[3],
                x3,
                x4,
                res2,
                valid_add2
                );
    fpadd add_3(
                clk,
                resetn,
                enable,
                valid_out_mult[4]&valid_out_mult[5],
                x5,
                x6,
                res3,
                valid_add3
                );
    fpadd add_4(
                clk,
                resetn,
                enable,
                valid_out_mult[6]&valid_out_mult[7],
                x7,
                x8,
                res4,
                valid_add4
                );
    fpadd add_5(
                clk,
                resetn,
                enable,
                valid_add1&valid_add2,
                res1,
                res2,
                res5,
                valid_add5
                );
    fpadd add_6(
                clk,
                resetn,
                enable,
                valid_add3&valid_add4,
                res3,
                res4,
                res6,
                valid_add6
                );
    fpadd add_7(
                clk,
                resetn,
                enable,
                valid_add5&valid_add6,
                res5,
                res6,
                res7,
                valid_add7
                );
    
    delay_clock #(.DATA_WIDTH(32),.N_CLOCKs(18)) delayx9(clk,enable,resetn,x9,x9_bk);
    fpadd add_8(
                clk,
                resetn,
                enable,
                valid_add7&valid_out_mult[8],
                res7,
                x9_bk,
                res8,
                valid_add8
                );
    //activate relu(data_out,res8);
    fpdiv division(
                clk,
                resetn,
                enable,
                valid_add8,
                res8,
                32'b01000001000100000000000000000000,
                data_out,
                valid_out_pixel
                );

    always @ (posedge clk or negedge resetn) begin
      if(resetn == 1'b0) counter <= 0;
      else if(valid_out_pixel == 1'b1) counter <= counter + 1;
      else counter <= counter;
    end
    always @ (posedge clk or negedge resetn) begin
      if(resetn == 1'b0) done_img <= 0;
      else if(counter == (IMG_WIDTH)*(IMG_HEIGHT) -1 ) done_img <= 1'b1;
      else done_img <= done_img;
    end


    //float2int result(clk,resetn,enable,res8,valid_add8,data_out,valid_out_pixel);
    
  

  


  endmodule
