`timescale 1ns/1ps
module test_activate();
    reg clk, enable, resetn;
    reg [8:0] in;
    wire [8:0] out;
    wire valid_out;

    always #1 clk = ~clk;
    always #2 in = $random;

    activate #(9)m1(clk,enable,resetn,in,out,valid_out);
    initial begin
        $monitor("clk = %b, in = %b, out = %b, valid_out = %b",clk,in,out,valid_out);
    clk = 0;
    resetn = 0;
    enable =1;
    #10 resetn = 1;
    
    #200 $finish();

    end


endmodule