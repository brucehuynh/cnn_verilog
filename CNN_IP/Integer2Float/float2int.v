// this module works only with unsigned 8 bit int (image's pixel)
module float2int(
		input clk,
		input resetn,
		input enable,
		input [31:0] float_in,
		input valid_in,
		output [7:0] int_out,
		output reg valid_out
		);
	//wire [31:0] in;
	reg [31:0] int;
	reg [7:0] shift;
	reg [23:0] mantissa;
	//assign in = (valid_in)?float_in:32'bx;
	assign int_out = int[7:0];
	
	always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0) begin
		int = 32'b0;
		valid_out = 1'b0;
		shift = 8'b0;
		mantissa = 24'b0;
		end 
		else if (enable == 1'b1 && valid_in == 1'b1) begin
			mantissa <= {1'b1,float_in[22:0]};
			if (float_in != 32'b0) begin
			shift = float_in[30:23] - 127;
			//if(shift >0)
			int = mantissa >> (24 - shift);
			//else int = float_in[22:0] << (shift);
			valid_out = 1'b1;
			end
			else begin
			shift = 0;
			int = 32'b0;
			valid_out = 1'b1;
			end
		end
		else begin
			int = int;
			valid_out = valid_out;
			shift = shift;
			mantissa = mantissa;
		end
	end

endmodule
