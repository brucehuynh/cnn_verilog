

`timescale 1ns/1ps

module test_converter();
    reg clk;
    reg resetn;
    reg enable;
    reg [7:0] int_value;
    reg valid_in;
    wire [31:0] flt_value;
    wire valid_out_int;

    wire [7:0] int_out;
    wire valid_out;

    int2float m1(clk,resetn,enable,int_value,valid_in,flt_value,valid_out_int);
    float2int m2(clk,resetn,enable,flt_value,valid_out_int,int_out,valid_out);

    always #20 clk = ~clk;

    always #40 int_value = int_value + 1'b1;

    always @ (posedge clk) begin
        #1 if(int_value == int_out + 8'b1)
            $display("PASSSSSSSSSSS");
        else $display("FAIL at INPUT = %d",int_value);
    end

    initial begin
        $monitor("clk= %b, int = %d, float = %b, int out = %d",clk,int_value,flt_value,int_out);
        clk = 0;
        resetn = 1;
        enable = 1;
        
        int_value = 8'b0;
        /*#10*/ valid_in = 1;
        /*#30*/ resetn = 1;
        #10000 $finish();
    end

endmodule 
