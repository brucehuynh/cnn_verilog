
module normalizer(
				clk,
				resetn,
				enable,
				in,
				mantissa
				);
	input clk;
	input resetn;
	input enable;
	input [47:0] in;
	output [22:0] mantissa;
	
	wire [22:0] temp;
	
	wire [24:0] plus;
	
	
	//assign plus = (in[22:0]!=23'b0)?in[47:23]+1:in[47:23];
	
	
	
	//assign temp = (plus[24])?plus[23:1]:in[22:0];
	assign temp = (in[47])?in[46:24]:in[45:23];
	
	nbit_dff #(23) nbit_dff_ins(clk,resetn,enable,temp,mantissa);
	
endmodule
