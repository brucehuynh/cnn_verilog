/*
`include "full_adder_cell.v"
`include "full_adder_latched.v"
`include "../Lib_Cells/dff.v"
*/
module mult_24bit(
				clk,
				resetn,
				enable,
				valid_in,
				in1,
				in2,
				p
);

input clk;
input resetn;
input enable;
input valid_in;
input [23:0] in1;
input [23:0] in2;
output [47:0] p;

reg [23:0] a,b;

always @ (posedge clk or negedge resetn) begin
	if(resetn == 1'b0) begin
	  a <= 0;
	  b <= 0;
	end
	else if (enable&valid_in == 1'b1) begin
	  a <= in1;
	  b <= in2;
	end
	else if (enable&valid_in == 1'b0) begin
	  a <= a;
	  b <= b;
	end
end

//1
wire [23:0] P1,C1;
wire [22:0] R1;
wire [23:0] A1;
genvar i;
generate 
	for(i = 0; i < 24; i= i+1) begin
		full_adder_cell FAC_1(clk,resetn,enable,a[i],b[0],1'b0,1'b0,P1[i],C1[i],A1[i]);
	end
endgenerate
generate 
	for(i = 0; i < 23; i= i+1) begin
		dff dff_1(clk,resetn,enable,b[i+1],R1[i]);
	end
endgenerate

//2
wire [23:0] P2,C2;
wire [22:0] R2;
wire [23:0] A2;
full_adder_cell FAC_2_a(clk,resetn,enable,A1[23],R1[0],1'b0,C1[23],P2[23],C2[23],A2[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_2(clk,resetn,enable,A1[i],R1[0],P1[i+1],C1[i],P2[i],C2[i],A2[i]);
	end
endgenerate
	dff dff_2_a(clk,resetn,enable,P1[0],R2[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_2(clk,resetn,enable,R1[i+1],R2[i]);
	end
endgenerate

//3
wire [23:0] P3,C3;
wire [22:0] R3;
wire [23:0] A3;
full_adder_cell FAC_3_a(clk,resetn,enable,A2[23],R2[0],1'b0,C2[23],P3[23],C3[23],A3[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_3(clk,resetn,enable,A2[i],R2[0],P2[i+1],C2[i],P3[i],C3[i],A3[i]);
	end
endgenerate
dff dff_3_a(clk,resetn,enable,P2[0],R3[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		//if(i==0) 
		dff dff_3(clk,resetn,enable,R2[i+1],R3[i]);
	end
endgenerate

//4
wire [23:0] P4,C4;
wire [22:0] R4;
wire [23:0] A4;
full_adder_cell FAC_4_a(clk,resetn,enable,A3[23],R3[0],1'b0,C3[23],P4[23],C4[23],A4[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_4(clk,resetn,enable,A3[i],R3[0],P3[i+1],C3[i],P4[i],C4[i],A4[i]);
	end
endgenerate
dff dff_4_a(clk,resetn,enable,P3[0],R4[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_4(clk,resetn,enable,R3[i+1],R4[i]);
	end
endgenerate

//5
wire [23:0] P5,C5;
wire [22:0] R5;
wire [23:0] A5;
full_adder_cell FAC_5_a(clk,resetn,enable,A4[23],R4[0],1'b0,C4[23],P5[23],C5[23],A5[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_5(clk,resetn,enable,A4[i],R4[0],P4[i+1],C4[i],P5[i],C5[i],A5[i]);
	end
endgenerate
dff dff_5_a(clk,resetn,enable,P4[0],R5[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_5(clk,resetn,enable,R4[i+1],R5[i]);
	end
endgenerate

//6
wire [23:0] P6,C6;
wire [22:0] R6;
wire [23:0] A6;
full_adder_cell FAC_6_a(clk,resetn,enable,A5[23],R5[0],1'b0,C5[23],P6[23],C6[23],A6[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_6(clk,resetn,enable,A5[i],R5[0],P5[i+1],C5[i],P6[i],C6[i],A6[i]);
	end
endgenerate
dff dff_6_a(clk,resetn,enable,P5[0],R6[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_6(clk,resetn,enable,R5[i+1],R6[i]);
	end
endgenerate

//7
wire [23:0] P7,C7;
wire [22:0] R7;
wire [23:0] A7;
full_adder_cell FAC_7_a(clk,resetn,enable,A6[23],R6[0],1'b0,C6[23],P7[23],C7[23],A7[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_7(clk,resetn,enable,A6[i],R6[0],P6[i+1],C6[i],P7[i],C7[i],A7[i]);
	end
endgenerate
dff dff_7_a(clk,resetn,enable,P6[0],R7[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_7(clk,resetn,enable,R6[i+1],R7[i]);
	end
endgenerate

//8
wire [23:0] P8,C8;
wire [22:0] R8;
wire [23:0] A8;
full_adder_cell FAC_8_a(clk,resetn,enable,A7[23],R7[0],1'b0,C7[23],P8[23],C8[23],A8[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_8(clk,resetn,enable,A7[i],R7[0],P7[i+1],C7[i],P8[i],C8[i],A8[i]);
	end
endgenerate
	dff dff_8_a(clk,resetn,enable,P7[0],R8[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_8(clk,resetn,enable,R7[i+1],R8[i]);
	end
endgenerate

//9
wire [23:0] P9,C9;
wire [22:0] R9;
wire [23:0] A9;
full_adder_cell FAC_9_a(clk,resetn,enable,A8[23],R8[0],1'b0,C8[23],P9[23],C9[23],A9[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_9(clk,resetn,enable,A8[i],R8[0],P8[i+1],C8[i],P9[i],C9[i],A9[i]);
	end
endgenerate
dff dff_9_a(clk,resetn,enable,P8[0],R9[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_9(clk,resetn,enable,R8[i+1],R9[i]);
	end
endgenerate

//10
wire [23:0] P10,C10;
wire [22:0] R10;
wire [23:0] A10;
full_adder_cell FAC_10_a(clk,resetn,enable,A9[23],R9[0],1'b0,C9[23],P10[23],C10[23],A10[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_10(clk,resetn,enable,A9[i],R9[0],P9[i+1],C9[i],P10[i],C10[i],A10[i]);
	end
endgenerate
dff dff_10_a(clk,resetn,enable,P9[0],R10[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_10(clk,resetn,enable,R9[i+1],R10[i]);
	end
endgenerate

//11
wire [23:0] P11,C11;
wire [22:0] R11;
wire [23:0] A11;
full_adder_cell FAC_11_a(clk,resetn,enable,A10[23],R10[0],1'b0,C10[23],P11[23],C11[23],A11[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_11(clk,resetn,enable,A10[i],R10[0],P10[i+1],C10[i],P11[i],C11[i],A11[i]);
	end
endgenerate
dff dff_11_a(clk,resetn,enable,P10[0],R11[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_11(clk,resetn,enable,R10[i+1],R11[i]);
	end
endgenerate

//12
wire [23:0] P12,C12;
wire [22:0] R12;
wire [23:0] A12;
full_adder_cell FAC_12_a(clk,resetn,enable,A11[23],R11[0],1'b0,C11[23],P12[23],C12[23],A12[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_12(clk,resetn,enable,A11[i],R11[0],P11[i+1],C11[i],P12[i],C12[i],A12[i]);
	end
endgenerate
dff dff_12_a(clk,resetn,enable,P11[0],R12[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_12(clk,resetn,enable,R11[i+1],R12[i]);
	end
endgenerate

//13
wire [23:0] P13,C13;
wire [22:0] R13;
wire [23:0] A13;
full_adder_cell FAC_13_a(clk,resetn,enable,A12[23],R12[0],1'b0,C12[23],P13[23],C13[23],A13[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_13(clk,resetn,enable,A12[i],R12[0],P12[i+1],C12[i],P13[i],C13[i],A13[i]);
	end
endgenerate
dff dff_13_a(clk,resetn,enable,P12[0],R13[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		 dff dff_13(clk,resetn,enable,R12[i+1],R13[i]);
	end
endgenerate

//14
wire [23:0] P14,C14;
wire [22:0] R14;
wire [23:0] A14;
full_adder_cell FAC_14_a(clk,resetn,enable,A13[23],R13[0],1'b0,C13[23],P14[23],C14[23],A14[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		 full_adder_cell FAC_14(clk,resetn,enable,A13[i],R13[0],P13[i+1],C13[i],P14[i],C14[i],A14[i]);
	end
endgenerate
dff dff_14_a(clk,resetn,enable,P13[0],R14[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_14(clk,resetn,enable,R13[i+1],R14[i]);
	end
endgenerate

//15
wire [23:0] P15,C15;
wire [22:0] R15;
wire [23:0] A15;
full_adder_cell FAC_15_a(clk,resetn,enable,A14[23],R14[0],1'b0,C14[23],P15[23],C15[23],A15[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_15(clk,resetn,enable,A14[i],R14[0],P14[i+1],C14[i],P15[i],C15[i],A15[i]);
	end
endgenerate
dff dff_15_a(clk,resetn,enable,P14[0],R15[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_15(clk,resetn,enable,R14[i+1],R15[i]);
	end
endgenerate

//16
wire [23:0] P16,C16;
wire [22:0] R16;
wire [23:0] A16;
full_adder_cell FAC_16_a(clk,resetn,enable,A15[23],R15[0],1'b0,C15[23],P16[23],C16[23],A16[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_16(clk,resetn,enable,A15[i],R15[0],P15[i+1],C15[i],P16[i],C16[i],A16[i]);
	end
endgenerate
dff dff_16_a(clk,resetn,enable,P15[0],R16[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_16(clk,resetn,enable,R15[i+1],R16[i]);
	end
endgenerate

//17
wire [23:0] P17,C17;
wire [22:0] R17;
wire [23:0] A17;
full_adder_cell FAC_17_a(clk,resetn,enable,A16[23],R16[0],1'b0,C16[23],P17[23],C17[23],A17[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_17(clk,resetn,enable,A16[i],R16[0],P16[i+1],C16[i],P17[i],C17[i],A17[i]);
	end
endgenerate
	dff dff_17_a(clk,resetn,enable,P16[0],R17[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_17(clk,resetn,enable,R16[i+1],R17[i]);
	end
endgenerate

//18
wire [23:0] P18,C18;
wire [22:0] R18;
wire [23:0] A18;
full_adder_cell FAC_18_a(clk,resetn,enable,A17[23],R17[0],1'b0,C17[23],P18[23],C18[23],A18[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_18(clk,resetn,enable,A17[i],R17[0],P17[i+1],C17[i],P18[i],C18[i],A18[i]);
	end
endgenerate
dff dff_18_a(clk,resetn,enable,P17[0],R18[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_18(clk,resetn,enable,R17[i+1],R18[i]);
	end
endgenerate

//19
wire [23:0] P19,C19;
wire [22:0] R19;
wire [23:0] A19;
full_adder_cell FAC_19_a(clk,resetn,enable,A18[23],R18[0],1'b0,C18[23],P19[23],C19[23],A19[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_19(clk,resetn,enable,A18[i],R18[0],P18[i+1],C18[i],P19[i],C19[i],A19[i]);
	end
endgenerate
dff dff_19_a(clk,resetn,enable,P18[0],R19[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_19(clk,resetn,enable,R18[i+1],R19[i]);
	end
endgenerate

//20
wire [23:0] P20,C20;
wire [22:0] R20;
wire [23:0] A20;
full_adder_cell FAC_20_a(clk,resetn,enable,A19[23],R19[0],1'b0,C19[23],P20[23],C20[23],A20[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_20(clk,resetn,enable,A19[i],R19[0],P19[i+1],C19[i],P20[i],C20[i],A20[i]);
	end
endgenerate
dff dff_20_a(clk,resetn,enable,P19[0],R20[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_20(clk,resetn,enable,R19[i+1],R20[i]);
	end
endgenerate

//21
wire [23:0] P21,C21;
wire [22:0] R21;
wire [23:0] A21;
full_adder_cell FAC_21_a(clk,resetn,enable,A20[23],R20[0],1'b0,C20[23],P21[23],C21[23],A21[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_21(clk,resetn,enable,A20[i],R20[0],P20[i+1],C20[i],P21[i],C21[i],A21[i]);
	end
endgenerate
dff dff_21_a(clk,resetn,enable,P20[0],R21[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_21(clk,resetn,enable,R20[i+1],R21[i]);
	end
endgenerate

//22
wire [23:0] P22,C22;
wire [22:0] R22;
wire [23:0] A22;
full_adder_cell FAC_22_a(clk,resetn,enable,A21[23],R21[0],1'b0,C21[23],P22[23],C22[23],A22[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_22(clk,resetn,enable,A21[i],R21[0],P21[i+1],C21[i],P22[i],C22[i],A22[i]);
	end
endgenerate
dff dff_22_a(clk,resetn,enable,P21[0],R22[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_22(clk,resetn,enable,R21[i+1],R22[i]);
	end
endgenerate

//23
wire [23:0] P23,C23;
wire [22:0] R23;
wire [23:0] A23;
full_adder_cell FAC_23_a(clk,resetn,enable,A22[23],R22[0],1'b0,C22[23],P23[23],C23[23],A23[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_23(clk,resetn,enable,A22[i],R22[0],P22[i+1],C22[i],P23[i],C23[i],A23[i]);
	end
endgenerate
dff dff_23_a(clk,resetn,enable,P22[0],R23[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_23(clk,resetn,enable,R22[i+1],R23[i]);
	end
endgenerate

//24
wire [23:0] P24,C24;
wire [22:0] R24;
wire [23:0] A24;
full_adder_cell FAC_24_a(clk,resetn,enable,A23[23],R23[0],1'b0,C23[23],P24[23],C24[23],A24[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		full_adder_cell FAC_24(clk,resetn,enable,A23[i],R23[0],P23[i+1],C23[i],P24[i],C24[i],A24[i]);
	end
endgenerate
 dff dff_24_a(clk,resetn,enable,P23[0],R24[22]);
generate 
	for(i = 0; i < 22; i= i+1) begin
		dff dff_24(clk,resetn,enable,R23[i+1],R24[i]);
	end
endgenerate
//////////////////////////////////////////////////////////////
//25
wire [21:0] P25,C25;
wire [23:0] R25;
wire S25_1;
wire CO25_1;
generate
	for(i = 1;i < 23; i= i +1) begin
		dff dff_25_a(clk,resetn,enable,C24[i],C25[i-1]);
	end
endgenerate
generate
	for(i = 2;i < 24; i= i +1) begin
		dff dff_25_b(clk,resetn,enable,P24[i],P25[i-2]);
	end
endgenerate
full_adder_latched FAL_1(clk,resetn,enable,C24[0],P24[1],1'b0,S25_1,CO25_1);
dff dff_25_c(clk,resetn,enable,P24[0],R25[23]);
generate 
	for(i = 0; i < 23; i= i+1) begin
		dff dff_25(clk,resetn,enable,R24[i],R25[i]);
	end
endgenerate

//26
wire [20:0] P26,C26;
wire [23:0] R26;
wire S26_1;
wire CO26_2;
wire S26_2;
generate
	for(i = 1;i < 22; i= i +1) begin
		dff dff_26(clk,resetn,enable,C25[i],C26[i-1]);
		dff dff_26_a(clk,resetn,enable,P25[i],P26[i-1]);
	end
endgenerate

full_adder_latched FAL_2(clk,resetn,enable,C25[0],P25[0],CO25_1,S26_2,CO26_2);
dff dff_26_1(clk,resetn,enable,S25_1,S26_1);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_26_b(clk,resetn,enable,R25[i],R26[i]);
	end
endgenerate

//27
wire [19:0] P27,C27;
wire [23:0] R27;
wire S27_1;
wire S27_2;
wire CO27_3;
wire S27_3;
generate
	for(i = 1;i < 21; i= i +1) begin
		dff dff_27(clk,resetn,enable,C26[i],C27[i-1]);
		dff dff_27_a(clk,resetn,enable,P26[i],P27[i-1]);
	end
endgenerate

full_adder_latched FAL_3(clk,resetn,enable,C26[0],P26[0],CO26_2,S27_3,CO27_3);
dff dff_27_1(clk,resetn,enable,S26_1,S27_1);
dff dff_27_2(clk,resetn,enable,S26_2,S27_2);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_27_b(clk,resetn,enable,R26[i],R27[i]);
	end
endgenerate

//28
wire [18:0] P28,C28;
wire [23:0] R28;
wire S28_1;
wire S28_2;
wire S28_3;
wire CO28_4;
wire S28_4;
generate
	for(i = 1;i < 20; i= i +1) begin
		dff dff_28(clk,resetn,enable,C27[i],C28[i-1]);
		dff dff_28_a(clk,resetn,enable,P27[i],P28[i-1]);
	end
endgenerate

full_adder_latched FAL_4(clk,resetn,enable,C27[0],P27[0],CO27_3,S28_4,CO28_4);
dff dff_28_1(clk,resetn,enable,S27_1,S28_1);
dff dff_28_2(clk,resetn,enable,S27_2,S28_2);
dff dff_28_3(clk,resetn,enable,S27_3,S28_3);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_28_b(clk,resetn,enable,R27[i],R28[i]);
	end
endgenerate

//29
wire [17:0] P29,C29;
wire [23:0] R29;
wire S29_1;
wire S29_2;
wire S29_3;
wire S29_4;
wire CO29_5;
wire S29_5;
generate
	for(i = 1;i < 19; i= i +1) begin
		dff dff_29(clk,resetn,enable,C28[i],C29[i-1]);
		dff dff_29_a(clk,resetn,enable,P28[i],P29[i-1]);
	end
endgenerate

full_adder_latched FAL_5(clk,resetn,enable,C28[0],P28[0],CO28_4,S29_5,CO29_5);
dff dff_29_1(clk,resetn,enable,S28_1,S29_1);
dff dff_29_2(clk,resetn,enable,S28_2,S29_2);
dff dff_29_3(clk,resetn,enable,S28_3,S29_3);
dff dff_29_4(clk,resetn,enable,S28_4,S29_4);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_29_b(clk,resetn,enable,R28[i],R29[i]);
	end
endgenerate

//30
wire [16:0] P30,C30;
wire [23:0] R30;
wire S30_1;
wire S30_2;
wire S30_3;
wire S30_4;
wire S30_5;
wire S30_6;
wire CO30_6;

generate
	for(i = 1;i < 18; i= i +1) begin
		dff dff_30(clk,resetn,enable,C29[i],C30[i-1]);
		dff dff_30_a(clk,resetn,enable,P29[i],P30[i-1]);
	end
endgenerate

full_adder_latched FAL_6(clk,resetn,enable,C29[0],P29[0],CO29_5,S30_6,CO30_6);
dff dff_30_1(clk,resetn,enable,S29_1,S30_1);
dff dff_30_2(clk,resetn,enable,S29_2,S30_2);
dff dff_30_3(clk,resetn,enable,S29_3,S30_3);
dff dff_30_4(clk,resetn,enable,S29_4,S30_4);
dff dff_30_5(clk,resetn,enable,S29_5,S30_5);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_30_b(clk,resetn,enable,R29[i],R30[i]);
	end
endgenerate

//31
wire [15:0] P31,C31;
wire [23:0] R31;
wire S31_1;
wire S31_2;
wire S31_3;
wire S31_4;
wire S31_5;
wire S31_6;
wire S31_7;
wire CO31_7;

generate
	for(i = 1;i < 17; i= i +1) begin
		dff dff_31(clk,resetn,enable,C30[i],C31[i-1]);
		dff dff_31_a(clk,resetn,enable,P30[i],P31[i-1]);
	end
endgenerate

full_adder_latched FAL_7(clk,resetn,enable,C30[0],P30[0],CO30_6,S31_7,CO31_7);
dff dff_31_1(clk,resetn,enable,S30_1,S31_1);
dff dff_31_2(clk,resetn,enable,S30_2,S31_2);
dff dff_31_3(clk,resetn,enable,S30_3,S31_3);
dff dff_31_4(clk,resetn,enable,S30_4,S31_4);
dff dff_31_5(clk,resetn,enable,S30_5,S31_5);
dff dff_31_6(clk,resetn,enable,S30_6,S31_6);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_31_b(clk,resetn,enable,R30[i],R31[i]);
	end
endgenerate

//32
wire [14:0] P32,C32;
wire [23:0] R32;
wire S32_1;
wire S32_2;
wire S32_3;
wire S32_4;
wire S32_5;
wire S32_6;
wire S32_7;
wire S32_8;
wire CO32_8;

generate
	for(i = 1;i < 16; i= i +1) begin
		dff dff_32(clk,resetn,enable,C31[i],C32[i-1]);
		dff dff_32_a(clk,resetn,enable,P31[i],P32[i-1]);
	end
endgenerate

full_adder_latched FAL_8(clk,resetn,enable,C31[0],P31[0],CO31_7,S32_8,CO32_8);
dff dff_32_1(clk,resetn,enable,S31_1,S32_1);
dff dff_32_2(clk,resetn,enable,S31_2,S32_2);
dff dff_32_3(clk,resetn,enable,S31_3,S32_3);
dff dff_32_4(clk,resetn,enable,S31_4,S32_4);
dff dff_32_5(clk,resetn,enable,S31_5,S32_5);
dff dff_32_6(clk,resetn,enable,S31_6,S32_6);
dff dff_32_7(clk,resetn,enable,S31_7,S32_7);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_32_b(clk,resetn,enable,R31[i],R32[i]);
	end
endgenerate

//33
wire [13:0] P33,C33;
wire [23:0] R33;
wire S33_1;
wire S33_2;
wire S33_3;
wire S33_4;
wire S33_5;
wire S33_6;
wire S33_7;
wire S33_8;
wire S33_9;
wire CO33_9;

generate
	for(i = 1;i < 15; i= i +1) begin
		dff dff_33(clk,resetn,enable,C32[i],C33[i-1]);
		dff dff_33_a(clk,resetn,enable,P32[i],P33[i-1]);
	end
endgenerate

full_adder_latched FAL_9(clk,resetn,enable,C32[0],P32[0],CO32_8,S33_9,CO33_9);
dff dff_33_1(clk,resetn,enable,S32_1,S33_1);
dff dff_33_2(clk,resetn,enable,S32_2,S33_2);
dff dff_33_3(clk,resetn,enable,S32_3,S33_3);
dff dff_33_4(clk,resetn,enable,S32_4,S33_4);
dff dff_33_5(clk,resetn,enable,S32_5,S33_5);
dff dff_33_6(clk,resetn,enable,S32_6,S33_6);
dff dff_33_7(clk,resetn,enable,S32_7,S33_7);
dff dff_33_8(clk,resetn,enable,S32_8,S33_8);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_33_b(clk,resetn,enable,R32[i],R33[i]);
	end
endgenerate

//34
wire [12:0] P34,C34;
wire [23:0] R34;
wire S34_1;
wire S34_2;
wire S34_3;
wire S34_4;
wire S34_5;
wire S34_6;
wire S34_7;
wire S34_8;
wire S34_9;
wire S34_10;
wire CO34_10;

generate
	for(i = 1;i < 14; i= i +1) begin
		dff dff_34(clk,resetn,enable,C33[i],C34[i-1]);
		dff dff_34_a(clk,resetn,enable,P33[i],P34[i-1]);
	end
endgenerate

full_adder_latched FAL_10(clk,resetn,enable,C33[0],P33[0],CO33_9,S34_10,CO34_10);
dff dff_34_1(clk,resetn,enable,S33_1,S34_1);
dff dff_34_2(clk,resetn,enable,S33_2,S34_2);
dff dff_34_3(clk,resetn,enable,S33_3,S34_3);
dff dff_34_4(clk,resetn,enable,S33_4,S34_4);
dff dff_34_5(clk,resetn,enable,S33_5,S34_5);
dff dff_34_6(clk,resetn,enable,S33_6,S34_6);
dff dff_34_7(clk,resetn,enable,S33_7,S34_7);
dff dff_34_8(clk,resetn,enable,S33_8,S34_8);
dff dff_34_9(clk,resetn,enable,S33_9,S34_9);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_34_b(clk,resetn,enable,R33[i],R34[i]);
	end
endgenerate

//35 // toi day roi
wire [11:0] P35,C35;
wire [23:0] R35;
wire [10:0] S35;
wire CO35_11;

generate
	for(i = 1;i < 13; i= i +1) begin
		dff dff_35(clk,resetn,enable,C34[i],C35[i-1]);
		dff dff_35_a(clk,resetn,enable,P34[i],P35[i-1]);
	end
endgenerate

full_adder_latched FAL_11(clk,resetn,enable,C34[0],P34[0],CO34_10,S35[10],CO35_11);
nbit_dff #(10) nbit_dff_35(clk,resetn,enable,{S34_10,S34_9,S34_8,S34_7,S34_6,S34_5,S34_4,S34_3,S34_2,S34_1},S35[9:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_35_b(clk,resetn,enable,R34[i],R35[i]);
	end
endgenerate

//36 
wire [10:0] P36,C36;
wire [23:0] R36;
wire [11:0] S36;
wire CO36_12;

generate
	for(i = 1;i < 12; i= i +1) begin
		dff dff_36(clk,resetn,enable,C35[i],C36[i-1]);
		dff dff_36_a(clk,resetn,enable,P35[i],P36[i-1]);
	end
endgenerate

full_adder_latched FAL_12(clk,resetn,enable,C35[0],P35[0],CO35_11,S36[11],CO36_12);
nbit_dff #(11) nbit_dff_36(clk,resetn,enable,S35[10:0],S36[10:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_36_b(clk,resetn,enable,R35[i],R36[i]);
	end
endgenerate

//37
wire [9:0] P37,C37;
wire [23:0] R37;
wire [12:0] S37;
wire CO37_13;

generate
	for(i = 1;i < 11; i= i +1) begin
		dff dff_37(clk,resetn,enable,C36[i],C37[i-1]);
		dff dff_37_a(clk,resetn,enable,P36[i],P37[i-1]);
	end
endgenerate

full_adder_latched FAL_13(clk,resetn,enable,C36[0],P36[0],CO36_12,S37[12],CO37_13);
nbit_dff #(12) nbit_dff_37(clk,resetn,enable,S36[11:0],S37[11:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_37_b(clk,resetn,enable,R36[i],R37[i]);
	end
endgenerate

//38
wire [8:0] P38,C38;
wire [23:0] R38;
wire [13:0] S38;
wire CO38_14;

generate
	for(i = 1;i < 10; i= i +1) begin
		dff dff_38(clk,resetn,enable,C37[i],C38[i-1]);
		dff dff_38_a(clk,resetn,enable,P37[i],P38[i-1]);
	end
endgenerate

full_adder_latched FAL_14(clk,resetn,enable,C37[0],P37[0],CO37_13,S38[13],CO38_14);
nbit_dff #(13) nbit_dff_38(clk,resetn,enable,S37[12:0],S38[12:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_38_b(clk,resetn,enable,R37[i],R38[i]);
	end
endgenerate

//39
wire [7:0] P39,C39;
wire [23:0] R39;
wire [14:0] S39;
wire CO39_15;

generate
	for(i = 1;i < 9; i= i +1) begin
		dff dff_39(clk,resetn,enable,C38[i],C39[i-1]);
		dff dff_39_a(clk,resetn,enable,P38[i],P39[i-1]);
	end
endgenerate

full_adder_latched FAL_15(clk,resetn,enable,C38[0],P38[0],CO38_14,S39[14],CO39_15);
nbit_dff #(14) nbit_dff_39(clk,resetn,enable,S38[13:0],S39[13:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_39_b(clk,resetn,enable,R38[i],R39[i]);
	end
endgenerate

//40
wire [6:0] P40,C40;
wire [23:0] R40;
wire [15:0] S40;
wire CO40_16;

generate
	for(i = 1;i < 8; i= i +1) begin
		dff dff_40(clk,resetn,enable,C39[i],C40[i-1]);
		dff dff_40_a(clk,resetn,enable,P39[i],P40[i-1]);
	end
endgenerate

full_adder_latched FAL_16(clk,resetn,enable,C39[0],P39[0],CO39_15,S40[15],CO40_16);
nbit_dff #(15) nbit_dff_40(clk,resetn,enable,S39[14:0],S40[14:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_40_b(clk,resetn,enable,R39[i],R40[i]);
	end
endgenerate

//41
wire [5:0] P41,C41;
wire [23:0] R41;
wire [16:0] S41;
wire CO41_17;

generate
	for(i = 1;i < 7; i= i +1) begin
		dff dff_41(clk,resetn,enable,C40[i],C41[i-1]);
		dff dff_41_a(clk,resetn,enable,P40[i],P41[i-1]);
	end
endgenerate

full_adder_latched FAL_17(clk,resetn,enable,C40[0],P40[0],CO40_16,S41[16],CO41_17);
nbit_dff #(16) nbit_dff_41(clk,resetn,enable,S40[15:0],S41[15:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_41_b(clk,resetn,enable,R40[i],R41[i]);
	end
endgenerate

//42
wire [4:0] P42,C42;
wire [23:0] R42;
wire [17:0] S42;
wire CO42_18;

generate
	for(i = 1;i < 6; i= i +1) begin
		dff dff_42(clk,resetn,enable,C41[i],C42[i-1]);
		dff dff_42_a(clk,resetn,enable,P41[i],P42[i-1]);
	end
endgenerate

full_adder_latched FAL_18(clk,resetn,enable,C41[0],P41[0],CO41_17,S42[17],CO42_18);
nbit_dff #(17) nbit_dff_42(clk,resetn,enable,S41[16:0],S42[16:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_42_b(clk,resetn,enable,R41[i],R42[i]);
	end
endgenerate

//43
wire [3:0] P43,C43;
wire [23:0] R43;
wire [18:0] S43;
wire CO43_19;

generate
	for(i = 1;i < 5; i= i +1) begin
		dff dff_43(clk,resetn,enable,C42[i],C43[i-1]);
		dff dff_43_a(clk,resetn,enable,P42[i],P43[i-1]);
	end
endgenerate

full_adder_latched FAL_19(clk,resetn,enable,C42[0],P42[0],CO42_18,S43[18],CO43_19);
nbit_dff #(18) nbit_dff_43(clk,resetn,enable,S42[17:0],S43[17:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_43_b(clk,resetn,enable,R42[i],R43[i]);
	end
endgenerate

//44
wire [2:0] P44,C44;
wire [23:0] R44;
wire [19:0] S44;
wire CO44_20;

generate
	for(i = 1;i < 4; i= i +1) begin
		dff dff_44(clk,resetn,enable,C43[i],C44[i-1]);
		dff dff_44_a(clk,resetn,enable,P43[i],P44[i-1]);
	end
endgenerate

full_adder_latched FAL_20(clk,resetn,enable,C43[0],P43[0],CO43_19,S44[19],CO44_20);
nbit_dff #(19) nbit_dff_44(clk,resetn,enable,S43[18:0],S44[18:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_44_b(clk,resetn,enable,R43[i],R44[i]);
	end
endgenerate

//45
wire [1:0] P45,C45;
wire [23:0] R45;
wire [20:0] S45;
wire CO45_21;

generate
	for(i = 1;i < 3; i= i +1) begin
		dff dff_45(clk,resetn,enable,C44[i],C45[i-1]);
		dff dff_45_a(clk,resetn,enable,P44[i],P45[i-1]);
	end
endgenerate

full_adder_latched FAL_21(clk,resetn,enable,C44[0],P44[0],CO44_20,S45[20],CO45_21);
nbit_dff #(20) nbit_dff_45(clk,resetn,enable,S44[19:0],S45[19:0]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_45_b(clk,resetn,enable,R44[i],R45[i]);
	end
endgenerate

//46
wire P46,C46;
wire [23:0] R46;
wire S46;
wire CO46_22;


dff dff_46_1(clk,resetn,enable,C45[1],C46);
dff dff_46_2(clk,resetn,enable,P45[1],P46);


full_adder_latched FAL_22(clk,resetn,enable,C45[0],P45[0],CO45_21,S46,CO46_22);
delay_clock #(21,2) delay_46(clk,resetn,enable,S45[20:0],p[44:24]);
dff dff_ins_58(clk,resetn,enable,S46,p[45]);
generate 
	for(i = 0; i < 24; i= i+1) begin
		dff dff_46_b(clk,resetn,enable,R45[i],R46[i]);
	end
endgenerate

//47
full_adder_latched FAL_23(clk,resetn,enable,C46,P46,CO46_22,p[46],p[47]);
nbit_dff #(24) nbit_dff_47(clk,resetn,enable,R46[23:0],p[23:0]);


endmodule
