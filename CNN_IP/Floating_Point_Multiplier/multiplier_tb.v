`timescale 1ns/1ps

module multiplier_tb();
	reg clk, enable, reset;
	reg valid;
	reg [23:0] x,y;
	wire [47:0] p;
	//wire out_en;
	integer clk_count;
	wire [23:0] x_bk,y_bk;
	mult_24bit inst(clk,reset,enable,valid,x,y,p);
	delay_clock #(.DATA_WIDTH(24),.N_CLOCKs(48)) store_x(clk,reset,enable,x,x_bk);
    delay_clock #(.DATA_WIDTH(24),.N_CLOCKs(48)) store_y(clk,reset,enable,y,y_bk);
	//multiplier inst(clk,start,reset,x,y,p);
	always #5 clk= ~clk;
	
	always @(posedge clk) begin
		if(enable == 1'b0) clk_count <=0;
		else if(enable == 1'b1) begin
				clk_count <= clk_count +1;
				$display("------Number of periods: %d", clk_count);
				if ( clk_count >= 47 && p == x_bk*y_bk  ) $display("X %d , X %d ,P %b, --->PASS",x_bk,y_bk,p);
            	else if ( clk_count >= 47 && p != x_bk*y_bk ) $display("X %d , Y %d ,P %b, --->FAIL",x_bk,y_bk,p);
				else $display("X %d , Y %d ,P %d",x_bk,y_bk,p);
				end
	end
	initial begin
		//$monitor("time= %d, clk= %b, enable= %b, reset= %b, x= %d, y= %d, p= %b",$time,clk,enable,reset,x,y,p);
		clk <= 0;
		enable <= 0;
		reset <= 1;
		valid <=1;
		#10 enable <= 1;
		#5 x <= 24'b100000000000000000000000;
		y <= 24'b100011010000000000000000;
		#10 y <= 24'b100000000000000000000000;
		x <= 24'b100011010000000000000000;
		
		
		/*
		#10 x <= 24'd0;
		y<= 24'd20;
		#10 x <= 24'd0;
		y<= 24'd11;
		#10 x <= 24'd0;
		y<= 24'd99;
		#10 y <= 0;
		x <= 24'd45;
		//stimulus();
		*/
		//#10 x <= 24'd34;
		//y<= 24'd95;
		#2000 $finish;
	end

	task stimulus;
		begin
			repeat(200) begin
			#10 x<= $random;
            
            y <= $random;
            //d <= 
    
            //ci <= $random;
	    end
    end
    endtask
endmodule
