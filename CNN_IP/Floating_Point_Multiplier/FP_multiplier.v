/*
`include "sign.v"
`include "exponent_add.v"
`include "mult_24bit.v"
`include "normalizer.v"
`include "../Lib_Cells/delay_clock.v"
*/
module FP_multiplier(
			clk,
			resetn,
			enable,
			valid_in,
			//FP1,
			//FP2,
			in1,
			in2,
			product,
			valid_out
			//ovf
);	
	input clk;
	input resetn;
	input enable;
	input valid_in;
	input [31:0] in1;
	input [31:0] in2;
	output [31:0] product;
	output valid_out;
	wire ovf;
	
	reg [31:0] FP1;
	reg [31:0] FP2;
	wire [31:0] temp_output;
	wire temp_ovf;
	wire [47:0] temp_48;
	reg [5:0] counter;
	//wire out_en;
	//assign out_en = (counter == 0)?1'b1:1'b0;
	//assign product = (counter == 6'd0)?temp_output:32'bx;
	assign product = (valid_out)?temp_output:32'b0;
	//assign FP1 = (valid_in)?in1:32'bx;
	//assign FP2 = (valid_in)?in2:32'bx;

	delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(50)) valid_ins(clk,resetn,enable,valid_in,valid_out);
	sign sign_inst(clk,resetn,valid_in,FP1[31],FP2[31],temp_output[31]);
	exponent_add exponent_add_inst(clk,resetn,valid_in,FP1[30:23],FP2[30:23],temp_48[47],temp_output[30:23],temp_ovf);
	mult_24bit mult_24bit_inst(clk,resetn,enable,valid_in,{1'b1,FP1[22:0]},{1'b1,FP2[22:0]},temp_48);
	normalizer normalizer_inst(clk,resetn,valid_in,temp_48,temp_output[22:0]);
//
	wire iszero;
	assign iszero = (in1 == 32'b0 || in2 == 32'b0)?1'b1:1'b0;
	always @(posedge clk or negedge resetn) begin
			if (resetn == 1'b0) begin
				FP1 <= 0;
				FP2 <= 0;
			end 
			else if (valid_in == 1'b1) begin
				if(iszero==1'b0) begin
					FP1 <= in1;
					FP2 <= in2;
				end
				else if(iszero == 1'b1)  begin
					FP1 <= 0;
					FP2 <= 0;
				end
			end
			else if (valid_in == 1'b0) begin
				FP1 <= FP1;
				FP2 <= FP2;
			end
	end
	/*
	always @(posedge clk or negedge resetn) begin
		if(resetn == 1'b0 || enable == 1'b0) counter <= 6'd47;
			else counter <= counter -1;
	end
*/
endmodule
