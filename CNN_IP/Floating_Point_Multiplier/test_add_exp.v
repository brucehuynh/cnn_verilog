`timescale 1ns/1ps
module test_add_exp();
	reg clk, enable, reset;
	reg [7:0] exp1,exp2;
	reg increase;
	wire [7:0] exp;
	reg valid_in;
	wire ovf;
	//wire out_en;
	reg [10:0] clk_count;
	//mult_5bit inst(clk,reset,enable,x,y,p);
	//multiplier inst(clk,start,reset,x,y,p);
	exponent_add inst(clk,reset,enable,valid_in,exp1,exp2,increase,exp,ovf);
	//exponent_add m(.clk(clk),.resetn(reset),.enable(enable),.exp1(exp1),.exp2(exp2),.increase(increase),.exp(exp),.ovf(ovf));
	always #5 clk= ~clk;
	
	always @(posedge clk) begin
		if(!enable || !reset) clk_count <=0;
		else begin
				clk_count <= clk_count +1;
				$display("------Number of periods: %d", clk_count);
				//if(clk_count == 10'd45) increase <= 1;
				end
	end
	initial begin
		$monitor("time= %d, clk= %b, enable= %b, reset= %b, E1= %b, E2= %b, res= %b,ovf = %b",$time,clk,enable,reset,exp1,exp2,exp,ovf);
		clk <= 0;
		enable <= 0;
		reset <= 1;
		valid_in <= 1;
		increase <= 0;
		#10 enable <= 1;
		//#10 reset <= 0;
		//#10 reset <= 1;
		#5 exp1 <= 8'b01111111;
		exp2 <= 8'b10000110;
		#10 exp2 <= 8'b01111111;
		exp1 <= 8'b10000110;
		/*
		#10 exp1 <= 8'b10001100;
		exp2 <= 8'b10000011;
		#10 exp1 <= 8'b10011000;
		exp2 <= 8'b10100011;
		#10 exp1 <= 8'b10000001;
		exp2 <= 8'b10100010;
		#10 exp1 <= 8'b10000101;
		exp2 <= 8'b10100010;
		*/
		#900 $finish;
	end
endmodule