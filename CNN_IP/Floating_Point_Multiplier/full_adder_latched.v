/*
`include "../Lib_Cells/addbit.v"
`include "../Lib_Cells/dff.v"
*/
module full_adder_latched(
					clk,
					resetn,
					enable,
					a,
					b,
					ci, 
					s, 
					co //carry out
);
	input clk;
	input resetn;
	input enable;
	input a;
	input b;
	input ci;
	output s;
	output co;
	
	wire add_wire;
	wire carry_wire;
	
	addbit addbit_inst(a,b,ci,add_wire,carry_wire);
	dff dff_inst_0(clk,resetn,enable,add_wire,s);
	dff dff_inst_1(clk,resetn,enable,carry_wire,co);
	
endmodule
