`timescale 1ns/1ps
module test_delay();
	parameter DATA_WIDTH = 8;
	parameter N_CLOCKs = 5;

	reg clk;
	reg resetn;
	reg enable;
	reg [DATA_WIDTH-1 : 0] in;
	wire [DATA_WIDTH-1 : 0] out;
	
	
	
	delay_clock #(.DATA_WIDTH(8),.N_CLOCKs(5)) delay_clock_ins(clk,resetn,enable,in,out);
	
	always #5 clk = ~clk;
	
	initial begin
		$monitor("Time= %d, clk= %b,in= %d, resetn= %b, enable= %b, out= %d",$time,clk,in,resetn,enable,out);
		clk = 0;
		resetn = 0;
		in = 8'hFF;
		#1 enable = 0;
		#2 resetn = 1;
		enable = 1;
		#5 in = 8'b00000000;
		#10 in = 8'b00000001;
		#10 in = 8'b00000010;
		#10 in = 8'b00000011;
		#10 in = 8'b00000100;
		#10 in = 8'b00000101;
		#10 resetn = 0;
		#10 resetn = 1;
		#10 in = 8'b00000111;
		#10 in = 8'b00001000;
		#10 in = 8'b00001001;
		#10 in = 8'b00001010;
		#10 in = 8'b00001011;
		#10 enable = 0;
		#10 in = 8'b00001100;
		#10 in = 8'b00001101;
		#10 in = 8'b00001110;
		#10 in = 8'b00001111;
		#30 $finish;
	end
	
endmodule