/*
`include "../Lib_Cells/adder.v"
`include "../Lib_Cells/subtractor.v"
`include "../Lib_Cells/delay_clock.v"
`include "../Lib_Cells/nbit_dff.v"
//`include ../Lib_Cells/adder.v
*/
module exponent_add(
						clk,
						resetn,
						enable,
						//valid_in,
						in1,
						in2,
						increase,
						exp,
						ovf
);
	input clk;
	input resetn;
	input enable;
	//input valid_in;
	input [7:0] in1;
	input [7:0] in2;
	input increase;
	output [7:0] exp;
	output ovf;
	
	wire [7:0] A;
	//wire [7:0] X;
	wire [8:0] Y1,Y2;
	wire [7:0] Z1,Z2;
	wire [8:0] B;
	wire ovf1,ovf2;
	wire c1,c2,c3;

	reg [8:0] W1,W2;
	
	
	adder #(.DATA_WIDTH(8)) m_adder (.s(A),.co(c1),.ci(1'b0),.a(in1),.b(in2));

	always @ (posedge clk or negedge resetn) begin
	  	if (resetn == 1'b0) begin
			W1 <= 0;
			W2 <= 0;
		  end
		  else if (enable == 1'b1) begin
		  	if ({c1,A} == 9'b0)begin
				W1 <= 0;
				W2 <= 0;
		  	end
			else if ({c1,A} != 9'b0) begin
				W1 <= 9'b00111_1111;
				W2 <= 9'b00111_1110;
			end
		  end
		  else if (enable == 1'b0)begin
			W1 <= 0;
			W2 <= 0;
		  end
	end	
	
	//assign {c1,A} = exp1 + exp2;
	
	//(in1,in2,out,co); của Triết
	//adder_nbit #(8) m_adder (in1,in2,A,c1);
	
	//( a ,b ,c ,diff ,borrow );
	nbit_dff #(9) nbit_dff_A(clk,resetn,enable,{c1,A},B);
	subtractor #(9) sub_0(B,W1,1'b0,Y1,c2); //-127
	delay_clock #(.DATA_WIDTH(9),.N_CLOCKs(47)) delay_clock_ins(clk,resetn,enable,Y1,{ovf1,Z1});
	subtractor #(9) sub_1(B,W2,1'b0,Y2,c3); //-126
	delay_clock #(.DATA_WIDTH(9),.N_CLOCKs(47)) delay_clock_ins_1(clk,resetn,enable,Y2,{ovf2,Z2});
	
	
	wire [8:0] temp;
	assign temp = (increase)?{ovf2,Z2}:{ovf1,Z1};
	nbit_dff #(9) nbit_dff_ins3(clk,resetn,enable,temp,{ovf,exp});
	
endmodule
	
