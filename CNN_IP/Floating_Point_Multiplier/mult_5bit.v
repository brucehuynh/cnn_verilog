/*
`include "full_adder_cell.v"
`include "full_adder_latched.v"
`include "../Lib_Cells/dff.v"
*/
module mult_5bit(
				clk,
				resetn,
				enable,
				a,
				b,
				p
);
	input clk;
	input resetn;
	input enable;
	input [4:0] a;
	input [4:0] b;
	output [9:0] p;
	
	wire [4:0] P1,C1;
	wire [3:0] R1;
	wire [4:0] A1;
	//1
	full_adder_cell FAC_1(clk,resetn,enable,a[4],b[0],1'b0,1'b0,P1[4],C1[4],A1[4]);
	full_adder_cell FAC_2(clk,resetn,enable,a[3],b[0],1'b0,1'b0,P1[3],C1[3],A1[3]);
	full_adder_cell FAC_3(clk,resetn,enable,a[2],b[0],1'b0,1'b0,P1[2],C1[2],A1[2]);
	full_adder_cell FAC_4(clk,resetn,enable,a[1],b[0],1'b0,1'b0,P1[1],C1[1],A1[1]);
	full_adder_cell FAC_5(clk,resetn,enable,a[0],b[0],1'b0,1'b0,P1[0],C1[0],A1[0]);
	dff dff_ins_1(clk,resetn,enable,b[4],R1[3]);
	dff dff_ins_2(clk,resetn,enable,b[3],R1[2]);
	dff dff_ins_3(clk,resetn,enable,b[2],R1[1]);
	dff dff_ins_4(clk,resetn,enable,b[1],R1[0]);
	//nbit_dff#(4) nbit_dff_ins_1(clk,resetn,enable,b[4:1],R1[3:0]);
	
	wire [4:0] P2,C2;
	wire [3:0] R2;
	wire [4:0] A2;
	//2
	full_adder_cell FAC_6(clk,resetn,enable,A1[4],R1[0],1'b0,C1[4],P2[4],C2[4],A2[4]);
	full_adder_cell FAC_7(clk,resetn,enable,A1[3],R1[0],P1[4],C1[3],P2[3],C2[3],A2[3]);
	full_adder_cell FAC_8(clk,resetn,enable,A1[2],R1[0],P1[3],C1[2],P2[2],C2[2],A2[2]);
	full_adder_cell FAC_9(clk,resetn,enable,A1[1],R1[0],P1[2],C1[1],P2[1],C2[1],A2[1]);
	full_adder_cell FAC_10(clk,resetn,enable,A1[0],R1[0],P1[1],C1[0],P2[0],C2[0],A2[0]);
	dff dff_ins_5(clk,resetn,enable,P1[0],R2[3]);
	dff dff_ins_6(clk,resetn,enable,R1[3],R2[2]);
	dff dff_ins_7(clk,resetn,enable,R1[2],R2[1]);
	dff dff_ins_8(clk,resetn,enable,R1[1],R2[0]);
	//dff	dff_ins_2(clk,resetn,enable,P1[0],R2[3]);
	//nbit_dff#(3) nbit_dff_ins_2(clk,resetn,enable,R1[3:1],R2[2:0]);
	
	wire [4:0] P3,C3;
	wire [3:0] R3;
	wire [4:0] A3;
	//3
	full_adder_cell FAC_11(clk,resetn,enable,A2[4],R2[0],1'b0,C2[4],P3[4],C3[4],A3[4]);
	full_adder_cell FAC_12(clk,resetn,enable,A2[3],R2[0],P2[4],C2[3],P3[3],C3[3],A3[3]);
	full_adder_cell FAC_13(clk,resetn,enable,A2[2],R2[0],P2[3],C2[2],P3[2],C3[2],A3[2]);
	full_adder_cell FAC_14(clk,resetn,enable,A2[1],R2[0],P2[2],C2[1],P3[1],C3[1],A3[1]);
	full_adder_cell FAC_15(clk,resetn,enable,A2[0],R2[0],P2[1],C2[0],P3[0],C3[0],A3[0]);
	dff dff_ins_9(clk,resetn,enable,P2[0],R3[3]);
	dff dff_ins_10(clk,resetn,enable,R2[3],R3[2]);
	dff dff_ins_11(clk,resetn,enable,R2[2],R3[1]);
	dff dff_ins_12(clk,resetn,enable,R2[1],R3[0]);
	
	wire [4:0] P4,C4;
	wire [3:0] R4;
	wire [4:0] A4;
	//4
	full_adder_cell FAC_16(clk,resetn,enable,A3[4],R3[0],1'b0,C3[4],P4[4],C4[4],A4[4]);
	full_adder_cell FAC_17(clk,resetn,enable,A3[3],R3[0],P3[4],C3[3],P4[3],C4[3],A4[3]);
	full_adder_cell FAC_18(clk,resetn,enable,A3[2],R3[0],P3[3],C3[2],P4[2],C4[2],A4[2]);
	full_adder_cell FAC_19(clk,resetn,enable,A3[1],R3[0],P3[2],C3[1],P4[1],C4[1],A4[1]);
	full_adder_cell FAC_20(clk,resetn,enable,A3[0],R3[0],P3[1],C3[0],P4[0],C4[0],A4[0]);
	dff dff_ins_13(clk,resetn,enable,P3[0],R4[3]);
	dff dff_ins_14(clk,resetn,enable,R3[3],R4[2]);
	dff dff_ins_15(clk,resetn,enable,R3[2],R4[1]);
	dff dff_ins_16(clk,resetn,enable,R3[1],R4[0]);
	
	wire [4:0] P5,C5;
	wire [3:0] R5;
	wire [4:0] A5;
	//5
	full_adder_cell FAC_21(clk,resetn,enable,A4[4],R4[0],1'b0,C4[4],P5[4],C5[4],A5[4]);
	full_adder_cell FAC_22(clk,resetn,enable,A4[3],R4[0],P4[4],C4[3],P5[3],C5[3],A5[3]);
	full_adder_cell FAC_23(clk,resetn,enable,A4[2],R4[0],P4[3],C4[2],P5[2],C5[2],A5[2]);
	full_adder_cell FAC_24(clk,resetn,enable,A4[1],R4[0],P4[2],C4[1],P5[1],C5[1],A5[1]);
	full_adder_cell FAC_25(clk,resetn,enable,A4[0],R4[0],P4[1],C4[0],P5[0],C5[0],A5[0]);
	dff dff_ins_17(clk,resetn,enable,P4[0],R5[3]);
	dff dff_ins_18(clk,resetn,enable,R4[3],R5[2]);
	dff dff_ins_19(clk,resetn,enable,R4[2],R5[1]);
	dff dff_ins_20(clk,resetn,enable,R4[1],R5[0]);
	
	wire [2:0] P6,C6;
	wire [4:0] R6;
	wire s1,co1,s01;
	//6
	dff dff_ins_21(clk,resetn,enable,P5[4],P6[2]);
	dff dff_ins_22(clk,resetn,enable,P5[3],P6[1]);
	dff dff_ins_23(clk,resetn,enable,P5[2],P6[0]);
	dff dff_ins_24(clk,resetn,enable,C5[3],C6[2]);
	dff dff_ins_25(clk,resetn,enable,C5[2],C6[1]);
	dff dff_ins_26(clk,resetn,enable,C5[1],C6[0]);
	full_adder_latched FAL_1(clk,resetn,enable,P5[1],C5[0],1'b0,s1,co1);
	dff dff_ins_27(clk,resetn,enable,P5[0],R6[4]);
	dff dff_ins_28(clk,resetn,enable,R5[3],R6[3]);
	dff dff_ins_29(clk,resetn,enable,R5[2],R6[2]);
	dff dff_ins_30(clk,resetn,enable,R5[1],R6[1]);
	dff dff_ins_31(clk,resetn,enable,R5[0],R6[0]);
	dff dff_ins_53(clk,resetn,enable,s1,s01);
	
	wire [1:0] P7,C7;
	wire [4:0] R7;
	wire s2,co2;
	wire s_01;
	wire s02;
	//7
	dff dff_ins_32(clk,resetn,enable,P6[2],P7[1]);
	dff dff_ins_33(clk,resetn,enable,C6[2],C7[1]);
	dff dff_ins_34(clk,resetn,enable,P6[1],P7[0]);
	dff dff_ins_35(clk,resetn,enable,C6[1],C7[0]);
	full_adder_latched FAL_2(clk,resetn,enable,P6[0],C6[0],co1,s2,co2);
	dff dff_ins_36(clk,resetn,enable,R6[4],R7[4]);
	dff dff_ins_37(clk,resetn,enable,R6[3],R7[3]);
	dff dff_ins_38(clk,resetn,enable,R6[2],R7[2]);
	dff dff_ins_39(clk,resetn,enable,R6[1],R7[1]);
	dff dff_ins_40(clk,resetn,enable,R6[0],R7[0]);
	dff dff_ins_54(clk,resetn,enable,s01,s_01);
	dff dff_ins_55(clk,resetn,enable,s2,s02);
	
	wire P8,C8;
	wire [4:0] R8;
	wire s3,co3;
	wire s_11,s_12,s03;
	//8
	dff dff_ins_41(clk,resetn,enable,P7[1],P8);
	dff dff_ins_42(clk,resetn,enable,C7[1],C8);
	full_adder_latched FAL_3(clk,resetn,enable,P7[0],C7[0],co2,s3,co3);
	dff dff_ins_43(clk,resetn,enable,R7[4],R8[4]);
	dff dff_ins_44(clk,resetn,enable,R7[3],R8[3]);
	dff dff_ins_45(clk,resetn,enable,R7[2],R8[2]);
	dff dff_ins_46(clk,resetn,enable,R7[1],R8[1]);
	dff dff_ins_47(clk,resetn,enable,R7[0],R8[0]);
	dff dff_ins_56(clk,resetn,enable,s_01,p[5]);
	dff dff_ins_57(clk,resetn,enable,s02,p[6]);
	dff dff_ins_58(clk,resetn,enable,s3,p[7]);
	
	//9
	full_adder_latched FAL_4(clk,resetn,enable,P8,C8,co3,p[8],p[9]);
	dff dff_ins_48(clk,resetn,enable,R8[4],p[4]);
	dff dff_ins_49(clk,resetn,enable,R8[3],p[3]);
	dff dff_ins_50(clk,resetn,enable,R8[2],p[2]);
	dff dff_ins_51(clk,resetn,enable,R8[1],p[1]);
	dff dff_ins_52(clk,resetn,enable,R8[0],p[0]);
	//dff dff_ins_59(clk,resetn,enable,s_11,p[5]);
	//dff dff_ins_60(clk,resetn,enable,s_12,p[6]);
	//dff dff_ins_61(clk,resetn,enable,s03,p[7]);

endmodule
