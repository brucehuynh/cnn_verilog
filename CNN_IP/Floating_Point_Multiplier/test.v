module test();
	//parameter CLOCK = 20;
	reg clk;
	reg resetn;
	reg enable;
	reg [31:0] FP1;
	reg [31:0] FP2;
	/*
	reg S1;
	reg S2;
	reg [6:0] E1;
	reg [6:0] E2;
	reg [22:0] M1;
	reg [22:0] M2;
	*/
	wire [31:0] result;
	wire ovf;
	reg [10:0] clk_count;
	reg valid_in;
	//real r1;
	//real r2;
	//integer f,f1;
	//integer seed = 1;   //change this for different random sequence
	FP_multiplier FP_inst(clk,resetn,enable,valid_in,FP1,FP2,result,ovf);
	//FP_multiplier FP_inst(clk,resetn,enable,{S1,E1,1'd0,M1},{S2,E2,1'd0,M2},result,ovf);
	

	
	always #2 clk= ~clk;
	

	
	initial begin
		$monitor("Time= %d,resetn= %b, enable= %b,fp1= %h, fp2= %h, product= %b,valid_out= %b",$time,resetn,enable,FP1,FP2,result,ovf);
		clk=0;
		//resetn=0;
		//enable=0;
		clk_count =0;
		valid_in = 1;
		resetn = 1;
		enable = 1;
		#2 FP2 = 32'b01000011000001000000000000000000;
		FP1 = 32'b00111111100000000000000000000000;
		#4 FP2 = 32'b01000011000011010000000000000000;
		//FP2 = 32'b00111111100000000000000000000000;
		#4 FP2 = 32'b01000010110010000000000000000000;
		
		/*
		 00011010 26
		+00000010  2
		 00011100 28
		+10000001
		 10011101 
		#20 FP1 = 32'b00111111100110011001100110011010;
		FP2 = 32'b01000001010101001100110011001101;
		#20 FP1 = 32'b11000001010000111101011100001010;
		FP2 = 32'b11000010100111010010001111010111;
		#20 FP1 = 32'b01000001101110011100011010101000;
		FP2 = 32'b01000001110010100010000011000101;
		#20 FP1 = 32'b11000001011000111001001100001100;
		FP2 = 32'b11000010000010000111000111111001;
		#20 FP1 = 32'b01000001100000000000001000001100;
		FP2 = 32'b11000000000000000000010011101010;

		#841 if(result == 32'b11000011001010110000000000000000) $display("Expect: 11000011001010110000000000000000 ========> PASSED");
			else $display("Expect: 11000011001010110000000000000000 ========> FAILED");
		#20 if(result == 32'b01000001011111110101110000101001) $display("Expect: 01000001011111110101110000101001 ========> PASSED");
			else $display("Expect: 01000001011111110101110000101001 ========> FAILED");
		#20 if(result == 32'b01000100011100000110110010011000) $display("Expect: 01000100011100000110110010011000 ========> PASSED");
			else $display("Expect: 01000100011100000110110010011000 ========> FAILED");
		#20 if(result == 32'b01000100000100101010111010001000) $display("Expect: 01000100000100101010111010001000 ========> PASSED");
			else $display("Expect: 01000100000100101010111010001000 ========> FAILED");
		#20 if(result == 32'b01000011111100101001011011011110) $display("Expect: 01000011111100101001011011011110 ========> PASSED");
			else $display("Expect: 01000011111100101001011011011110 ========> FAILED");
		#20 if(result == 32'b11000010000000000000011011110111) $display("Expect: 11000010000000000000011011110111 ========> PASSED");
			else $display("Expect: 11000010000000000000011011110111 ========> FAILED");
*/		//#100000
		#960 
		#0 $finish;
	end

endmodule