`timescale 1ns/1ps

module mult5bit_tb();
	reg clk, enable, reset;
	reg [4:0] x,y;
	wire [9:0] p;
	//wire out_en;
	reg [10:0] clk_count;
	mult_5bit inst(clk,reset,enable,x,y,p);
	//multiplier inst(clk,start,reset,x,y,p);
	always #5 clk= ~clk;
	
	always @(posedge clk) begin
		if(!enable || !reset) clk_count <=0;
		else begin
				clk_count <= clk_count +1;
				$display("------Number of periods: %d", clk_count);
				end
	end
	initial begin
		$monitor("time= %d, clk= %b, enable= %b, reset= %b, x= %d, y= %d, p= %d",$time,clk,enable,reset,x,y,p);
		clk <= 0;
		enable <= 0;
		reset <= 1;
		#10 enable <= 1;
		#10 reset <= 0;
		#10 reset <= 1;
		#4 x <= 5'd9;
		y<= 5'd10;
		
		#10 x <= 5'd19;
		y<= 5'd20;
		#10 x <= 5'd22;
		y<= 5'd11;
		#10 x <= 5'd25;
		y<= 5'd16;
		#10 x <= 5'd28;
		y<= 5'd32;
		#10 x <= 5'd11;
		y<= 5'd1;
		#10 x <= 5'd11;
		y<= 5'd12;
		#10 x <= 5'd11;
		y<= 5'd11;
		#10 x <= 5'd11;
		y<= 5'd14;
		
		#900 $finish;
	end
endmodule
