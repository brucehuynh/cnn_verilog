
module sign(
			clk,
			resetn,
			enable,
			s1,
			s2,
			s
);
	input clk;
	input resetn;
	input enable;
	input s1;
	input s2;
	output s;
	
	wire hold;
	
	xor xor_1(hold,s1,s2);
	delay_clock #(.DATA_WIDTH(1),.N_CLOCKs(49)) delay_clock_inst(clk,resetn,enable,hold,s);
	
endmodule
