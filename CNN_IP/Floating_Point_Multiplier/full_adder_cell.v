/*
`include "../Lib_Cells/addbit.v"
`include "../Lib_Cells/dff.v"
*/
module full_adder_cell(
					clk,
					resetn,
					enable,
					a, 
					x, //a*x 
					in,
					ci, 
					p, //a*x + in + ci
					co, //carry out
					oa
);
	input clk;
	input resetn;
	input enable;
	input a;
	input x;
	input in;
	input ci;
	output p;
	output co;
	output oa;
	
	wire add_wire;
	wire carry_wire;
	
	and and_inst(a_x,a,x); // calculate a*x
	addbit addbit_inst(a_x,in,ci,add_wire,carry_wire);
	dff dff_inst_0(clk,resetn,enable,add_wire,p);
	dff dff_inst_1(clk,resetn,enable,carry_wire,co);
	dff dff_inst_2(clk,resetn,enable,a,oa);
	
endmodule
