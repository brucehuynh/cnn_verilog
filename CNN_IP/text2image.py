import cv2 as cv
import sys
from scipy import ndimage
import numpy as np
import struct

def bin_to_float(b):
    """ Convert binary string to a float. """
    f = int(b, 2)  
    return struct.unpack('f', struct.pack('I', f))[0]
def t2i_gray(filename):
    f = open(filename,'r')
    line1 = f.readline()
    width=int(line1.split('\n')[0])
    print (width)
    line2 = f.readline()
    height=int(line2.split('\n')[0])
    print (height)
    line3 = f.readline()
    frame=int(line3.split('\n')[0])
    print (frame)
    im = np.empty([height,width,1],dtype=np.uint8)
    k=0
    for i in range (width):
        for j in range (height):
            #R = f.readline()
            k=k+1
            print("Pixels #",k)
            G = f.readline()
            G=G.strip()
            G=np.uint8(bin_to_float(G))
            print(G)
            #G=np.uint8(G/9)
            #print(G)
            if not G: break
            #if G > 255:
            #    G = 255
            #elif G < 0:
            #    G = 0
            #B = f.readline()
            #data = G.split(' ')[0]
            im[i][j][0] = (G)
    #print (im)
    f.close()
    cv.imwrite('result.jpg',im)
    cv.imshow('result',im)
    cv.waitKey(0)
    return im

filename = sys.argv[1]
t2i_gray(filename)
