`timescale 1ns/1ps
//`include "max_tb.v"
//`include "max.v"
//`include "test_activate.v"
//`include "activate.v"
`include "controller.v"
`include "conv2d_tb.v"
`include "conv2d.v"
`include "conv2d_test.v"
`include "line_buffer.v"
`include "sink.v"
`include "source.v"
`include "../Floating_Point_Adder/buffer1.v"
`include "../Floating_Point_Adder/buffer2.v"
`include "../Floating_Point_Adder/cmpshift.v"
`include "../Floating_Point_Adder/faddsub.v"
`include "../Floating_Point_Adder/fpadd.v"
`include "../Floating_Point_Adder/normalized.v"
//`include "../Floating_Point_Adder/test_fpadd.v"
//`include "../Integer2Float/float2int.v"
`include "../Integer2Float/int2float.v"
//`include "../Integer2Float/test_converter.v"
`include "../Lib_Cells/addbit.v"
`include "../Lib_Cells/addbit.v"
`include "../Lib_Cells/adder.v"
`include "../Lib_Cells/CLA_4bits.v"
`include "../Lib_Cells/CLA_8bits.v"
`include "../Lib_Cells/delay_clock.v"
`include "../Lib_Cells/dff.v"
`include "../Lib_Cells/nbit_dff.v"
`include "../Lib_Cells/subtract_bit.v"
`include "../Lib_Cells/subtractor.v"
`include "../Floating_Point_Multiplier/exponent_add.v"
`include "../Floating_Point_Multiplier/FP_multiplier.v"
`include "../Floating_Point_Multiplier/full_adder_cell.v"
`include "../Floating_Point_Multiplier/full_adder_latched.v"
`include "../Floating_Point_Multiplier/mult_24bit.v"
//`include "../Floating_Point_Multiplier/mult5bit_tb.v"
`include "../Floating_Point_Multiplier/mult_5bit.v"
//`include "../Floating_Point_Multiplier/multiplier_tb.v"
`include "../Floating_Point_Multiplier/normalizer.v"
`include "../Floating_Point_Multiplier/sign.v"
//`include "../Floating_Point_Multiplier/test_add_exp.v"
//`include "../Floating_Point_Multiplier/test_delay.v"
//`include "../Floating_Point_Multiplier/test.v"


module conv2d_tb();
reg clk,resetn,enable;
conv2d_test inst(clk,resetn,enable);

always #1 clk = ~clk;
initial begin
		clk = 0;
		resetn = 0;
	 	enable = 0;		
	#10	resetn = 1;
		enable = 1;
end

endmodule
