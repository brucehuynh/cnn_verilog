@echo off
if "%~1" equ "" (
  set TESTNAME=Top
) else (
  set TESTNAME=%1
)
vsim -do wave.do -view ../result/%TESTNAME%.wlf
