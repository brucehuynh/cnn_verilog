onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider lines
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data_valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/valid_out
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a1
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a2
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a3
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a4
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a5
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a6
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a7
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a8
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/valid_out_cvt2float
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/counter
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/enable_cvt
add wave -noupdate -divider top
add wave -noupdate /conv2d_tb/inst/conv2d_inst/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/data_valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/data_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable_cvt
add wave -noupdate /conv2d_tb/inst/conv2d_inst/data_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_out_pixel
add wave -noupdate /conv2d_tb/inst/conv2d_inst/done_img
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/w9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/k9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_out_mult
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/res8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/x9_bk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable_mult
add wave -noupdate /conv2d_tb/inst/conv2d_inst/counter
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1347 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2138 ps} {59888 ps}
