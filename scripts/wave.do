onerror {resume}
radix define float32 {
    -default symbolic
}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider truncate
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/clk
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/resetn
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/enable
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/data_conv
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/truncate_inst/data_truncate
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/valid_out
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/done
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/truncate_inst/count_v
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/truncate_inst/count_h
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/truncate_inst/counter_2
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/truncate_inst/counter_3
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/enable_buffer
add wave -noupdate -divider lb
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/lb_2/clk
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/lb_2/resetn
add wave -noupdate /conv2d_kernel_tb/inst/truncate_inst/lb_2/enable
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/truncate_inst/lb_2/in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/truncate_inst/lb_2/out
add wave -noupdate -divider pooling
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/clk
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/enable
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/resetn
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_out
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_out
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/done
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/enable_count
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/counter_h
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/counter_v
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/counter_2
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/counter_1
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/counter_3
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_max
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w1
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w2
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w3
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w4
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_out_max
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_out_max
add wave -noupdate -divider softmax
add wave -noupdate /conv2d_kernel_tb/inst/softmax/clk
add wave -noupdate /conv2d_kernel_tb/inst/softmax/resetn
add wave -noupdate /conv2d_kernel_tb/inst/softmax/enable
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_in
add wave -noupdate /conv2d_kernel_tb/inst/softmax/load_bias
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b0
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b1
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b2
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b3
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b4
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b5
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b6
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b7
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b8
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b9
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/data_in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is0
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is1
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is2
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is3
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is4
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is5
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is6
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is7
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is8
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/is9
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_out
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/a
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/a_delay
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_out_get_weight
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_out_div
add wave -noupdate /conv2d_kernel_tb/inst/softmax/load_bias_done
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b0R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b1R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b2R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b3R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b4R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b5R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b6R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b7R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b8R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/b9R
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/stage1
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_add_1
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/stage2
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_add_2
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/stage3
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_add_3
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/softmax/sum_of_exp
add wave -noupdate /conv2d_kernel_tb/inst/softmax/valid_add_4
add wave -noupdate -divider weight
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/clk
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/resetn
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/enable
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_in
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/data_in
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/bias
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/data_out
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_out
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/getWeight
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/Weight
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/Data
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/counter
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/counter_1
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/product
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/eof
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_out_mult
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/enable_add1
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/tmp
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage1
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_1
add wave -noupdate -expand /conv2d_kernel_tb/inst/softmax/W0_inst/stage2
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_2
add wave -noupdate -expand -subitemconfig {{/conv2d_kernel_tb/inst/softmax/W0_inst/stage3[20]} -expand} /conv2d_kernel_tb/inst/softmax/W0_inst/stage3
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_3
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage4
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_4
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage5
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_5
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_bias
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage6
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_6
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage7
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_7
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/stage8
add wave -noupdate /conv2d_kernel_tb/inst/softmax/W0_inst/valid_add_8
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1988559 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 178
configure wave -valuecolwidth 164
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1731046 ps} {2246072 ps}
