@echo off
if "%~1" equ "" (
  set TESTNAME=test_top
) else (
  set TESTNAME=%1
)
if "%~2" equ "" (
  set SEED=%RANDOM%
) else (
  set SEED=%2
)
echo "  Test name = %TESTNAME%"
echo "  Test seed = %SEED%"
vsim -f simulate.f