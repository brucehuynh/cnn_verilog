@echo off
if "%~1" equ "" (
  set TESTNAME=test_top
) else (
  set TESTNAME=%1
)
rd /s /q work
vlib work
vmap work work
vlog -novopt -f compile.f
