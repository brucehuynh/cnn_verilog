onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider division
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/dividend
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/divisor
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/quotient
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/dividendR
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/divisorR
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/e
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/s_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/e_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/e_next
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/e_final
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/temp_q
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/q
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/temp_s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/division/done_div
add wave -noupdate /conv2d_tb/clk
add wave -noupdate /conv2d_tb/resetn
add wave -noupdate /conv2d_tb/enable
add wave -noupdate -divider lines
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data_valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/lbs/w9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/valid_out
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a1
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a2
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a3
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a4
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a5
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a6
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a7
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a8
add wave -noupdate -radix unsigned /conv2d_tb/inst/conv2d_inst/lbs/a9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/valid_out_cvt2float
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/counter
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/data
add wave -noupdate /conv2d_tb/inst/conv2d_inst/lbs/enable_cvt
add wave -noupdate -divider top
add wave -noupdate /conv2d_tb/inst/conv2d_inst/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/data_valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/data_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable_cvt
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/data_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_out_pixel
add wave -noupdate /conv2d_tb/inst/conv2d_inst/done_img
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/w9
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/k9
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x9
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_out_mult
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res6
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/res8
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add7
add wave -noupdate /conv2d_tb/inst/conv2d_inst/valid_add8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/x9_bk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/enable_mult
add wave -noupdate /conv2d_tb/inst/conv2d_inst/counter
add wave -noupdate -divider add1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_1/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_1/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/valid_in
add wave -noupdate -radix float32 -childformat {{{/conv2d_tb/inst/conv2d_inst/add_1/out[31]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[30]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[29]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[28]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[27]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[26]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[25]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[24]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[23]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[22]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[21]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[20]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[19]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[18]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[17]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[16]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[15]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[14]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[13]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[12]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[11]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[10]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[9]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[8]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[7]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[6]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[5]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[4]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[3]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[2]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[1]} -radix float32} {{/conv2d_tb/inst/conv2d_inst/add_1/out[0]} -radix float32}} -subitemconfig {{/conv2d_tb/inst/conv2d_inst/add_1/out[31]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[30]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[29]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[28]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[27]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[26]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[25]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[24]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[23]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[22]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[21]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[20]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[19]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[18]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[17]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[16]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[15]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[14]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[13]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[12]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[11]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[10]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[9]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[8]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[7]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[6]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[5]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[4]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[3]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[2]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[1]} {-height 17 -radix float32} {/conv2d_tb/inst/conv2d_inst/add_1/out[0]} {-height 17 -radix float32}} /conv2d_tb/inst/conv2d_inst/add_1/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_1/m2
add wave -noupdate -divider add2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_2/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_2/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_2/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_2/m2
add wave -noupdate -divider add3
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_3/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_3/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_3/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_3/m2
add wave -noupdate -divider add4
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_4/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_4/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_4/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_4/m2
add wave -noupdate -divider add5
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_5/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_5/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_5/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_5/m2
add wave -noupdate -divider add6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/a
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/valid_in
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_6/m2
add wave -noupdate -divider add7
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_7/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_7/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_7/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_7/m2
add wave -noupdate -divider add8
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_8/a
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_8/b
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/add_8/out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ex
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ey
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/exy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ex1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ey1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ex2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/ex3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/s
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/s3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sr
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/s4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn3
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn4
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sr1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sr2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/sn6
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/mx
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/my
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/mxy
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/mx1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/my1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/mxy1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/mxy2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/s1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/s2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/e1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/e2
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/m1
add wave -noupdate /conv2d_tb/inst/conv2d_inst/add_8/m2
add wave -noupdate -divider mult5
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/clk
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/resetn
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/enable
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/valid_in
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/in1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/in2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/product
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/valid_out
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/ovf
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/FP1
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/FP2
add wave -noupdate -radix float32 /conv2d_tb/inst/conv2d_inst/kernel_5/temp_output
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/temp_ovf
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/temp_48
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/counter
add wave -noupdate /conv2d_tb/inst/conv2d_inst/kernel_5/iszero
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {181000 ps} 0} {{Cursor 4} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 182
configure wave -valuecolwidth 174
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {202402 ps} {227638 ps}
