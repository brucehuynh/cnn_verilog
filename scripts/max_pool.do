onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider max_pool
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/clk
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/enable
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/resetn
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_in
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_out
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_out
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/done
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/enable_count
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/max_pooling_inst/counter_h
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/max_pooling_inst/counter_v
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/max_pooling_inst/counter_2
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/max_pooling_inst/counter_1
add wave -noupdate -radix unsigned /conv2d_kernel_tb/inst/max_pooling_inst/counter_3
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_max
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w1
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w2
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w3
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/w4
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/valid_out_max
add wave -noupdate -radix float32 /conv2d_kernel_tb/inst/max_pooling_inst/data_out_max
add wave -noupdate /conv2d_kernel_tb/inst/max_pooling_inst/enable_lb2
add wave -noupdate -divider n1
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[0]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[0]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[0]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[0]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[0]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n2
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[1]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[1]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[1]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[1]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[1]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n3
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[2]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[2]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[2]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[2]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[2]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n4
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[3]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[3]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[3]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[3]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[3]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n5
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[4]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[4]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[4]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[4]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[4]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n6
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[5]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[5]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[5]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[5]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[5]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n7
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[6]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[6]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[6]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[6]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[6]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n8
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[7]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[7]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[7]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[7]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[7]/genblk1/nbit_dff_ins/out}
add wave -noupdate -divider n9
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[8]/genblk1/nbit_dff_ins/clk}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[8]/genblk1/nbit_dff_ins/resetn}
add wave -noupdate {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[8]/genblk1/nbit_dff_ins/enable}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[8]/genblk1/nbit_dff_ins/in}
add wave -noupdate -radix float32 {/conv2d_kernel_tb/inst/max_pooling_inst/lb_2/genblk1[8]/genblk1/nbit_dff_ins/out}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {371000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {360743 ps} {379447 ps}
